require 'test_helper'

class RobokassaControllerTest < ActionController::TestCase
  test "should get notify" do
    get :notify
    assert_response :success
  end

  test "should get success" do
    get :success
    assert_response :success
  end

  test "should get fail" do
    get :fail
    assert_response :success
  end

end
