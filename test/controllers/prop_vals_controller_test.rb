require 'test_helper'

class PropValsControllerTest < ActionController::TestCase
  setup do
    @prop_val = prop_vals(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:prop_vals)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create prop_val" do
    assert_difference('PropVal.count') do
      post :create, prop_val: { color: @prop_val.color, img: @prop_val.img, title: @prop_val.title }
    end

    assert_redirected_to prop_val_path(assigns(:prop_val))
  end

  test "should show prop_val" do
    get :show, id: @prop_val
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @prop_val
    assert_response :success
  end

  test "should update prop_val" do
    patch :update, id: @prop_val, prop_val: { color: @prop_val.color, img: @prop_val.img, title: @prop_val.title }
    assert_redirected_to prop_val_path(assigns(:prop_val))
  end

  test "should destroy prop_val" do
    assert_difference('PropVal.count', -1) do
      delete :destroy, id: @prop_val
    end

    assert_redirected_to prop_vals_path
  end
end
