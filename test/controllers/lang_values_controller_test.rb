require 'test_helper'

class LangValuesControllerTest < ActionController::TestCase
  setup do
    @lang_value = lang_values(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:lang_values)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create lang_value" do
    assert_difference('LangValue.count') do
      post :create, lang_value: { value: @lang_value.value }
    end

    assert_redirected_to lang_value_path(assigns(:lang_value))
  end

  test "should show lang_value" do
    get :show, id: @lang_value
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @lang_value
    assert_response :success
  end

  test "should update lang_value" do
    patch :update, id: @lang_value, lang_value: { value: @lang_value.value }
    assert_redirected_to lang_value_path(assigns(:lang_value))
  end

  test "should destroy lang_value" do
    assert_difference('LangValue.count', -1) do
      delete :destroy, id: @lang_value
    end

    assert_redirected_to lang_values_path
  end
end
