json.array!(@page_lang_rels) do |lang|
    json.short_name lang.lang.short_name
    json.id lang.id
    json.name @page.name
    json.href @page.href
    json.title lang.title
    json.text lang.text
    json.long_name lang.lang.long_name

 end
