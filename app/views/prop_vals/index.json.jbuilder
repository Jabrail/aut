json.prop_vals(@prop_vals) do |prop_val|
  json.extract! prop_val, :id, :title, :color
  json.image prop_val.image.url(:thumb)
end


json.vals(['id', 'title' , 'color',  'image', 'Actions']) do |val|
  json.value val
end