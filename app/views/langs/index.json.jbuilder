  json.langs(@langs) do |lang|
    json.extract! lang, :id, :short_name, :long_name
  end

json.vals(['id', 'Short_name' , 'Long_name', 'Actions']) do |val|
  json.value val
end