json.filters(@properties) do |property|
  json.extract! property, :id, :name, :type_value, :valid_data
 end


json.vals(['id', 'name' , 'type_value' , 'valid_data' , 'Actions']) do |val|
  json.value val
end