json.array!(@marks) do |mark|
  json.id mark.id
  json.name mark.title
  json.count  Advert.where('mark = ?', mark.id).count
end
