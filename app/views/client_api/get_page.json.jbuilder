json.page do

  if @page
    json.id @page.id
    json.href @page.href
    json.name @page.name
    json.title  PageLangRel.find_by_page_id(@page.id).title
    json.text  PageLangRel.find_by_page_id(@page.id).text

  end

end