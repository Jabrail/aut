json.array!(@adverts) do |advert|
  json.extract! advert, :id, :price, :description
  if  json.service advert.services.first
    if  json.service advert.services.first.s_type
      json.service '1'
    else
      json.service '0'
    end
  else
    json.service '0'
  end

  json.big_image  advert.images[0].image.url(:big_image)
  json.medium  advert.images[0].image.url(:medium)
  json.thumb  advert.images[0].image.url(:thumb)
  json.created_at advert.created_at.strftime("%d.%m.%Y")

  Property.all().each do |filter|

    if filter.type_value == 1
      json.set! filter.name , advert[filter.name]
    else
      id = advert[filter.name].to_i

      if id != 0
        json.set! filter.name, PropVal.find(id).title
      else
        json.set! filter.name, 'nill'
      end
    end


  end

  json.model PropVal.find(advert.model).title
  json.city PropVal.find(advert.city).title

end
