json.pages do

  @pages.each do |page|
    json.set! page.name do

    json.id page.id
    json.href page.href
    json.title PageLangRel.find_by_page_id(page.id).title

    end

  end

end