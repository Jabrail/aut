json.filters do

  @filters.each do |filter|
    json.set! filter.name do

      json.extract! filter, :id, :name, :type_value, :valid_data

      json.title LangValue.where('lang_id = ? AND property_id = ?' , @lang.id, filter.id)[0].value

      if filter.type_value == 4
        json.selected '0'
      end
      if filter.type_value == 1
        json.selected '0'
      end

      json.prop_vals filter.prop_vals do |prop_val|
        json.extract! prop_val, :id, :title, :color
        json.url prop_val_url(prop_val, format: :json)
        json.image prop_val.image.url(:thumb)

        if filter.type_value == 10
          json.prop_vals prop_val.prop_vals do |prop_val|
            json.extract! prop_val, :id, :title, :color
            json.url prop_val_url(prop_val, format: :json)
            json.image prop_val.image.url(:thumb)
          end
        end
        if filter.type_value == 13
          json.prop_vals prop_val.prop_vals do |prop_val|
            json.extract! prop_val, :id, :title, :color
            json.url prop_val_url(prop_val, format: :json)
            json.image prop_val.image.url(:thumb)
          end
        end
      end


    end

  end

end