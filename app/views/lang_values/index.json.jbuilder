json.array!(@lang_values) do |lang_value|
  json.extract! lang_value, :id, :value
  json.url lang_value_url(lang_value, format: :json)
end
