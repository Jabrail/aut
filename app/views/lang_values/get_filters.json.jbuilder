json.langs(@property.lang_values) do |vals|
  json.id vals.id
  json.lang vals.lang
  json.value vals.value
end
