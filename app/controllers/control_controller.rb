class ControlController < ApplicationController
  before_filter :authenticate_user!

  def index


    if current_user.user_type == 1
      render :private_person, :layout => "admin"
    elsif current_user.user_type == 2
      render :dealer, :layout => "admin"
    else
      render :admin, :layout => "admin"
    end
  end

  def private_person
  end

  def dealer
  end

  def admin
  end

end
