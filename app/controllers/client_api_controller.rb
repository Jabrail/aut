class ClientApiController < ApplicationController

  def get_filters

    @filters = Property.all();
    @lang = Lang.find_by_short_name('ru')
  end

  def get_poster

    @advert = Advert.find(params[:id])
    @lang = Lang.find_by_short_name('ru');
    @additional = []

  end

  def get_filters_for_new

    @filters = Property.all();
    @lang = Lang.find_by_short_name('ru')


  end

  def last_poster
    @adverts = Advert.all().limit(4)
    @marks = Property.find_by_name('mark').prop_vals
  end

  def get_marks
    @marks = Property.find_by_name('mark').prop_vals
  end

  def get_pages

    @pages = Page.all()

  end

  def get_page

    @page = Page.find_by_name(params[:id])

  end

  def get_user

  end

  def count_poster
    @adverts = Advert.all();
  end

  def get_user_poster
    if current_user
      @adverts = current_user.adverts
      @marks = Property.find_by_name('mark').prop_vals
    end
  end
end
