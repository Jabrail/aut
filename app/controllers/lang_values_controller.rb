class LangValuesController < ApplicationController
  before_action :set_lang_value, only: [:show, :edit, :update, :destroy]

  # GET /lang_values
  # GET /lang_values.json
  def index
    @lang_values = LangValue.all
  end

  def get_filters

    @langs = Lang.all()
    @lang_values = []

    @property = Property.find(params[:id])

    @langs.each do |lang|

      lang_val = LangValue.where('lang_id = ? AND property_id = ?' , lang.id , params[:id])

      if lang_val.length > 0

      else

        lang_val = LangValue.new()
        lang_val.lang = lang
        lang_val.property = @property
        lang_val.value = ""
        lang_val.save


      end

    end

  end

  # GET /lang_values/1
  # GET /lang_values/1.json
  def show
  end

  # GET /lang_values/new
  def new
    @lang_value = LangValue.new
  end

  # GET /lang_values/1/edit
  def edit
  end

  # POST /lang_values
  # POST /lang_values.json
  def create
    @lang_value = LangValue.new(lang_value_params)

    @lang_value.lang = Lang.find(params[:l_id]);
    @lang_value.property = Property.find(params[:p_id]);

    respond_to do |format|
      if @lang_value.save
        format.html { redirect_to @lang_value, notice: 'Lang value was successfully created.' }
        format.json { render :show, status: :created, location: @lang_value }
      else
        format.html { render :new }
        format.json { render json: @lang_value.errors, status: :unprocessable_entity }
      end
    end
  end

  def add_filter

    params[:items].each do |item|

      lang_val = LangValue.find(item[:id])
      lang_val.value = item[:value];
      lang_val.save
    end
  end


  # PATCH/PUT /lang_values/1
  # PATCH/PUT /lang_values/1.json
  def update
    respond_to do |format|
      if @lang_value.update(lang_value_params)
        format.html { redirect_to @lang_value, notice: 'Lang value was successfully updated.' }
        format.json { render :show, status: :ok, location: @lang_value }
      else
        format.html { render :edit }
        format.json { render json: @lang_value.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lang_values/1
  # DELETE /lang_values/1.json
  def destroy
    @lang_value.destroy
    respond_to do |format|
      format.html { redirect_to lang_values_url, notice: 'Lang value was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_lang_value
      @lang_value = LangValue.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def lang_value_params
      params.require(:lang_value).permit(:value)
    end
end
