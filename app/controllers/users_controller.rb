class UsersController < Devise::RegistrationsController
  def create

    super

    puts user_params[:user_type]
    @user.user_type = user_params[:user_type]
    @user.save

    @service_user = ServiceUser.create
    @service_user.user = @user
    @service_user.service = Service.find(13)
    @service_user.save

  end

  def user_params
    params.require(:user).permit(:user_type)
  end

  def index

    @users = User.all

  end

  def sign_in

  end

  def sign_out

  end

end
