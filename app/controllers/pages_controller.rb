class PagesController < ApplicationController
  before_action :set_page, only: [:show, :edit, :update, :destroy, :add_content, :update_lang]

  # GET /pages
  # GET /pages.json
  def index
    @pages = Page.all
  end

  # GET /pages/1
  # GET /pages/1.json
  def show

    @langs = Lang.all
    @langs.each do |lang|
      page_lang_rel = PageLangRel.where("lang_id = ? AND page_id = ?" , lang.id , @page.id).to_a
puts 'page_lang_rel'
puts page_lang_rel
      if page_lang_rel.length == 0

        page_lang = PageLangRel.new()
        page_lang.title = ''
        page_lang.text = ''
        page_lang.page_id = @page.id
        page_lang.lang_id = lang.id
        page_lang.save
        puts 'page_lang_rel1'
        puts page_lang
        puts 'page_lang_rel1'
      end
    end

    @page_lang_rels = PageLangRel.where('page_id = ?' , @page.id).to_a
    puts @page_lang_rels
  end

  # GET /pages/new
  def new
    @page = Page.new
  end

  # GET /pages/1/edit
  def edit
  end

  def add_content



  end
  # POST /pages
  # POST /pages.json
  def create
    @page = Page.new(page_params)

    respond_to do |format|
      if @page.save
        format.html { redirect_to @page, notice: 'Page was successfully created.' }
        format.json { render :show, status: :created, location: @page }
      else
        format.html { render :new }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pages/1
  # PATCH/PUT /pages/1.json
  def update
    respond_to do |format|
      if @page.update(page_params)
        format.html { redirect_to @page, notice: 'Page was successfully updated.' }
        format.json { render :show, status: :ok, location: @page }
      else
        format.html { render :edit }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_lang
    lang = Lang.find_by_short_name(params[:page][:short_name])

    if PageLangRel.where('page_id = ? AND lang_id = ? ', @page.id, lang.id)

      page_lang = PageLangRel.where('page_id = ? AND lang_id = ? ', @page.id, lang.id).to_a;
      page_lang[0].title = params[:page][:title]
      page_lang[0].text = params[:page][:text]
      page_lang[0].save

    else

      page_lang = PageLangRel.new()
      page_lang.title = params[:page][:title]
      page_lang.text = params[:page][:text]
      page_lang.page_id = @page.id
      page_lang.lang_id = lang.id
      page_lang.save

    end
  end

  # DELETE /pages/1
  # DELETE /pages/1.json
  def destroy
    @page.destroy
    respond_to do |format|
      format.html { redirect_to pages_url, notice: 'Page was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_page
    @page = Page.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def page_params
    params.require(:page).permit(:name, :href)
  end
end
