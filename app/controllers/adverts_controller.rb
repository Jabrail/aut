class AdvertsController < ApplicationController
  before_action :set_advert, only: [:show, :edit, :update, :destroy, :set_top]
  require "rubygems"
  require "json"
  # GET /adverts
  # GET /adverts.json
  def index

    # @adverts = current_user.adverts
    @adverts = Advert.all
    @count_free = 0
    @count_all = 0
    current_user.services.each do |service|

      @count_all = @count_all + service.count

    end

    @count_free = @count_all - @adverts.count

  end

  def get_by_prop
    if params[:pops]
      where = ' '
      property = {}
      last = {}
      params[:pops].each do |filter|
        property = Property.find(filter[:id])
        if filter[:value]
          if property.type_value == 10
            if filter[:value_sub]
              if property.name == 'mark'
                where = where + property.name + ' = ' + filter[:value].to_s + ' AND '
                where = where + 'model = ' + filter[:value_sub].to_s + ' AND '
              else
                where = where + property.name + ' = ' + filter[:value].to_s + ' AND '
                where = where + 'city = ' + filter[:value_sub].to_s + ' AND '
              end
            else

              where = where + property.name + ' = ' + filter[:value].to_s + ' AND '
            end
          elsif filter[:value].is_a? Array

            where = where + '('

            if property.name == 'additional'

              filter[:value].each do |val|

                where = where + property.name + ' LIKE "%' + val.to_s + '%" AND '

              end
              where = where +  ' id > 0  ) AND '
            else

              filter[:value].each do |val|

                where = where + property.name + ' = ' + val.to_s + ' OR '

              end
              where = where +  ' id < 0  ) AND '
            end

          else
            where = where + property.name + ' = ' + filter[:value].to_s + ' AND '
          end

          last =filter
        elsif filter[:value_befor]

          if property.name == "year"

            if filter[:value_befor].to_i > filter[:value_after].to_i

              befor = filter[:value_befor].to_i
              filter[:value_befor] = filter[:value_after].to_i
              filter[:value_after] =  befor

            end

            where = where + '0 + '+property.name + ' >= ' +  filter[:value_befor].to_s + ' AND '
            where = where + '0 + '+property.name + ' <= ' +  filter[:value_after].to_s + ' AND '

          elsif property.name == "mileage"

            where = where + '0 + ' +property.name + ' >= ' +  PropVal.find(filter[:value_befor]).title.delete(' ').to_i.to_s + ' AND '
            where = where + '0 + ' +property.name + ' <= ' +  PropVal.find(filter[:value_after]).title.delete(' ').to_i.to_s + ' AND '

          end

        end


      end
      where = where +  ' id > 0 '

      if params[:desc]
        if params[:desc] == "0"
          @adverts = Advert.where(where).order(:created_at)
        elsif params[:desc] == "1"
          @adverts = Advert.where(where).order(:price)
        elsif params[:desc] == "2"
          @adverts = Advert.where(where).order(:created_at)
        end

      else
        @adverts = Advert.where(where)
      end

    else
      @adverts = Advert.all
    end

  end

  # GET /adverts/1
  # GET /adverts/1.json
  def show
  end

  # GET /adverts/new
  def new
    @advert = Advert.new
  end

  # GET /adverts/1/edit
  def edit
  end

  # POST /adverts
  # POST /adverts.json
  def create
    @advert = Advert.new(advert_params)

    @advert.user = current_user
    if params[:set_top] == 1
      @service_rel = ServiceAdvert.new
      @service_rel.advert = @advert
      @service_rel.service = Service.find(11)
      @service_rel.save
    end

    @advert.save

    respond_to do |format|


      params[:filters].each do |filter|
        _filter = Property.find(filter[:id])
        if filter[:type] == 10

          if _filter.name == 'mark'
            @advert.mark = filter[:value][:base].to_i
            @advert.model = filter[:value][:sub].to_i
          end

          if _filter.name == 'country'
            @advert.country = filter[:value][:base].to_i
            @advert.city = filter[:value][:sub].to_i
          end

        elsif filter[:type] == 2

          @advert[_filter.name] = filter[:value].map(&:inspect).join(', ')

        else

          @advert[_filter.name] = filter[:value]

        end

      end

      params[:images].each do |image|
        puts image
        @image = Image.find(image)
        @image.advert_id = @advert.id
        @image.save
      end

      if @advert.save
        format.html { redirect_to @advert, notice: 'Advert was successfully created.' }
        format.json { render :show, status: :created, location: @advert }
      else
        format.html { render :new }
        format.json { render json: @advert.errors, status: :unprocessable_entity }
      end
    end
  end

  def create_with_reg
    build_resource(sign_up_params)

    resource.save
    yield resource if block_given?
    if resource.persisted?
      if resource.active_for_authentication?
        set_flash_message :notice, :signed_up if is_flashing_format?
        sign_up(resource_name, resource)
        respond_with resource, location: after_sign_up_path_for(resource)
      else
        set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_flashing_format?
        expire_data_after_sign_in!
        respond_with resource, location: after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      set_minimum_password_length
      respond_with resource
    end

  end
  # PATCH/PUT /adverts/1
  # PATCH/PUT /adverts/1.json
  def update
    respond_to do |format|
      if @advert.update(advert_params)
        format.html { redirect_to @advert, notice: 'Advert was successfully updated.' }
        format.json { render :show, status: :ok, location: @advert }
      else
        format.html { render :edit }
        format.json { render json: @advert.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /adverts/1
  # DELETE /adverts/1.json
  def destroy
    if current_user

      if @advert.user_id == current_user.id

        @advert.images.each do |image|
          image.destroy
        end

        @advert.destroy
        respond_to do |format|
          format.html { redirect_to adverts_url, notice: 'Advert was successfully destroyed.' }
          format.json { head :no_content }
        end
      end

    end
  end

  def set_top
    puts @advert
    if params[:set_top] == '1'
      @service_rel = ServiceAdvert.new
      @service_rel.advert = @advert
      @service_rel.service = Service.find(11)
      @service_rel.save
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_advert
    @advert = Advert.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def advert_params
    params.require(:poster).permit(:price, :description)
  end

  def build_resource(hash=nil)
    self.resource = resource_class.new_with_session(hash || {}, session)
  end

  def sign_up_params
    devise_parameter_sanitizer.sanitize(:sign_up)
  end

  def sign_up(resource_name, resource)
    sign_in(resource_name, resource)
  end

end
