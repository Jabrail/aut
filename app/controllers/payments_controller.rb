require 'digest/md5'
class PaymentsController < ApplicationController

  def pay
    # назодим заказ который надо оплатить, например так:
    @order = Order.find(params[:id])
    unless @order.blank? && @order.payment.blank?
      # заполняем параметры формы
      @pay_desc = Hash.new
      @pay_desc['mrh_url']   = Payment::MERCHANT_URL
      @pay_desc['mrh_login'] = Payment::MERCHANT_LOGIN
      @pay_desc['mrh_pass1'] = Payment::MERCHANT_PASS_1
      @pay_desc['inv_id']    = 0
      @pay_desc['inv_desc']  = @order.payment.desc
      @pay_desc['out_summ']  = @order.payment.price.to_s
      @pay_desc['shp_item']  = @order.id
      @pay_desc['in_curr']   = "WMRM"
      @pay_desc['culture']   = "ru"
      @pay_desc['encoding']  = "utf-8"
      # расчет контрольной суммы
      @pay_desc['crc'] = Payment.get_hash(@pay_desc['mrh_login'],
                                          @pay_desc['out_summ'],
                                          @pay_desc['inv_id'],
                                          @pay_desc['mrh_pass1'],
                                          "Shp_item=#{@pay_desc['shp_item']}")
    end
  end

  def result
    crc = Payment.get_hash(params['OutSum'],
                           params['InvId'],
                           Payment::MERCHANT_PASS_2,
                           "Shp_item=#{params['Shp_item']}")
    @result = "FAIL"
    begin
      # проверяем контрольную сумму, чтобы нас не похекали
      break if params['SignatureValue'].blank? || crc.casecmp(params['SignatureValue']) != 0
      @order = Order.where(:id => params['Shp_item']).first
      # ищем заказ
      break if @order.blank? || @order.payment.price != params['OutSum'].to_f
      # делаем с заказом то что нам нужно
      @order.payment.invid = params['InvId'].to_i
      @order.payment.status = Payment::STATUS_OK
      @order.payment.save
      # ...
      # говорим робокассе, что все хорошо
      @result = "OK#{params['InvId']}"
    end while false
  end

  def success
    # тут говорим пользователю что все ОК
  end

  def fail
    # тут говорим, что есть проблемы...
  end

end
