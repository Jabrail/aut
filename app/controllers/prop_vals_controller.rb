class PropValsController < ApplicationController
  before_action :set_prop_val, only: [:show, :edit, :update, :destroy]

  # GET /prop_vals
  # GET /prop_vals.json
  def index
    @property = Property.find(params[:p_id])
    @prop_vals =  @property.prop_vals
  end

  # GET /prop_vals/1
  # GET /prop_vals/1.json
  def show
  end

  # GET /prop_vals/new
  def new
    @prop_val = PropVal.new
  end

  # GET /prop_vals/1/edit
  def edit
  end

  def get_sub
    @prop_vals = PropVal.find(params[:v_id])
    @prop_vals =  @prop_vals.prop_vals
  end


  def add_sub
    @prop_val = PropVal.new(prop_val_params)
    @prop_val.prop_val_id = params[:v_id]

    respond_to do |format|
      if @prop_val.save
        format.html { redirect_to @prop_val, notice: 'Prop val was successfully created.' }
        format.json { render :show, status: :created, location: @prop_val }
      else
        format.html { render :new }
        format.json { render json: @prop_val.errors, status: :unprocessable_entity }
      end
    end
  end

  # POST /prop_vals
  # POST /prop_vals.json
  def create
    @prop_val = PropVal.new(prop_val_params)
    @prop_val.property = Property.find(params[:p_id])

    respond_to do |format|
      if @prop_val.save
        format.html { redirect_to @prop_val, notice: 'Prop val was successfully created.' }
        format.json { render :show, status: :created, location: @prop_val }
      else
        format.html { render :new }
        format.json { render json: @prop_val.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /prop_vals/1
  # PATCH/PUT /prop_vals/1.json
  def update
    respond_to do |format|
      if @prop_val.update(prop_val_params)
        format.html { redirect_to @prop_val, notice: 'Prop val was successfully updated.' }
        format.json { render :show, status: :ok, location: @prop_val }
      else
        format.html { render :edit }
        format.json { render json: @prop_val.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /prop_vals/1
  # DELETE /prop_vals/1.json
  def destroy
    @prop_val.destroy
    respond_to do |format|
      format.html { redirect_to prop_vals_url, notice: 'Prop val was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_prop_val
      @prop_val = PropVal.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def prop_val_params
      params.require(:prop_val).permit(:title, :color, :image, :img)
    end
end
