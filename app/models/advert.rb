class Advert < ActiveRecord::Base
  has_many :advProRel
  has_many :prop_vals, :through => :advProRel
  has_many :service_adverts
  has_many :services, :through => :service_adverts
  has_many :images
  belongs_to :user
end
