class Page < ActiveRecord::Base
  has_many :langs
  has_many :page_lang_rels, :through => :langs
end
