class Order < ActiveRecord::Base
  belongs_to :payment
  belongs_to :user
  belongs_to :service
end
