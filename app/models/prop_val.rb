class PropVal < ActiveRecord::Base
  has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/

  has_many :advProRel
  has_many :adverts, :through => :advProRel

  has_many :prop_vals

  belongs_to :prop_val
  belongs_to :property
end
