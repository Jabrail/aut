class Property < ActiveRecord::Base
  has_many :lang_values
  has_many :prop_vals
  belongs_to :group
end
