class Image < ActiveRecord::Base
  has_attached_file :image, styles: { big_image: "559x431#", medium: "263x188#", thumb: "260x171#" , icon: "100x60#" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
end
