class Lang < ActiveRecord::Base
  has_many :lang_values

  has_many :pages
  has_many :page_lang_rels, :through => :pages
end
