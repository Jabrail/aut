auto.factory("Users", function($resource) {
    return $resource("users/:id", { id: "@id" },
        {
            'create':  { method: 'POST' },
            'index':   { method: 'GET', isArray: false },
            'show':    { method: 'GET', isArray: false },
            'update':  { method: 'PUT', isArray: false  },
            'destroy': { method: 'DELETE' }
        }
    );
});