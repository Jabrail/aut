auto.factory("Filter", function($resource) {
    return $resource("properties/:id", { id: "@id" },
        {
            'create':  { method: 'POST' },
            'index':   { method: 'GET', isArray: false },
            'show':    { method: 'GET', isArray: false },
            'update':  { method: 'PUT', isArray: false  },
            'destroy': { method: 'DELETE' }
        }
    );
});