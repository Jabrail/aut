auto.factory("FilterValue", function($resource) {
    return $resource("prop_vals/:id", { id: "@id" },
        {
            'create':  { method: 'POST' },
            'add_sub':  { method: 'POST' , url: 'prop_vals/add_sub'},
            'index':   { method: 'GET', isArray: false },
            'getSub':   { method: 'GET', isArray: false, url: 'prop_vals/get_sub'},
            'show':    { method: 'GET', isArray: false },
            'update':  { method: 'PUT', isArray: false  },
            'destroy': { method: 'DELETE' }
        }
    );
});