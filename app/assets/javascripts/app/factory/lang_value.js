auto.factory("LangVal", function($resource) {
    return $resource("langs/:id", { id: "@id" },
        {
            'add_filter':  { method: 'POST', url: 'lang/add_filter' },
            'get_filters':   { method: 'GET', url: 'lang/get_filters/:id', isArray: false },
            'show':    { method: 'GET', isArray: false },
            'update':  { method: 'PUT', isArray: false  },
            'destroy': { method: 'DELETE' }
        }
    );
});