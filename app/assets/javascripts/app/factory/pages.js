auto.factory("Pages", function($resource) {
    return $resource("pages/:id", { id: "@id" },
        {
            'update_lang':  { method: 'POST', url: 'pages/update_lang/:id' },
            'create':  { method: 'POST' },
            'index':   { method: 'GET', isArray: false },
            'show':    { method: 'GET', isArray: true },
            'update':  { method: 'PUT', isArray: false  },
            'destroy': { method: 'DELETE' }
        }
    );
});