auto.factory("Group", function($resource) {
    return $resource("groups/:id", { id: "@id" },
        {
            'add_filter':  { method: 'GET' , url: '/add_filter' , isArray: false  },
            'remove_filter':  { method: 'GET' , url: '/remove_filter', isArray: false   },
            'create':  { method: 'POST' },
            'index':   { method: 'GET', isArray: false },
            'show':    { method: 'GET', isArray: false },
            'update':  { method: 'PUT', isArray: false  },
            'destroy': { method: 'DELETE' }
        }
    );
});