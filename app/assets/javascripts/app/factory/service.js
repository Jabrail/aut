auto.factory("Service", function($resource) {
    return $resource("services/:id", { id: "@id" },
        {
            'create':  { method: 'POST' },
            'add':  { method: 'POST', url:'service/add/:id'},
            'index':   { method: 'GET', isArray: false },
            'show':    { method: 'GET', isArray: false },
            'update':  { method: 'PUT', isArray: false  },
            'destroy': { method: 'DELETE' }
        }
    );
});