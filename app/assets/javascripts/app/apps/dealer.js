'use strict';

var auto = angular.module('auto', [
    'ngRoute',
    'ngResource',
    'angularFileUpload'
])

auto.config(function($httpProvider) {
    $httpProvider.defaults.headers.common['X-CSRF-Token'] =
        $('meta[name=csrf-token]').attr('content');
});

auto.config(['$routeProvider', '$httpProvider',
    function($routeProvider ) {
        $routeProvider.
            when('/', {
                templateUrl: "/assets/app/view/main_page/index.html",
                controller: 'HomeCtrl'
            }).
            when('/filters', {
                templateUrl: "/assets/app/view/filter/index.html",
                controller: 'FiltersCtrl'
            }).
            when('/filters/:id', {
                templateUrl: "/assets/app/view/filter/show.html",
                controller: 'ShowFiltersCtrl'
            }).
            when('/filters/:id/:v_id', {
                templateUrl: "/assets/app/view/filter/valRel.html",
                controller: 'VelRelCtrl'
            }).
            when('/languages', {
                templateUrl: "/assets/app/view/languages/index.html",
                controller: 'LanguagesCtrl'
            }).
            when('/groups', {
                templateUrl: "/assets/app/view/group/index.html",
                controller: 'GroupCtrl'
            }).
            when('/groups/:id', {
                templateUrl: "/assets/app/view/group/show.html",
                controller: 'GroupShowCtrl'
            }).
            when('/adverts', {
                templateUrl:  "assets/app/view/poster/index.html",
                controller: 'AdvertsCtrl'
            }).
            when('/adverts/new', {
                templateUrl:  "assets/app/view/poster/new.html",
                controller: 'NewAdvertCtrl'
            }).
            otherwise({
                redirectTo: '/'
            });

    }
]);

auto.controller('MenuCtrl',['$scope', '$location'
    , function ($scope, $location ) {

        $scope.getClass = function (path) {
            if ($location.path().substr(0, path.length) === path) {
                return 'active';
            } else {
                return '';
            }
        }

    }]);

