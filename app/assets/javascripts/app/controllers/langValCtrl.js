"use strict";

auto.controller('LangValCtrl',
    function ($scope, LangVal, $stateParams , $state) {

        $scope.actions = {
            show: 'true',
            edit: 'true',
            remove: 'true'
        }
        LangVal.get_filters({id: $stateParams.id }).$promise.then(function(data) {

            $scope.items = data.langs;

        });

             $scope.ckEditors = [];
            $scope.addEditor = function(){
                var rand = ""+(Math.random() * 10000);
                $scope.ckEditors.push({value:rand});
            }

        // Called when the editor is completely ready.
        $scope.onReady = function () {
            // ...
        };

        $scope.save = function() {

            LangVal.add_filter({items: $scope.items}).$promise.then(function() {

                $state.go('app.auto-filter-show' , {id: $stateParams.id})

            })

        }

    });