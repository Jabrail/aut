"use strict";

auto.controller('FiltersCtrl'
    , function ($scope, $http, Filter, $state,  $rootScope, $modal, $sce) {

        $scope.new_filter = {}

        $scope.actions = {
            show: 'true',
            edit: 'true',
            remove: 'true'
        }

        $scope.openModal = function(modal_id, modal_size, modal_backdrop)
        {
            $rootScope.currentModal = $modal.open({
                templateUrl: modal_id,
                size: modal_size,
                scope: $scope,
                backdrop: typeof modal_backdrop == 'undefined' ? true : modal_backdrop
            });
        };

        $scope.update_filter = {}

        Filter.index().
            $promise.then(function(data) {

                $scope.items = data.filters.reverse()
                $scope.values = data.vals;

            });

        $scope.type_options = [
            {
                name: 'Text',
                value: '1'
            },
            {
                name: 'Checkbox',
                value: '2'
            },
            {
                name: 'Radio',
                value: '3'
            },
            {
                name: 'Select',
                value: '4'
            },
            {
                name: 'Color',
                value: '5'
            },
            {
                name: 'Interval',
                value: '6'
            },
            {
                name: 'Image',
                value: '7'
            },
            {
                name: 'Mark',
                value: '10'
            },
            {
                name: 'Model',
                value: '11'
            },
            {
                name: 'Year',
                value: '12'
            },
            {
                name: 'Country',
                value: '13'
            },
            {
                name: 'boolean',
                value: '14'
            }
        ];

        $scope._create = function() {

            Filter.create({'property': { name: $scope.new_filter.name , 'type_value' : $scope.new_filter.type_value.value }}).
                $promise.then(function(data) {
                    $scope.items.unshift(data);
                    $rootScope.currentModal.close();
                })

        }

        $scope._remove = function(filter) {

            Filter.destroy({id: filter.id})
            $scope.items.splice($scope.items.indexOf(filter), 1);

        }

        $scope.showEdit = function(filter) {

            $scope.update_filter = filter;
            $scope.update_filter.type_value = $scope.type_options[Number(filter.type_value) - 1];

            $rootScope.currentModal = $modal.open({
                templateUrl: 'updateModel',
                size: 'undefined',
                backdrop: typeof modal_backdrop == 'undefined' ? true : modal_backdrop,
                scope: $scope
            });



        }

        $scope._edit = function() {

            Filter.update({id : $scope.update_filter.id, 'property' : { name: $scope.update_filter.name , 'type_value' : $scope.update_filter.type_value.value, 'valid_data' : $scope.update_filter.valid_data }}).
                $promise.then(function(data) {

                    $scope.update_filter.type_value = Number($scope.update_filter.type_value.value);

                    $rootScope.currentModal.close()

                })
        }

        $scope.show = function(filter) {

            $state.go('app.auto-filter-show' , {id: filter.id})

        }

    });