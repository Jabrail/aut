"use strict";

auto.controller('PageShowCtrl',
    function ($scope, Pages, Lang, $stateParams) {

        Pages.show({id: $stateParams.id}).$promise.then(function(data){

            $scope.langs = data;
            $scope.current_lang = $scope.langs[0]

        })



        $scope.setLang = function(lang) {

            $scope.current_lang = lang

        }

        $scope.updatePage = function() {
            $scope.current_lang.text = CKEDITOR.instances.editor1.getData();
            Pages.update_lang({id: $stateParams.id, page:$scope.current_lang}).$promise.then(function(data){


            })

        }

    });