"use strict";

auto.controller('LanguagesCtrl', function ($scope, $http, Lang, $rootScope, $modal, $sce) {


    $scope.new_language = {};

    $scope.actions = {
        show: 'false',
        edit: 'true',
        remove: 'true'
    };


    $scope.openModal = function(modal_id, modal_size, modal_backdrop)
    {
        $rootScope.currentModal = $modal.open({
            templateUrl: modal_id,
            size: modal_size,
            backdrop: typeof modal_backdrop == 'undefined' ? true : modal_backdrop,
            scope: $scope
        });
    };

    Lang.index().
        $promise.then(function(data) {

            $scope.items = data.langs.reverse();
            $scope.values = data.vals;

        });

    $scope._create = function() {

        Lang.create({'lang': { short_name: $scope.new_language.short_name , 'long_name' : $scope.new_language.long_name }}).
            $promise.then(function(data) {
                $scope.items.unshift(data);
                $rootScope.currentModal.close()
             })

    }

    $scope._remove = function(language) {

        Lang.destroy({id: language.id})
        $scope.items.splice($scope.items.indexOf(language), 1);

    }

    $scope.showEdit = function(languag) {

        $scope.update_language = languag;

        $rootScope.currentModal = $modal.open({
            templateUrl: 'updateModel',
            size: 'undefined',
            backdrop: typeof modal_backdrop == 'undefined' ? true : modal_backdrop,
            scope: $scope
        });

    }

    $scope._edit = function() {

        Lang.update({id : $scope.update_language.id, 'lang' : { short_name: $scope.update_language.short_name , 'long_name' : $scope.update_language.long_name }}).
            $promise.then(function(data) {

                $rootScope.currentModal.close()
            })

    }


});