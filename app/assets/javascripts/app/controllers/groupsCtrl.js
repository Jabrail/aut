"use strict";

auto.controller('GroupCtrl', function ($scope, $http, Group, $state ,  $rootScope, $modal, $sce) {

    $scope.new_group = {}

    $scope.actions = {
        show: 'true',
        edit: 'true',
        remove: 'true'
    }

    $scope.openModal = function(modal_id, modal_size, modal_backdrop)
    {
        $rootScope.currentModal = $modal.open({
            templateUrl: modal_id,
            size: modal_size,
            backdrop: typeof modal_backdrop == 'undefined' ? true : modal_backdrop,
            scope : $scope
        });
        $scope.$apply()
    };


    Group.index().
        $promise.then(function(data) {

            $scope.items = data.groups.reverse();
            $scope.values = data.vals;

        });

    $scope._create = function() {

        Group.create({'group': { name: $scope.new_group.name }}).
            $promise.then(function(data) {
                $scope.items.unshift(data);
                $rootScope.currentModal.close()
             })

    }

    $scope._remove = function(group) {

        Group.destroy({id: group.id})
        $scope.items.splice($scope.items.indexOf(group), 1);

    }

    $scope.showEdit = function(group) {

        $scope.update_group = group;
        $('#updateModel').modal('show');

    }

    $scope._edit = function() {

        Group.update({id : $scope.update_group.id, 'group' : { name: $scope.update_group.name  }}).
            $promise.then(function(data) {

                $('#updateModel').modal('hide');
            })

    }


    $scope.show = function(group) {

        $state.go('app.auto-groups-show' , {id: group.id})

    }

});