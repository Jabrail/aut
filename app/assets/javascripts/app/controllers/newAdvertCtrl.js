"use strict";

auto.controller('NewAdvertCtrl',
    function ($scope, $http , Filters, Advert , $location) {

        var images = [];
        var prop_vals = [];
        var i = 1,
            $example_dropzone_filetable = $("#example-dropzone-filetable"),
            example_dropzone = $("#advancedDropzone").dropzone({
                url: '/images',
                method: 'POST',

                // Events
                addedfile: function(file)
                {
                    if(i == 1)
                    {
                        $example_dropzone_filetable.find('tbody').html('');
                    }

                    var size = parseInt(file.size/1024, 10);
                    size = size < 1024 ? (size + " KB") : (parseInt(size/1024, 10) + " MB");

                    var $entry = $('<tr>\
										<td class="text-center">'+(i++)+'</td>\
										<td>'+file.name+'</td>\
										<td style="width: 100px"><div class="progress progress-striped"><div class="progress-bar progress-bar-warning"></div></div></td>\
										<td  style="width: 100px">'+size+'</td>\
										<td class="actions"> </td>\
										<td style="width: 100px">Uploading...</td>\
									</tr>');

                    $example_dropzone_filetable.find('tbody').append($entry);
                    file.fileEntryTd = $entry;
                    file.progressBar = $entry.find('.progress-bar');
                    file.actions = $entry.find('.actions');
                },

                uploadprogress: function(file, progress, bytesSent)
                {
                    file.progressBar.width(progress + '%');
                },

                success: function(file, response)
                {
                    images.push(response.id)
                    file.fileEntryTd.find('td:last').html('<span class="text-success">Uploaded</span>');
                    file.actions.html('<button type="button" class="btn btn-danger btn-sm"   ng-click="remove_image(response.id)">Remove</button>');
                    file.progressBar.removeClass('progress-bar-warning').addClass('progress-bar-success');
                    $scope.$apply();
                },

                error: function(file)
                {
                    file.fileEntryTd.find('td:last').html('<span class="text-danger">Failed</span>');
                    file.progressBar.removeClass('progress-bar-warning').addClass('progress-bar-red');
                }
            });

        $("#advancedDropzone").css({
            minHeight: 200
        });


        Filters.get_filter_for_new().$promise.then(function(data) {

            $scope.filter = data.filters;
            $scope.filter.mark._selected = '0'
            $scope.filter.additional.selected = []

        })


        $scope._change = function(filter) {

            findById(prop_vals, filter.id).value.sub = filter.__selected

         }

        $scope.setInput = function(filter) {

            var val = findById(prop_vals, filter.id)

            if (val) {
                val.value.value = filter.selected;
            } else {
                val = { id: filter.id,
                    type: filter.type_value,
                    value: {
                        id: filter.prop_vals[0].id,
                        value: filter.selected
                    }
                }
                prop_vals.push(val)
            }
        }

        $scope.change = function(filter) {
            filter.selected_sub = JSON.parse(filter._selected);
            filter.selected = filter.selected_sub.id;
            filter.__selected = '0';

            removeById(prop_vals, filter.id);
            prop_vals.push({ id: filter.id,
                type: filter.type_value,
                value: {
                    base: filter.selected,
                    sub: 0
                }
            });
        }

        $scope.setValue = function(value) {


        }

        $scope.set_model = function () {
        }

        $scope.setRadio = function(filter, val) {
            filter.selected = val.id
        };

        $scope.setSelect = function(filter) {

            var val = findById(prop_vals, filter.id);

            if (val) {
                val.value.value =  filter.selected
            } else {
                val = { id: filter.id,
                    type: filter.type_value,
                    value: {
                        value: filter.selected
                    }
                };
                prop_vals.push(val);
            }
        };

        $scope.setCheckboxSingl = function(filter) {

            var val = findById(prop_vals, filter.id);
            if (val) {
                prop_vals.splice( prop_vals.indexOf(val) , 1)
            } else {
                val = { id: filter.id,
                    type: filter.type_value,
                    value: {
                        value: filter.prop_vals[0].id
                    }
                };
                prop_vals.push(val);
            }

        };

        $scope.setCheckbox = function(filter, val) {

            if (filter.selected.indexOf(val.id) > -1) {

                filter.selected.splice(filter.selected.indexOf(val.id), 1);

            }
            else {
                filter.selected.push(val.id)
            }

            var val = findById(prop_vals, filter.id);

            if (val) {
                val.value = filter.selected
            } else {
                val = { id: filter.id,
                    type: filter.type_value,
                    value: filter.selected
                };
                prop_vals.push(val);
            }

        };

        $scope.create = function(type) {



            if (type == 'top') {
                Advert.create({
                    poster: {description: $scope.advert.desc, price: $scope.advert.price},
                    set_top: 1,
                    currency: $scope.advert.currency,
                    filters: prop_vals
                }).$promise.then(function () {
                        $location.path('/adverts/')
                    })
            } else {

                Advert.create({
                    poster: {description: $scope.advert.desc, price: $scope.advert.price},
                    set_top: 0,
                    images: images,
                    currency: $scope.advert.currency,
                    filters: prop_vals
                }).$promise.then(function () {
                        $location.path('/adverts/')
                    })

            }

        }

        $scope.remove_image = function (id) {

            return alert(id)
        }


        function findById(arr, id) {

            var res_item = false;
            arr.forEach(function(item) {

                if ( item.id == id ) {
                    res_item = item
                }

            });

            return res_item
        }

        function removeById(arr, id) {

            arr.forEach(function(item) {

                if ( item.id == id ) {
                    arr.splice( arr.indexOf(item) , 1)
                }

            });

        }

    }
);

