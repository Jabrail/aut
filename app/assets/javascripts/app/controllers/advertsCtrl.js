"use strict";

auto.controller('AdvertsCtrl',
    function ($scope, $http, Advert ,  $rootScope, $modal, $sce) {

        $scope.actions = {
            show: 'true',
            edit: 'true',
            remove: 'true'
        }

        $scope.openModal = function(modal_id, modal_size, modal_backdrop)
        {
            $rootScope.currentModal = $modal.open({
                templateUrl: modal_id,
                size: modal_size,
                backdrop: typeof modal_backdrop == 'undefined' ? true : modal_backdrop,
                scope : $scope
            });
            $scope.$apply()
        };

        Advert.index().$promise.then(function(data) {
            $scope.items = data.adverts.reverse();
            $scope.values = data.vals;
            $scope.poster_count = data.poster_count
        })

        $scope.setTop = function(advert) {

            Advert.setTop({ id: advert.id , set_top: 1}).$promise.then(function(data) {
                advert.service = 1
            })

        }

        $scope._remove = function(advert) {

            Advert.destroy({ id: advert.id}).$promise.then(function(data){

                $scope.items.splice($scope.items.indexOf(advert), 1);


            })

        }
    });