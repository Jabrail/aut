"use strict";

auto.controller('GroupShowCtrl',
    function ($scope, $http, Filter, Group, $stateParams,  $rootScope, $modal, $sce ) {

        $scope.new_filter = 0;

        Filter.index().
            $promise.then(function(data){
                $scope.type_options = data.filters
             })

        $scope.actions = {
            show: 'false',
            edit: 'false',
            remove: 'true'
        }

        $scope.openModal = function(modal_id, modal_size, modal_backdrop)
        {
            $rootScope.currentModal = $modal.open({
                templateUrl: modal_id,
                size: modal_size,
                backdrop: typeof modal_backdrop == 'undefined' ? true : modal_backdrop,
                scope : $scope
            });
            $scope.$apply()
        };

        Group.show({id: $stateParams.id }).
            $promise.then(function(data) {

                $scope.items = data.filters.reverse();
                $scope.values = data.vals;

            })

        $scope.addFilter = function (fl) {

            Group.add_filter({p_id: fl , g_id: $stateParams.id}).$promise.then(function(data) {

                $scope.items.unshift(data);
                $rootScope.currentModal.close()

            })

        }
        $scope._remove = function (filter) {

            Group.remove_filter({p_id: filter.id , g_id: $stateParams.id})
            $scope.items.splice($scope.items.indexOf(filter), 1);

        }
    });