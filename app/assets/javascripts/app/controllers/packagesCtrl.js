"use strict";

auto.controller('PackagesCtrl',['$scope', '$http', 'Service'
    , function ($scope, $http, Service ) {

        Service.index().$promise.then(function(data) {
            $scope.services = data.reverse()
        })


        $scope._add = function(service) {

            Service.add({id: service.id }).$promise.then(function() {

                service.on = 1

            })

        }
    }]);