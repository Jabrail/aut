
"use strict";

auto.controller('UsersCtrl', function ($scope, Users ) {

    Users.index().
        $promise.then(function(data) {

            $scope.items = data.users.reverse();
            $scope.values = data.vals;

        });

});