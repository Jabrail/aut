"use strict";

auto.controller('PagesCtrl',
    function ($scope, Pages, $state) {

        $scope.actions = {
            show: 'true',
            edit: 'false',
            remove: 'false'
        }

        Pages.index().$promise.then(function(data) {
            $scope.items = data.pages.reverse();
            $scope.values = data.vals;
        })

        $scope.show = function(page) {

            $state.go('app.auto-page-show' , {id: page.id})

        }

    });