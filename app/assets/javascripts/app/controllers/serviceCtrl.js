"use strict";

auto.controller('ServiceCtrl',
    function ($scope, $http, Service ,  $rootScope, $modal, $sce) {


        $scope.openModal = function(modal_id, modal_size, modal_backdrop)
        {
            $rootScope.currentModal = $modal.open({
                templateUrl: modal_id,
                size: modal_size,
                backdrop: typeof modal_backdrop == 'undefined' ? true : modal_backdrop
            });
        };

        Service.index().$promise.then(function(data) {
            $scope.items = data.services.reverse()
            $scope.values = data.vals
        })

        $scope._create = function() {
            Service.create({service :$scope.new_service}).$promise.then(function(data){

                $scope.services.unshift(data);
                $('#createModel').modal('hide');

            })
        }

        $scope.update_model = function(servise) {

            $scope.update_service = servise;
            $('#updateModel').modal('show');

        }

        $scope._edit = function() {

            Service.update({id: $scope.update_service.id, service :$scope.update_service}).$promise.then(function(data){

                $('#updateModel').modal('hide');

            })

        }

        $scope._remove = function(service) {

            Service.destroy({ id: service.id}).$promise.then(function(data){

                $scope.services.splice($scope.services.indexOf(service), 1);


            })
        }

    });