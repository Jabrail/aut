"use strict";

auto.controller('ShowFiltersCtrl',
    function ($scope, $http, FilterValue, $state, $stateParams ,  $rootScope, $modal, $sce) {

        $scope.props = true;
        $scope.langs = false;
        $scope.new_value = {};


        $scope.actions = {
            show: 'true',
            edit: 'true',
            remove: 'true'
        }

        $scope.openModal = function(modal_id, modal_size, modal_backdrop)
        {
            $rootScope.currentModal = $modal.open({
                templateUrl: modal_id,
                size: modal_size,
                scope: $scope,
                backdrop: typeof modal_backdrop == 'undefined' ? true : modal_backdrop
            });
        };

        $scope.openLang = function() {

            $state.go('app.auto-filter-show-lang' , {id: $stateParams.id})

        };

        FilterValue.index({p_id: $stateParams.id}).
            $promise.then(function(data) {

                $scope.items = data.prop_vals.reverse()
                $scope.values = data.vals

            });

        $scope._create = function() {

            var fd = new FormData();
            var image =  document.getElementById('file_val').files[0];
            if (image != null ) {
                fd.append('prop_val[image]', image);
            }
            fd.append('prop_val[title]',  $scope.new_value.title );
            fd.append('prop_val[color]',  $scope.new_value.color);
            fd.append('p_id',  $stateParams.id );

            var xhr = new XMLHttpRequest();
            xhr.open("POST", "prop_vals"); // Boooom!
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))

            xhr.onload = function(event) {

                var jsonObj = JSON.parse(this.responseText);
                $scope.items.unshift(jsonObj);
                $rootScope.currentModal.close();
                $scope.$apply();

            }

            xhr.send(fd);

        }

        $scope._remove = function(value) {

            FilterValue.destroy({id: value.id})
            $scope.items.splice($scope.items.indexOf(value), 1);

        }

        $scope.showEdit = function(value) {

            $scope.update_value = value;
            $('#updateModel').modal('show');

        }

        $scope._edit = function() {

            var fd = new FormData();
            var image =  document.getElementById('file_val_update').files[0];
            if (image != null ) {
                fd.append('prop_val[image]', image);
            }
            fd.append('prop_val[title]',  $scope.update_value.title );
            fd.append('prop_val[color]',  $scope.update_value.color);
            fd.append('p_id',  $stateParams.id );

            var xhr = new XMLHttpRequest();
            xhr.open("PUT", "prop_vals/" + $scope.update_value.id);
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))

            xhr.onload = function(event) {

                $scope.update_value.image = JSON.parse(this.responseText).image
                $('#updateModel').modal('hide')
                $scope.$apply()

            }

            xhr.send(fd);



        }

        $scope.setTab = function (page) {

            switch (page) {
                case 0:
                    $scope.props = true;
                    $scope.langs = false;
                    break
                case 1:
                    $scope.props =false;
                    $scope.langs = true;
                    break
                default:
                    break
            }

        }

        $scope.show = function(prop_val) {

            $state.go('app.auto-filter-subVel' , {id: prop_val.id})

        }


    });