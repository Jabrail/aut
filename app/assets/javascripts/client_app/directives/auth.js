auto.directive('auth', [   'User' , '$http' , '$location' , function(User, $http, $location){
	return {
		restrict: 'E',
		templateUrl: appHelper.templatePath('admTpls/auth'),
		link: function(scope, el) {
			$(el).hide();

			scope.user = {};

			scope.hideLogin = function() {
				scope.showAuth = false
			}
			scope.$watch(
					"showAuth",
					function handleFooChange( newValue, oldValue ) {
						scope.pageLoaded = false;
						if (scope.showAuth) {
							$(el).show()

							$http({
								method: 'GET',
								url: '/users/sign_in'
							}).then(function(data) {

								$(el).find('.login_box').empty();
								$(el).find('.login_box').append(data.data);

								$http({
									method: 'GET',
									url: '/users/sign_up'
								}).then(function(data) {
									scope.pageLoaded = true;
									$(el).find('.registration_box').empty();
									$(el).find('.registration_box').append(data.data);
								}, function(response) {
									// called asynchronously if an error occurs
									// or server returns response with an error status.
								});

							}, function(response) {
								// called asynchronously if an error occurs
								// or server returns response with an error status.
							});


						} else {
							$(el).hide(500)
						}
					}
			);

			scope.auth = function(form) {

				User.login({'user[email]' : scope.user.email, 'user[password]' : scope.user.password}).

				$promise.then(function(data) {

					User.get_user().$promise.then(function(data) {
						scope.login = data.status
						if (scope.login) {
							$(el).hide();
							$location.path('/profileMain')
						}
					});

				})

			}
			scope.registration = function() {

				User.registration({'user[email]' : scope.user.email, 'user[password]' : scope.user.password,
					'user[password_confirmation]' : scope.user.password_confirmation}).
				$promise.then(function(data) {

					User.get_user().$promise.then(function(data) {
						scope.login = data.status
						if (scope.login) $(el).hide()
					});

				})

			}
		}

	};
}])