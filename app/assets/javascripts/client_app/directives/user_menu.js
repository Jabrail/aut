auto.directive('userMenu', [ '$location' , function( $location){
	return {
		restrict: 'E',
		templateUrl: appHelper.templatePath('admTpls/user_menu'),
		link: function(scope, el) {

			scope.paths = [
				{href: '/profileMain', title: "Мои объявления" },
				{href: '/profileService', title: "Услуги" },
				{href: '/profileSettings', title: "Настройки" }
			]

			scope.currentActive = function(path) {
				if (path === $location.path()) return true
			}
			scope.opentPath = function(path) {

				$location.path(path)

			}
		}

	};
}])