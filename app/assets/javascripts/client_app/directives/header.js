
auto.directive('header', [ 'Pages' , '$location' , 'User',  function(Pages, $location, User){
	return {
		restrict: 'E',
		templateUrl: appHelper.templatePath('admTpls/header'),
		link: function(scope, el) {

			scope.login = false;
			scope.showAuth = false;

			Pages.get_pages().$promise.then(function (data) {
				scope.pages = data.pages;
			});

			User.get_user().$promise.then(function(data) {
				scope.login = data.status
			});

			scope.goToPage = function(href) {
				$location.path('/page/'+href);
			};
			scope.goToPath = function(href) {
				$location.path(href);
			};
			scope.showRegistration = function() {
				scope.showAuth = true;
				scope.showReg = true;
			};
			scope.showLogin = function() {
				scope.showAuth = true
				scope.showReg = false
			}
			scope.logout = function() {
				User.logout().$promise.then(function(){
						scope.login = false
						$location.path('/')
				})
			}

			scope.currentPath = function(path) {

				if ($location.path() == path) {
					return true
				} else {
					return false
				}
			}

		}

	};
}])