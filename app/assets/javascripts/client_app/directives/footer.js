auto.directive('footer', [ 'Advert' , '$location' , function(Advert, $location){
	return {
		restrict: 'E',
		templateUrl: appHelper.templatePath('admTpls/footer'),
		link: function(scope, el) {

			scope.marks = Advert.get_marks();

			scope.opentSearch = function(mark) {

				$location.path('/search/mark/'+mark.id)

			}
		}

	};
}])