auto.factory("Advert", function($resource) {
    return $resource("/posters/:id", { id: "@id" },
        {
            'create':  { method: 'POST' },
            'get_user_poster':{ method: 'GET', isArray: true, url: "/api/get_user_poster" },
            'get_by_prop':{ method: 'POST', isArray: true, url: "/api/get_by_prop" },
            'get_poster':{ method: 'GET', isArray: false, url: "/api/get_poster/:id" },
            'index':   { method: 'GET', isArray: true },
            'show':    { method: 'GET', isArray: false },
            'update':  { method: 'PUT', isArray: false  },
            'destroy': { method: 'DELETE' , isArray: false },
            'get_last' : {method: 'GET', isArray: true, url: "/api/last_poster/:id"},
            'get_marks' : {method: 'GET', isArray: true, url: "/api/get_marks/"},
            'count' : {method: 'GET', isArray: false, url: "/api/count_poster/"}

        }
    );
});