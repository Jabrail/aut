auto.factory("User", function($resource) {
	return $resource("/api/get_user", { name: "@name" },
		{
			'get_user':   { method: 'GET', isArray: false },
			'login':   { method: 'POST', isArray: false, url: '/users/sign_in' },
			'logout':   { method: 'DELETE', isArray: false, url: '/users/sign_out' },
			'registration':   { method: 'POST', isArray: false, url: '/users' }
		}
	);
});