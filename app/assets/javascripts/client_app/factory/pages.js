auto.factory("Pages", function($resource) {
	return $resource("/api/get_pages/:name", { name: "@name" },
		{
			'get_pages':  { method: 'GET' , isArray: false },
			'get_page':  { method: 'GET' , isArray: false }
		}
	);
});