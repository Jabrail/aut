auto.factory("Filter", function($resource) {
    return $resource("/api/:action", { action: "@action" },
        {
            'get_filters': { method: 'GET' , params: { 'action' : 'get_filters'}, isArray: false },
            'get_filter_for_new': { method: 'GET' , params: { 'action' : 'get_filters_for_new'}, isArray: false },

            'index':   { method: 'GET', isArray: true },
            'show':    { method: 'GET', isArray: false },
            'update':  { method: 'PUT', isArray: false  },
            'destroy': { method: 'DELETE' }
        }
    );
});

