"use strict";

auto.controller('SearchCtrl',['$scope', '$http', 'uiGmapLogger',  'uiGmapGoogleMapApi', 'Filter', 'Advert', '$rootScope', '$routeParams'
    , function ($scope, $http , $log,  GoogleMapApi, Filter, Advert, $rootScope, $routeParams) {




        $scope.orders = [
            { value: '0' , title: 'Упорядочить по:'},
            { value: '1' , title: 'Цене'},
            { value: '2' , title: 'Дате добавления'}
        ];
        $scope.order_desc = $scope.orders[0];

        $scope._change = function(filter) {

            filter.selected = filter.__selected.id;

            sendFilters();

        }

        Advert.count().$promise.then(function(data) {
            $scope.advert_count = data.count
        })

        $scope.change = function(filter) {

            filter.selected_sub = filter._selected;
            filter.selected = filter.selected_sub? filter.selected_sub.id : '0';
            filter.__selected = '0';

            sendFilters();

        };

        Filter.get_filters().$promise.then(function (data) {
            var jsonYear = ''
            var jsonMileage = ''

            $scope.filter = data.filters;
            $scope.filter.year.selected = '0'
            $scope.filter.state.selected = '0'
            $scope.filter.additional.selected = [];
            $scope.filter.mark.default = 'Выберите марку';
            $scope.filter.country.default = 'Выберите страну';
            $scope.filter.mark.title_sub = 'Выберите модель';
            $scope.filter.country.title_sub = 'Выберите город';

            jsonYear = JSON.stringify(data.filters.year);
            jsonMileage = JSON.stringify(data.filters.mileage);
            $scope.year = {
                before: JSON.parse(jsonYear),
                after: JSON.parse(jsonYear)
            };
            $scope.mileage = {
                before: JSON.parse(jsonMileage),
                after: JSON.parse(jsonMileage)
            };

            $scope.year.before.title = "Год от:";
            $scope.year.after.title = "Год до:";
            $scope.mileage.before.title = "Пробег от:";
            $scope.mileage.after.title = "Пробег до:";

            if ($rootScope.filters) {
                $scope.filter = $rootScope.filters;
            }


            if ($routeParams.id) {

                $scope.filter.mark._selected = $scope.filter.mark.prop_vals.filter(function (item) {

                    return item.id == $routeParams.id
                })[0];
                $scope.change($scope.filter.mark)
            }

            $scope.filter_loaded = true;
            sendFilters()
        });



        $scope.setSelect = function(filter) {
            filter.selected = filter._selected.id.toString();
            sendFilters();

        }

        $scope.setState = function(filter, val) {

            var elem;

            if (filter.selected && filter.selected != '0') {
                elem = filter.selected.filter(function (item) {
                    return item == val.id
                })[0]

                if (elem) {
                    filter.selected.splice(filter.selected.indexOf(val.id), 1)
                } else {
                    filter.selected.push(val.id);
                }
            } else {
                filter.selected = [];
                filter.selected.push(val.id);

            }

            sendFilters();

        }

        $scope.changrDesc = function(desc) {
            console.log($scope.order_desc)
            sendFilters()
        }



        function sendFilters() {

            var filtersArray = [];

            for (var fl in $scope.filter) {

                if ($scope.filter[fl].selected) {
                    if ($scope.filter[fl].selected != "0") {
                        if ($scope.filter[fl].type_value == 10) {
                            if ($scope.filter[fl].__selected != '0') {
                                filtersArray.push( { id: $scope.filter[fl].id , value: $scope.filter[fl].selected_sub.id,  value_sub: Number($scope.filter[fl].selected)});
                            } else {
                                filtersArray.push( { id: $scope.filter[fl].id , value: $scope.filter[fl].selected});
                            }
                        } else {
                            filtersArray.push( { id: $scope.filter[fl].id , value: $scope.filter[fl].selected});
                        }
                    } else if ($scope.filter[fl].selected_sub){
                        filtersArray.push( { id: $scope.filter[fl].id , value: Number($scope.filter[fl].selected_sub.id)});
                    }
                }

            }


            if ($scope.year.before.selected != '0' || $scope.year.after.selected != '0') {
                var before = ($scope.year.before.selected  != '0') ? $scope.year.before.selected : '10000000000000000'
                var after = ($scope.year.after.selected  != '0') ? $scope.year.after.selected : '0'
                filtersArray.push( { id: $scope.filter.year.id , value_befor: before, value_after: after});

            }

            if ($scope.mileage.before.selected != '0' || $scope.mileage.after.selected !='0') {
                var before = ($scope.mileage.before.selected != '0') ? $scope.mileage.before.selected : $scope.mileage.before.prop_vals[0].id;
                var after = ($scope.mileage.after.selected != '0') ? $scope.mileage.after.selected : $scope.mileage.before.prop_vals[$scope.mileage.before.prop_vals.length-1].id;

                filtersArray.push( { id: $scope.filter.mileage.id , value_befor: before, value_after: after});
            }


            Advert.get_by_prop({pops: filtersArray, desc: $scope.order_desc.value}).$promise.then(function(data){
                $scope.adverts = data
            })
        }

    }]);