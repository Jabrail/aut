"use strict";

auto.controller('DetailCtrl',
    function ($scope, $http, $routeParams, Advert , $interval) {

        var firstLoad = true;
        $scope.showGallery = false;

        $scope.bigImage = false;

        $scope.showBigImage = function(i) {
            $scope.bigImage  = true;

            if (firstLoad) {
                firstLoad = false;
                $scope.numberLoadedNav = true;
                $scope.numberLoadedBig = true;

                $interval(function() {

                    $scope.slickConfigBig = {
                        initialSlide: i,
                        draggable: false,
                        slidesToShow: 1,
                        infinite: true,
                        dots: false,
                        centerMode: true,
                        variableWidth:true,
                        adaptiveHeight:true,
                        asNavFor: '.slider-nav',
                        nextArrow: '<button type="button" data-role="none" class="slick-next navigator" aria-label="next"><span class="icon-arrow_right"></span></button>   ',
                        prevArrow: '<button type="button" data-role="none" class="slick-prev navigator" style="z-index: 1000;" aria-label="previous"><span class="icon-arrow_left"></span></button>'
                    };

                    $scope.slickConfigNav = {
                        slidesToShow: 9,
                        slidesToScroll: 4,
                        infinite: false,
                        asNavFor: '.slider-for',
                        dots: false,
                        arrows: false,
                        centerMode: true,
                        centerPadding: '20px',
                        focusOnSelect: true,
                        variableWidth:true
                    };
                }, 400);
            }

        }
        $scope.hideBigImage = function() {
            $scope.bigImage  = false;
            $scope.numberLoadedNav = false;
            $scope.numberLoadedBig = false;
        }

        Advert.get_poster({id: $routeParams.id}).$promise.then(function(data){

            var stop;
            $scope.advert = data;

            $scope.numberLoaded = true;

            stop = $interval(function() {



                $scope.slickConfig = {
                    draggable: false,
                    slidesToShow: 1,
                    infinite: true,
                    dots: true,
                    centerMode: true,
                    variableWidth:true,
                    adaptiveHeight:true,
                    nextArrow: '<button type="button" data-role="none" class="slick-next navigator" aria-label="next"><span class="icon-arrow_right"></span></button>   ',
                    prevArrow: '<button type="button" data-role="none" class="slick-prev navigator" style="z-index: 1000;" aria-label="previous"><span class="icon-arrow_left"></span></button>',
                    responsive: [
                        {
                            breakpoint:1200,
                            settings: {
                                arrows:false,
                                centerMode:false,
                                variableWidth:false,
                                slidesToShow:1,
                                dots:true,
                                adaptiveHeight:true,
                                draggable:true
                            }
                        }
                    ],

                    method: {}
                };


            }, 400);
        });
    });

