"use strict";

auto.controller('HomeCtrl',
    function ($scope, $http, Advert , Filter, $rootScope, $location, User) {

        var filtersArray = [];


        Filter.get_filters().$promise.then(function(data) {


            $scope.filter = data.filters;
            $scope.filter.mark.default = 'Выберите марку';
            $scope.filter.country.default = 'Выберите страну';
            $scope.filter.mark.title_sub = 'Выберите модель';
            $scope.filter.country.title_sub = 'Выберите город';
            $scope.filter.year.selected = '0';
            $scope.filter.mileage.selected = '0';
            $scope.filter.state.selected = '0';
            $scope.filter.additional.selected = [];

            sendFilters();

        });


        $scope._change = function(filter) {

            filter.selected = filter.__selected.id;

            sendFilters();

        }

        $scope.change = function(filter) {

            filter.selected_sub = filter._selected;
            filter.selected = filter.selected_sub? filter.selected_sub.id : '0';
            filter.__selected = '0';

            sendFilters();

        };

        $scope.setSelect = function(filter) {
            filter.selected = filter._selected.id;
            sendFilters();

        }

        $scope.setState = function(filter, val) {

            var elem;

            if (filter.selected && filter.selected != '0') {
                elem = filter.selected.filter(function (item) {
                    return item == val.id
                })[0]

                if (elem) {
                    filter.selected.splice(filter.selected.indexOf(val.id), 1)
                } else {
                    filter.selected.push(val.id);
                }
            } else {
                filter.selected = [];
                filter.selected.push(val.id);

            }

            sendFilters();

        };

        $scope.openSearch = function () {

            $rootScope.filters = $scope.filter;
            console.log($scope.filter)
            console.log($rootScope.filters)
            $location.path('/search')

        }


        function sendFilters() {

            var filtersArray = [];


            for (var fl in $scope.filter) {

                if ($scope.filter[fl].selected) {
                    if ($scope.filter[fl].selected != "0") {
                        if ($scope.filter[fl].type_value == 10) {
                            if ($scope.filter[fl].__selected != '0') {
                                filtersArray.push( { id: $scope.filter[fl].id , value: $scope.filter[fl].selected_sub.id,  value_sub: Number($scope.filter[fl].selected)});
                            } else {
                                filtersArray.push( { id: $scope.filter[fl].id , value: $scope.filter[fl].selected});
                            }
                        } else {
                            filtersArray.push( { id: $scope.filter[fl].id , value: $scope.filter[fl].selected});
                        }
                    } else if ($scope.filter[fl].selected_sub){
                        filtersArray.push( { id: $scope.filter[fl].id , value: Number($scope.filter[fl].selected_sub.id)});
                    }
                }

            }

            Advert.get_by_prop({pops: filtersArray}).$promise.then(function(data){
                $scope.adverts_by_search = data
            })
        }

        Advert.get_last({id: 10}).$promise.then(function (data) {

            $scope.adverts = data;


            $scope.slickConfig = {


                infinite: false,

                speed: 300,

                slidesToShow: 1
            };
            $scope.responsiveConfig = {
                dots: true,

                infinite: false,

                speed: 300,

                slidesToShow: 4,

                slidesToScroll: 4,

                responsive: [

                    {

                        breakpoint: 1024,

                        settings: {

                            slidesToShow: 3,

                            slidesToScroll: 3,

                            infinite: true,

                            dots: true

                        }

                    },

                    {

                        breakpoint: 640,

                        settings: {

                            slidesToShow: 2,

                            slidesToScroll: 2

                        }

                    },

                    {

                        breakpoint: 480,

                        settings: {

                            slidesToShow: 1,

                            slidesToScroll: 1

                        }

                    }

                ]

            }


            $scope.numberLoaded = true;


        });

        function findById(arr, id) {

            var res_item = false;
            arr.forEach(function(item) {

                if ( item.id == id ) {
                    res_item = item
                }

            });

            return res_item
        }

        function removeById(arr, id) {

            arr.forEach(function(item) {

                if ( item.id == id ) {
                    arr.splice( arr.indexOf(item) , 1)
                }

            });

        }

        function sendRequest() {

            var vals = [];
            filtersArray.forEach(function(item) {
                vals.push(item.val)
            })
            Advert.get_by_prop({pops_id: vals}).$promise.then(function(data){
                $scope.adverts_by_search = data
            })
        }


    });