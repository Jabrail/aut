'use strict';

var auto = angular.module('auto_client', [
    'ngRoute',
    'uiGmapgoogle-maps',
    'xenon.directives',
    'ngResource',
    'slickCarousel'
])

var view_dir = "/assets/client_app/view/"

auto.config(function($httpProvider) {
    $httpProvider.defaults.headers.common['X-CSRF-Token'] =
        $('meta[name=csrf-token]').attr('content');
});

auto.config(['$routeProvider', '$locationProvider',
    function($routeProvider ,$locationProvider ) {
        $routeProvider.
            when('/', {
                templateUrl: "/assets/client_app/view/home/index.html",
                controller: 'HomeCtrl'
            }).
            when('/search', {
                templateUrl: "/assets/client_app/view/search/index.html",
                controller: 'SearchCtrl'
            }).
            when('/search/mark/:id', {
                templateUrl: "/assets/client_app/view/search/index.html",
                controller: 'SearchCtrl'
            }).
            when('/search/:id', {
                templateUrl: "/assets/client_app/view/search/detail.html",
                controller: 'DetailCtrl'
            }).
            when('/newPoster', {
                templateUrl: "/assets/client_app/view/authpages/newPoster/newPoster.html",
                controller: 'NewAdvertCtrl'
            }).
            when('/profileMain', {
                templateUrl: "/assets/client_app/view/authpages/profileMain/index.html",
                controller: 'ProfileMain'
            }).
            when('/profileService', {
                templateUrl: "/assets/client_app/view/authpages/profileMain/service.html",
                controller: 'ProfileService'
            }).
            when('/profileSettings', {
                templateUrl: "/assets/client_app/view/authpages/profileMain/settings.html",
                controller: 'ProfileSettings'
            }).
            when('/page/:name', {
                templateUrl: "/assets/client_app/view/staticPages/index.html",
                controller: 'StaticPagesCtrl'
            }).
            otherwise({
                redirectTo: '/'
            });
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });


    }
]);

auto.controller('MenuCtrl',['$scope', '$location'
    , function ($scope, $location ) {

        $scope.getClass = function (path) {
            if ($location.path().substr(0, path.length) === path) {
                return 'active';
            } else {
                return '';
            }
        }

    }
    ]);

auto.config(function(uiGmapGoogleMapApiProvider) {
    uiGmapGoogleMapApiProvider.configure({
        //    key: 'your api key',
        v: '3.20', //defaults to latest 3.X anyhow
        libraries: 'places' // Required for SearchBox.
    });
})
auto.run(['$templateCache', function ($templateCache) {
    $templateCache.put('searchbox.tpl.html', '<input id="pac-input" class="pac-controls" type="text" placeholder="Search">');
    $templateCache.put('window.tpl.html', '<div ng-controller="WindowCtrl" ng-init="showPlaceDetails(parameter)">{{place.name}}</div>');
}])

function getUser(user, scope, callback) {

}