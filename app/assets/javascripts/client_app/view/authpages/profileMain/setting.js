"use strict";

auto.controller('ProfileSettings',
	function ($scope, User, Advert, $location) {

		$scope.showContent = false;

		$scope.showContent = false;

		User.get_user().$promise.then(function(data){

			if (data.status)  {

				Advert.get_user_poster().$promise.then(function(data){

					$scope.showContent = true;

					$scope.adverts = data;

				})

			} else {
				window.location = '/users/sign_in'
			}

		}, function(err) {

			window.location = '/users/sign_in'

		})


		$scope.remove_advert = function(advert) {

			Advert.destroy({id: advert.id}).$promise.then(function() {


				Advert.get_user_poster().$promise.then(function(data){

					$scope.adverts = data;

				})

			})

		}

		$scope.getAdvert = function(advert) {

			$location.path('/search/'+advert.id)

		}


	}
);

