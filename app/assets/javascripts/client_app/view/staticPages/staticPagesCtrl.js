"use strict";

auto.controller('StaticPagesCtrl',
	function ($scope, Pages, $routeParams, $sce) {

		Pages.get_page({name: $routeParams.name}).$promise.then(function(data) {

			$scope.page = data.page
			$scope.page.text = $sce.trustAsHtml($scope.page.text);
		})

	});