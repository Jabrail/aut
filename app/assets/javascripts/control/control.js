auto.controller('MenuCtrl', function($scope, $rootScope, $location, $layout, $layoutToggles, $pageLoadingBar, Fullscreen)
{
    $rootScope.isLoginPage        = false;
    $rootScope.isLightLoginPage   = false;
    $rootScope.isLockscreenPage   = false;
    $rootScope.isMainPage         = true;

    $rootScope.layoutOptions = {
        horizontalMenu: {
            isVisible		: false,
            isFixed			: true,
            minimal			: false,
            clickToExpand	: false,

            isMenuOpenMobile: false
        },
        sidebar: {
            isVisible		: true,
            isCollapsed		: false,
            toggleOthers	: true,
            isFixed			: true,
            isRight			: false,

            isMenuOpenMobile: false,

            // Added in v1.3
            userProfile		: true
        },
        chat: {
            isOpen			: false,
        },
        settingsPane: {
            isOpen			: false,
            useAnimation	: true
        },
        container: {
            isBoxed			: false
        },
        skins: {
            sidebarMenu		: '',
            horizontalMenu	: '',
            userInfoNavbar	: ''
        },
        pageTitles: true,
        userInfoNavVisible	: false
    };

    $layout.loadOptionsFromCookies(); // remove this line if you don't want to support cookies that remember layout changes


    $scope.updatePsScrollbars = function()
    {
        var $scrollbars = jQuery(".ps-scrollbar:visible");

        $scrollbars.each(function(i, el)
        {
            if(typeof jQuery(el).data('perfectScrollbar') == 'undefined')
            {
                jQuery(el).perfectScrollbar();
            }
            else
            {
                jQuery(el).perfectScrollbar('update');
            }
        })
    };


    // Define Public Vars
    public_vars.$body = jQuery("body");


    // Init Layout Toggles
    $layoutToggles.initToggles();


    // Other methods
    $scope.setFocusOnSearchField = function()
    {
        public_vars.$body.find('.search-form input[name="s"]').focus();

        setTimeout(function(){ public_vars.$body.find('.search-form input[name="s"]').focus() }, 100 );
    };


    // Watch changes to replace checkboxes
    $scope.$watch(function()
    {
        cbr_replace();
    });

    // Watch sidebar status to remove the psScrollbar
    $rootScope.$watch('layoutOptions.sidebar.isCollapsed', function(newValue, oldValue)
    {
        if(newValue != oldValue)
        {
            if(newValue == true)
            {
                public_vars.$sidebarMenu.find('.sidebar-menu-inner').perfectScrollbar('destroy')
            }
            else
            {
                public_vars.$sidebarMenu.find('.sidebar-menu-inner').perfectScrollbar({wheelPropagation: public_vars.wheelPropagation});
            }
        }
    });

    // Page Loading Progress (remove/comment this line to disable it)
    $pageLoadingBar.init();

    $scope.showLoadingBar = showLoadingBar;
    $scope.hideLoadingBar = hideLoadingBar;


    // Set Scroll to 0 When page is changed
    $rootScope.$on('$stateChangeStart', function()
    {
        var obj = {pos: jQuery(window).scrollTop()};

        TweenLite.to(obj, .25, {pos: 0, ease:Power4.easeOut, onUpdate: function()
        {
            $(window).scrollTop(obj.pos);
        }});
    });


    // Full screen feature added in v1.3
    $scope.isFullscreenSupported = Fullscreen.isSupported();
    $scope.isFullscreen = Fullscreen.isEnabled() ? true : false;

    $scope.goFullscreen = function()
    {
        if (Fullscreen.isEnabled())
            Fullscreen.cancel();
        else
            Fullscreen.all();

        $scope.isFullscreen = Fullscreen.isEnabled() ? true : false;
    }

})
"use strict";

auto.controller('AdvertsCtrl',
    function ($scope, $http, Advert ,  $rootScope, $modal, $sce) {

        $scope.actions = {
            show: 'true',
            edit: 'true',
            remove: 'true'
        }

        $scope.openModal = function(modal_id, modal_size, modal_backdrop)
        {
            $rootScope.currentModal = $modal.open({
                templateUrl: modal_id,
                size: modal_size,
                backdrop: typeof modal_backdrop == 'undefined' ? true : modal_backdrop,
                scope : $scope
            });
            $scope.$apply()
        };

        Advert.index().$promise.then(function(data) {
            $scope.items = data.adverts.reverse();
            $scope.values = data.vals;
            $scope.poster_count = data.poster_count
        })

        $scope.setTop = function(advert) {

            Advert.setTop({ id: advert.id , set_top: 1}).$promise.then(function(data) {
                advert.service = 1
            })

        }

        $scope._remove = function(advert) {

            Advert.destroy({ id: advert.id}).$promise.then(function(data){

                $scope.items.splice($scope.items.indexOf(advert), 1);


            })

        }
    });
'use strict';

angular.module('auto.controllers', []).
	controller('LoginCtrl', function($scope, $rootScope)
	{
		$rootScope.isLoginPage        = true;
		$rootScope.isLightLoginPage   = false;
		$rootScope.isLockscreenPage   = false;
		$rootScope.isMainPage         = false;
	}).
	controller('LoginLightCtrl', function($scope, $rootScope)
	{
		$rootScope.isLoginPage        = true;
		$rootScope.isLightLoginPage   = true;
		$rootScope.isLockscreenPage   = false;
		$rootScope.isMainPage         = false;
	}).
	controller('LockscreenCtrl', function($scope, $rootScope)
	{
		$rootScope.isLoginPage        = false;
		$rootScope.isLightLoginPage   = false;
		$rootScope.isLockscreenPage   = true;
		$rootScope.isMainPage         = false;
	}).
	controller('MainCtrl', function($scope, $rootScope, $location, $layout, $layoutToggles, $pageLoadingBar, Fullscreen)
	{
		$rootScope.isLoginPage        = false;
		$rootScope.isLightLoginPage   = false;
		$rootScope.isLockscreenPage   = false;
		$rootScope.isMainPage         = true;

		$rootScope.layoutOptions = {
			horizontalMenu: {
				isVisible		: false,
				isFixed			: true,
				minimal			: false,
				clickToExpand	: false,

				isMenuOpenMobile: false
			},
			sidebar: {
				isVisible		: true,
				isCollapsed		: false,
				toggleOthers	: true,
				isFixed			: true,
				isRight			: false,

				isMenuOpenMobile: false,

				// Added in v1.3
				userProfile		: true
			},
			chat: {
				isOpen			: false,
			},
			settingsPane: {
				isOpen			: false,
				useAnimation	: true
			},
			container: {
				isBoxed			: false
			},
			skins: {
				sidebarMenu		: '',
				horizontalMenu	: '',
				userInfoNavbar	: ''
			},
			pageTitles: true,
			userInfoNavVisible	: false
		};

		$layout.loadOptionsFromCookies(); // remove this line if you don't want to support cookies that remember layout changes


		$scope.updatePsScrollbars = function()
		{
			var $scrollbars = jQuery(".ps-scrollbar:visible");

			$scrollbars.each(function(i, el)
			{
				if(typeof jQuery(el).data('perfectScrollbar') == 'undefined')
				{
					jQuery(el).perfectScrollbar();
				}
				else
				{
					jQuery(el).perfectScrollbar('update');
				}
			})
		};


		// Define Public Vars
		public_vars.$body = jQuery("body");


		// Init Layout Toggles
		$layoutToggles.initToggles();


		// Other methods
		$scope.setFocusOnSearchField = function()
		{
			public_vars.$body.find('.search-form input[name="s"]').focus();

			setTimeout(function(){ public_vars.$body.find('.search-form input[name="s"]').focus() }, 100 );
		};


		// Watch changes to replace checkboxes
		$scope.$watch(function()
		{
			cbr_replace();
		});

		// Watch sidebar status to remove the psScrollbar
		$rootScope.$watch('layoutOptions.sidebar.isCollapsed', function(newValue, oldValue)
		{
			if(newValue != oldValue)
			{
				if(newValue == true)
				{
					public_vars.$sidebarMenu.find('.sidebar-menu-inner').perfectScrollbar('destroy')
				}
				else
				{
					public_vars.$sidebarMenu.find('.sidebar-menu-inner').perfectScrollbar({wheelPropagation: public_vars.wheelPropagation});
				}
			}
		});


		// Page Loading Progress (remove/comment this line to disable it)
		$pageLoadingBar.init();

		$scope.showLoadingBar = showLoadingBar;
		$scope.hideLoadingBar = hideLoadingBar;


		// Set Scroll to 0 When page is changed
		$rootScope.$on('$stateChangeStart', function()
		{
			var obj = {pos: jQuery(window).scrollTop()};

			TweenLite.to(obj, .25, {pos: 0, ease:Power4.easeOut, onUpdate: function()
			{
				$(window).scrollTop(obj.pos);
			}});
		});


		// Full screen feature added in v1.3
		$scope.isFullscreenSupported = Fullscreen.isSupported();
		$scope.isFullscreen = Fullscreen.isEnabled() ? true : false;

		$scope.goFullscreen = function()
		{
			if (Fullscreen.isEnabled())
				Fullscreen.cancel();
			else
				Fullscreen.all();

			$scope.isFullscreen = Fullscreen.isEnabled() ? true : false;
		}

	}).
	controller('SidebarMenuCtrl', function($scope, $rootScope, $menuItems, $timeout, $location, $state, $layout)
	{

		// Menu Items
		var $sidebarMenuItems = $menuItems.instantiate();

		$scope.menuItems = $sidebarMenuItems.prepareSidebarMenu().getAll();

		// Set Active Menu Item
		$sidebarMenuItems.setActive( $location.path() );

		$rootScope.$on('$stateChangeSuccess', function()
		{
			$sidebarMenuItems.setActive($state.current.name);
		});

		// Trigger menu setup
		public_vars.$sidebarMenu = public_vars.$body.find('.sidebar-menu');
		$timeout(setup_sidebar_menu, 1);

		ps_init(); // perfect scrollbar for sidebar
	}).
	controller('HorizontalMenuCtrl', function($scope, $rootScope, $menuItems, $timeout, $location, $state)
	{
		var $horizontalMenuItems = $menuItems.instantiate();

		$scope.menuItems = $horizontalMenuItems.prepareHorizontalMenu().getAll();

		// Set Active Menu Item
		$horizontalMenuItems.setActive( $location.path() );

		$rootScope.$on('$stateChangeSuccess', function()
		{
			$horizontalMenuItems.setActive($state.current.name);

			$(".navbar.horizontal-menu .navbar-nav .hover").removeClass('hover'); // Close Submenus when item is selected
		});

		// Trigger menu setup
		$timeout(setup_horizontal_menu, 1);
	}).
	controller('SettingsPaneCtrl', function($rootScope)
	{
		// Define Settings Pane Public Variable
		public_vars.$settingsPane = public_vars.$body.find('.settings-pane');
		public_vars.$settingsPaneIn = public_vars.$settingsPane.find('.settings-pane-inner');
	}).
	controller('ChatCtrl', function($scope, $element)
	{
		var $chat = jQuery($element),
			$chat_conv = $chat.find('.chat-conversation');

		$chat.find('.chat-inner').perfectScrollbar(); // perfect scrollbar for chat container


		// Chat Conversation Window (sample)
		$chat.on('click', '.chat-group a', function(ev)
		{
			ev.preventDefault();

			$chat_conv.toggleClass('is-open');

			if($chat_conv.is(':visible'))
			{
				$chat.find('.chat-inner').perfectScrollbar('update');
				$chat_conv.find('textarea').autosize();
			}
		});

		$chat_conv.on('click', '.conversation-close', function(ev)
		{
			ev.preventDefault();

			$chat_conv.removeClass('is-open');
		});
	}).
	controller('UIModalsCtrl', function($scope, $rootScope, $modal, $sce)
	{
		// Open Simple Modal
		$scope.openModal = function(modal_id, modal_size, modal_backdrop)
		{
			$rootScope.currentModal = $modal.open({
				templateUrl: modal_id,
				size: modal_size,
				backdrop: typeof modal_backdrop == 'undefined' ? true : modal_backdrop
			});
		};

		// Loading AJAX Content
		$scope.openAjaxModal = function(modal_id, url_location)
		{
			$rootScope.currentModal = $modal.open({
				templateUrl: modal_id,
				resolve: {
					ajaxContent: function($http)
					{
						return $http.get(url_location).then(function(response){
							$rootScope.modalContent = $sce.trustAsHtml(response.data);
						}, function(response){
							$rootScope.modalContent = $sce.trustAsHtml('<div class="label label-danger">Cannot load ajax content! Please check the given url.</div>');
						});
					}
				}
			});

			$rootScope.modalContent = $sce.trustAsHtml('Modal content is loading...');
		}
	}).
	controller('PaginationDemoCtrl', function($scope)
	{
		$scope.totalItems = 64;
		$scope.currentPage = 4;

		$scope.setPage = function (pageNo)
		{
			$scope.currentPage = pageNo;
		};

		$scope.pageChanged = function()
		{
			console.log('Page changed to: ' + $scope.currentPage);
		};

		$scope.maxSize = 5;
		$scope.bigTotalItems = 175;
		$scope.bigCurrentPage = 1;
	}).
	controller('LayoutVariantsCtrl', function($scope, $layout, $cookies)
	{
		$scope.opts = {
			sidebarType: null,
			fixedSidebar: null,
			sidebarToggleOthers: null,
			sidebarVisible: null,
			sidebarPosition: null,

			horizontalVisible: null,
			fixedHorizontalMenu: null,
			horizontalOpenOnClick: null,
			minimalHorizontalMenu: null,

			sidebarProfile: null
		};

		$scope.sidebarTypes = [
			{value: ['sidebar.isCollapsed', false], text: 'Expanded', selected: $layout.is('sidebar.isCollapsed', false)},
			{value: ['sidebar.isCollapsed', true], text: 'Collapsed', selected: $layout.is('sidebar.isCollapsed', true)},
		];

		$scope.fixedSidebar = [
			{value: ['sidebar.isFixed', true], text: 'Fixed', selected: $layout.is('sidebar.isFixed', true)},
			{value: ['sidebar.isFixed', false], text: 'Static', selected: $layout.is('sidebar.isFixed', false)},
		];

		$scope.sidebarToggleOthers = [
			{value: ['sidebar.toggleOthers', true], text: 'Yes', selected: $layout.is('sidebar.toggleOthers', true)},
			{value: ['sidebar.toggleOthers', false], text: 'No', selected: $layout.is('sidebar.toggleOthers', false)},
		];

		$scope.sidebarVisible = [
			{value: ['sidebar.isVisible', true], text: 'Visible', selected: $layout.is('sidebar.isVisible', true)},
			{value: ['sidebar.isVisible', false], text: 'Hidden', selected: $layout.is('sidebar.isVisible', false)},
		];

		$scope.sidebarPosition = [
			{value: ['sidebar.isRight', false], text: 'Left', selected: $layout.is('sidebar.isRight', false)},
			{value: ['sidebar.isRight', true], text: 'Right', selected: $layout.is('sidebar.isRight', true)},
		];

		$scope.horizontalVisible = [
			{value: ['horizontalMenu.isVisible', true], text: 'Visible', selected: $layout.is('horizontalMenu.isVisible', true)},
			{value: ['horizontalMenu.isVisible', false], text: 'Hidden', selected: $layout.is('horizontalMenu.isVisible', false)},
		];

		$scope.fixedHorizontalMenu = [
			{value: ['horizontalMenu.isFixed', true], text: 'Fixed', selected: $layout.is('horizontalMenu.isFixed', true)},
			{value: ['horizontalMenu.isFixed', false], text: 'Static', selected: $layout.is('horizontalMenu.isFixed', false)},
		];

		$scope.horizontalOpenOnClick = [
			{value: ['horizontalMenu.clickToExpand', false], text: 'No', selected: $layout.is('horizontalMenu.clickToExpand', false)},
			{value: ['horizontalMenu.clickToExpand', true], text: 'Yes', selected: $layout.is('horizontalMenu.clickToExpand', true)},
		];

		$scope.minimalHorizontalMenu = [
			{value: ['horizontalMenu.minimal', false], text: 'No', selected: $layout.is('horizontalMenu.minimal', false)},
			{value: ['horizontalMenu.minimal', true], text: 'Yes', selected: $layout.is('horizontalMenu.minimal', true)},
		];

		$scope.chatVisibility = [
			{value: ['chat.isOpen', false], text: 'No', selected: $layout.is('chat.isOpen', false)},
			{value: ['chat.isOpen', true], text: 'Yes', selected: $layout.is('chat.isOpen', true)},
		];

		$scope.boxedContainer = [
			{value: ['container.isBoxed', false], text: 'No', selected: $layout.is('container.isBoxed', false)},
			{value: ['container.isBoxed', true], text: 'Yes', selected: $layout.is('container.isBoxed', true)},
		];

		$scope.sidebarProfile = [
			{value: ['sidebar.userProfile', false], text: 'No', selected: $layout.is('sidebar.userProfile', false)},
			{value: ['sidebar.userProfile', true], text: 'Yes', selected: $layout.is('sidebar.userProfile', true)},
		];

		$scope.resetOptions = function()
		{
			$layout.resetCookies();
			window.location.reload();
		};

		var setValue = function(val)
		{
			if(val != null)
			{
				val = eval(val);
				$layout.setOptions(val[0], val[1]);
			}
		};

		$scope.$watch('opts.sidebarType', setValue);
		$scope.$watch('opts.fixedSidebar', setValue);
		$scope.$watch('opts.sidebarToggleOthers', setValue);
		$scope.$watch('opts.sidebarVisible', setValue);
		$scope.$watch('opts.sidebarPosition', setValue);

		$scope.$watch('opts.horizontalVisible', setValue);
		$scope.$watch('opts.fixedHorizontalMenu', setValue);
		$scope.$watch('opts.horizontalOpenOnClick', setValue);
		$scope.$watch('opts.minimalHorizontalMenu', setValue);

		$scope.$watch('opts.chatVisibility', setValue);

		$scope.$watch('opts.boxedContainer', setValue);

		$scope.$watch('opts.sidebarProfile', setValue);
	}).
	controller('ThemeSkinsCtrl', function($scope, $layout)
	{
		var $body = jQuery("body");

		$scope.opts = {
			sidebarSkin: $layout.get('skins.sidebarMenu'),
			horizontalMenuSkin: $layout.get('skins.horizontalMenu'),
			userInfoNavbarSkin: $layout.get('skins.userInfoNavbar')
		};

		$scope.skins = [
			{value: '', 			name: 'Default'			,	palette: ['#2c2e2f','#EEEEEE','#FFFFFF','#68b828','#27292a','#323435']},
			{value: 'aero', 		name: 'Aero'			,	palette: ['#558C89','#ECECEA','#FFFFFF','#5F9A97','#558C89','#255E5b']},
			{value: 'navy', 		name: 'Navy'			,	palette: ['#2c3e50','#a7bfd6','#FFFFFF','#34495e','#2c3e50','#ff4e50']},
			{value: 'facebook', 	name: 'Facebook'		,	palette: ['#3b5998','#8b9dc3','#FFFFFF','#4160a0','#3b5998','#8b9dc3']},
			{value: 'turquoise', 	name: 'Truquoise'		,	palette: ['#16a085','#96ead9','#FFFFFF','#1daf92','#16a085','#0f7e68']},
			{value: 'lime', 		name: 'Lime'			,	palette: ['#8cc657','#ffffff','#FFFFFF','#95cd62','#8cc657','#70a93c']},
			{value: 'green', 		name: 'Green'			,	palette: ['#27ae60','#a2f9c7','#FFFFFF','#2fbd6b','#27ae60','#1c954f']},
			{value: 'purple', 		name: 'Purple'			,	palette: ['#795b95','#c2afd4','#FFFFFF','#795b95','#27ae60','#5f3d7e']},
			{value: 'white', 		name: 'White'			,	palette: ['#FFFFFF','#666666','#95cd62','#EEEEEE','#95cd62','#555555']},
			{value: 'concrete', 	name: 'Concrete'		,	palette: ['#a8aba2','#666666','#a40f37','#b8bbb3','#a40f37','#323232']},
			{value: 'watermelon', 	name: 'Watermelon'		,	palette: ['#b63131','#f7b2b2','#FFFFFF','#c03737','#b63131','#32932e']},
			{value: 'lemonade', 	name: 'Lemonade'		,	palette: ['#f5c150','#ffeec9','#FFFFFF','#ffcf67','#f5c150','#d9a940']},
		];

		$scope.$watch('opts.sidebarSkin', function(val)
		{
			if(val != null)
			{
				$layout.setOptions('skins.sidebarMenu', val);

				$body.attr('class', $body.attr('class').replace(/\sskin-[a-z]+/)).addClass('skin-' + val);
			}
		});

		$scope.$watch('opts.horizontalMenuSkin', function(val)
		{
			if(val != null)
			{
				$layout.setOptions('skins.horizontalMenu', val);

				$body.attr('class', $body.attr('class').replace(/\shorizontal-menu-skin-[a-z]+/)).addClass('horizontal-menu-skin-' + val);
			}
		});

		$scope.$watch('opts.userInfoNavbarSkin', function(val)
		{
			if(val != null)
			{
				$layout.setOptions('skins.userInfoNavbar', val);

				$body.attr('class', $body.attr('class').replace(/\suser-info-navbar-skin-[a-z]+/)).addClass('user-info-navbar-skin-' + val);
			}
		});
	}).
	// Added in v1.3
	controller('FooterChatCtrl', function($scope, $element)
	{
		$scope.isConversationVisible = false;

		$scope.toggleChatConversation = function()
		{
			$scope.isConversationVisible = ! $scope.isConversationVisible;

			if($scope.isConversationVisible)
			{
				setTimeout(function()
				{
					var $el = $element.find('.ps-scrollbar');

					if($el.hasClass('ps-scroll-down'))
					{
						$el.scrollTop($el.prop('scrollHeight'));
					}

					$el.perfectScrollbar({
						wheelPropagation: false
					});

					$element.find('.form-control').focus();

				}, 300);
			}
		}
	});
"use strict";

auto.controller('FiltersCtrl'
    , function ($scope, $http, Filter, $state,  $rootScope, $modal, $sce) {

        $scope.new_filter = {}

        $scope.actions = {
            show: 'true',
            edit: 'true',
            remove: 'true'
        }

        $scope.openModal = function(modal_id, modal_size, modal_backdrop)
        {
            $rootScope.currentModal = $modal.open({
                templateUrl: modal_id,
                size: modal_size,
                scope: $scope,
                backdrop: typeof modal_backdrop == 'undefined' ? true : modal_backdrop
            });
        };

        $scope.update_filter = {}

        Filter.index().
            $promise.then(function(data) {

                $scope.items = data.filters.reverse()
                $scope.values = data.vals;

            });

        $scope.type_options = [
            {
                name: 'Text',
                value: '1'
            },
            {
                name: 'Checkbox',
                value: '2'
            },
            {
                name: 'Radio',
                value: '3'
            },
            {
                name: 'Select',
                value: '4'
            },
            {
                name: 'Color',
                value: '5'
            },
            {
                name: 'Interval',
                value: '6'
            },
            {
                name: 'Image',
                value: '7'
            },
            {
                name: 'Mark',
                value: '10'
            },
            {
                name: 'Model',
                value: '11'
            },
            {
                name: 'Year',
                value: '12'
            },
            {
                name: 'Country',
                value: '13'
            },
            {
                name: 'boolean',
                value: '14'
            }
        ];

        $scope._create = function() {

            Filter.create({'property': { name: $scope.new_filter.name , 'type_value' : $scope.new_filter.type_value.value }}).
                $promise.then(function(data) {
                    $scope.items.unshift(data);
                    $rootScope.currentModal.close();
                })

        }

        $scope._remove = function(filter) {

            Filter.destroy({id: filter.id})
            $scope.items.splice($scope.items.indexOf(filter), 1);

        }

        $scope.showEdit = function(filter) {

            $scope.update_filter = filter;
            $scope.update_filter.type_value = $scope.type_options[Number(filter.type_value) - 1];

            $rootScope.currentModal = $modal.open({
                templateUrl: 'updateModel',
                size: 'undefined',
                backdrop: typeof modal_backdrop == 'undefined' ? true : modal_backdrop,
                scope: $scope
            });



        }

        $scope._edit = function() {

            Filter.update({id : $scope.update_filter.id, 'property' : { name: $scope.update_filter.name , 'type_value' : $scope.update_filter.type_value.value, 'valid_data' : $scope.update_filter.valid_data }}).
                $promise.then(function(data) {

                    $scope.update_filter.type_value = Number($scope.update_filter.type_value.value);

                    $rootScope.currentModal.close()

                })
        }

        $scope.show = function(filter) {

            $state.go('app.auto-filter-show' , {id: filter.id})

        }

    });
"use strict";

auto.controller('GroupShowCtrl',
    function ($scope, $http, Filter, Group, $stateParams,  $rootScope, $modal, $sce ) {

        $scope.new_filter = 0;

        Filter.index().
            $promise.then(function(data){
                $scope.type_options = data.filters
             })

        $scope.actions = {
            show: 'false',
            edit: 'false',
            remove: 'true'
        }

        $scope.openModal = function(modal_id, modal_size, modal_backdrop)
        {
            $rootScope.currentModal = $modal.open({
                templateUrl: modal_id,
                size: modal_size,
                backdrop: typeof modal_backdrop == 'undefined' ? true : modal_backdrop,
                scope : $scope
            });
            $scope.$apply()
        };

        Group.show({id: $stateParams.id }).
            $promise.then(function(data) {

                $scope.items = data.filters.reverse();
                $scope.values = data.vals;

            })

        $scope.addFilter = function (fl) {

            Group.add_filter({p_id: fl , g_id: $stateParams.id}).$promise.then(function(data) {

                $scope.items.unshift(data);
                $rootScope.currentModal.close()

            })

        }
        $scope._remove = function (filter) {

            Group.remove_filter({p_id: filter.id , g_id: $stateParams.id})
            $scope.items.splice($scope.items.indexOf(filter), 1);

        }
    });
"use strict";

auto.controller('GroupCtrl', function ($scope, $http, Group, $state ,  $rootScope, $modal, $sce) {

    $scope.new_group = {}

    $scope.actions = {
        show: 'true',
        edit: 'true',
        remove: 'true'
    }

    $scope.openModal = function(modal_id, modal_size, modal_backdrop)
    {
        $rootScope.currentModal = $modal.open({
            templateUrl: modal_id,
            size: modal_size,
            backdrop: typeof modal_backdrop == 'undefined' ? true : modal_backdrop,
            scope : $scope
        });
        $scope.$apply()
    };


    Group.index().
        $promise.then(function(data) {

            $scope.items = data.groups.reverse();
            $scope.values = data.vals;

        });

    $scope._create = function() {

        Group.create({'group': { name: $scope.new_group.name }}).
            $promise.then(function(data) {
                $scope.items.unshift(data);
                $rootScope.currentModal.close()
             })

    }

    $scope._remove = function(group) {

        Group.destroy({id: group.id})
        $scope.items.splice($scope.items.indexOf(group), 1);

    }

    $scope.showEdit = function(group) {

        $scope.update_group = group;
        $('#updateModel').modal('show');

    }

    $scope._edit = function() {

        Group.update({id : $scope.update_group.id, 'group' : { name: $scope.update_group.name  }}).
            $promise.then(function(data) {

                $('#updateModel').modal('hide');
            })

    }


    $scope.show = function(group) {

        $state.go('app.auto-groups-show' , {id: group.id})

    }

});
"use strict";

auto.controller('HomeCtrl',['$scope', '$http'
    , function ($scope, $http ) {


    }]);
"use strict";

auto.controller('LangValCtrl',
    function ($scope, LangVal, $stateParams , $state) {

        $scope.actions = {
            show: 'true',
            edit: 'true',
            remove: 'true'
        }
        LangVal.get_filters({id: $stateParams.id }).$promise.then(function(data) {

            $scope.items = data.langs;

        });

             $scope.ckEditors = [];
            $scope.addEditor = function(){
                var rand = ""+(Math.random() * 10000);
                $scope.ckEditors.push({value:rand});
            }

        // Called when the editor is completely ready.
        $scope.onReady = function () {
            // ...
        };

        $scope.save = function() {

            LangVal.add_filter({items: $scope.items}).$promise.then(function() {

                $state.go('app.auto-filter-show' , {id: $stateParams.id})

            })

        }

    });
"use strict";

auto.controller('LanguagesCtrl', function ($scope, $http, Lang, $rootScope, $modal, $sce) {


    $scope.new_language = {};

    $scope.actions = {
        show: 'false',
        edit: 'true',
        remove: 'true'
    };


    $scope.openModal = function(modal_id, modal_size, modal_backdrop)
    {
        $rootScope.currentModal = $modal.open({
            templateUrl: modal_id,
            size: modal_size,
            backdrop: typeof modal_backdrop == 'undefined' ? true : modal_backdrop,
            scope: $scope
        });
    };

    Lang.index().
        $promise.then(function(data) {

            $scope.items = data.langs.reverse();
            $scope.values = data.vals;

        });

    $scope._create = function() {

        Lang.create({'lang': { short_name: $scope.new_language.short_name , 'long_name' : $scope.new_language.long_name }}).
            $promise.then(function(data) {
                $scope.items.unshift(data);
                $rootScope.currentModal.close()
             })

    }

    $scope._remove = function(language) {

        Lang.destroy({id: language.id})
        $scope.items.splice($scope.items.indexOf(language), 1);

    }

    $scope.showEdit = function(languag) {

        $scope.update_language = languag;

        $rootScope.currentModal = $modal.open({
            templateUrl: 'updateModel',
            size: 'undefined',
            backdrop: typeof modal_backdrop == 'undefined' ? true : modal_backdrop,
            scope: $scope
        });

    }

    $scope._edit = function() {

        Lang.update({id : $scope.update_language.id, 'lang' : { short_name: $scope.update_language.short_name , 'long_name' : $scope.update_language.long_name }}).
            $promise.then(function(data) {

                $rootScope.currentModal.close()
            })

    }


});
"use strict";

auto.controller('NewAdvertCtrl',
    function ($scope, $http , Filters, Advert , $location) {

        var images = [];
        var prop_vals = [];
        var i = 1,
            $example_dropzone_filetable = $("#example-dropzone-filetable"),
            example_dropzone = $("#advancedDropzone").dropzone({
                url: '/images',
                method: 'POST',

                // Events
                addedfile: function(file)
                {
                    if(i == 1)
                    {
                        $example_dropzone_filetable.find('tbody').html('');
                    }

                    var size = parseInt(file.size/1024, 10);
                    size = size < 1024 ? (size + " KB") : (parseInt(size/1024, 10) + " MB");

                    var $entry = $('<tr>\
										<td class="text-center">'+(i++)+'</td>\
										<td>'+file.name+'</td>\
										<td style="width: 100px"><div class="progress progress-striped"><div class="progress-bar progress-bar-warning"></div></div></td>\
										<td  style="width: 100px">'+size+'</td>\
										<td class="actions"> </td>\
										<td style="width: 100px">Uploading...</td>\
									</tr>');

                    $example_dropzone_filetable.find('tbody').append($entry);
                    file.fileEntryTd = $entry;
                    file.progressBar = $entry.find('.progress-bar');
                    file.actions = $entry.find('.actions');
                },

                uploadprogress: function(file, progress, bytesSent)
                {
                    file.progressBar.width(progress + '%');
                },

                success: function(file, response)
                {
                    images.push(response.id)
                    file.fileEntryTd.find('td:last').html('<span class="text-success">Uploaded</span>');
                    file.actions.html('<button type="button" class="btn btn-danger btn-sm"   ng-click="remove_image(response.id)">Remove</button>');
                    file.progressBar.removeClass('progress-bar-warning').addClass('progress-bar-success');
                    $scope.$apply();
                },

                error: function(file)
                {
                    file.fileEntryTd.find('td:last').html('<span class="text-danger">Failed</span>');
                    file.progressBar.removeClass('progress-bar-warning').addClass('progress-bar-red');
                }
            });

        $("#advancedDropzone").css({
            minHeight: 200
        });


        Filters.get_filter_for_new().$promise.then(function(data) {

            $scope.filter = data.filters;
            $scope.filter.mark._selected = '0'
            $scope.filter.additional.selected = []

        })


        $scope._change = function(filter) {

            findById(prop_vals, filter.id).value.sub = filter.__selected

         }

        $scope.setInput = function(filter) {

            var val = findById(prop_vals, filter.id)

            if (val) {
                val.value.value = filter.selected;
            } else {
                val = { id: filter.id,
                    type: filter.type_value,
                    value: {
                        id: filter.prop_vals[0].id,
                        value: filter.selected
                    }
                }
                prop_vals.push(val)
            }
        }

        $scope.change = function(filter) {
            filter.selected_sub = JSON.parse(filter._selected);
            filter.selected = filter.selected_sub.id;
            filter.__selected = '0';

            removeById(prop_vals, filter.id);
            prop_vals.push({ id: filter.id,
                type: filter.type_value,
                value: {
                    base: filter.selected,
                    sub: 0
                }
            });
        }

        $scope.setValue = function(value) {


        }

        $scope.set_model = function () {
        }

        $scope.setRadio = function(filter, val) {
            filter.selected = val.id
        };

        $scope.setSelect = function(filter) {

            var val = findById(prop_vals, filter.id);

            if (val) {
                val.value.value =  filter.selected
            } else {
                val = { id: filter.id,
                    type: filter.type_value,
                    value: {
                        value: filter.selected
                    }
                };
                prop_vals.push(val);
            }
        };

        $scope.setCheckboxSingl = function(filter) {

            var val = findById(prop_vals, filter.id);
            if (val) {
                prop_vals.splice( prop_vals.indexOf(val) , 1)
            } else {
                val = { id: filter.id,
                    type: filter.type_value,
                    value: {
                        value: filter.prop_vals[0].id
                    }
                };
                prop_vals.push(val);
            }

        };

        $scope.setCheckbox = function(filter, val) {

            if (filter.selected.indexOf(val.id) > -1) {

                filter.selected.splice(filter.selected.indexOf(val.id), 1);

            }
            else {
                filter.selected.push(val.id)
            }

            var val = findById(prop_vals, filter.id);

            if (val) {
                val.value = filter.selected
            } else {
                val = { id: filter.id,
                    type: filter.type_value,
                    value: filter.selected
                };
                prop_vals.push(val);
            }

        };

        $scope.create = function(type) {



            if (type == 'top') {
                Advert.create({
                    poster: {description: $scope.advert.desc, price: $scope.advert.price},
                    set_top: 1,
                    currency: $scope.advert.currency,
                    filters: prop_vals
                }).$promise.then(function () {
                        $location.path('/adverts/')
                    })
            } else {

                Advert.create({
                    poster: {description: $scope.advert.desc, price: $scope.advert.price},
                    set_top: 0,
                    images: images,
                    currency: $scope.advert.currency,
                    filters: prop_vals
                }).$promise.then(function () {
                        $location.path('/adverts/')
                    })

            }

        }

        $scope.remove_image = function (id) {

            return alert(id)
        }


        function findById(arr, id) {

            var res_item = false;
            arr.forEach(function(item) {

                if ( item.id == id ) {
                    res_item = item
                }

            });

            return res_item
        }

        function removeById(arr, id) {

            arr.forEach(function(item) {

                if ( item.id == id ) {
                    arr.splice( arr.indexOf(item) , 1)
                }

            });

        }

    }
);


"use strict";

auto.controller('PackagesCtrl',['$scope', '$http', 'Service'
    , function ($scope, $http, Service ) {

        Service.index().$promise.then(function(data) {
            $scope.services = data.reverse()
        })


        $scope._add = function(service) {

            Service.add({id: service.id }).$promise.then(function() {

                service.on = 1

            })

        }
    }]);
"use strict";

auto.controller('PageShowCtrl',
    function ($scope, Pages, Lang, $stateParams) {

        Pages.show({id: $stateParams.id}).$promise.then(function(data){

            $scope.langs = data;
            $scope.current_lang = $scope.langs[0]

        })



        $scope.setLang = function(lang) {

            $scope.current_lang = lang

        }

        $scope.updatePage = function() {
            $scope.current_lang.text = CKEDITOR.instances.editor1.getData();
            Pages.update_lang({id: $stateParams.id, page:$scope.current_lang}).$promise.then(function(data){


            })

        }

    });
"use strict";

auto.controller('PagesCtrl',
    function ($scope, Pages, $state) {

        $scope.actions = {
            show: 'true',
            edit: 'false',
            remove: 'false'
        }

        Pages.index().$promise.then(function(data) {
            $scope.items = data.pages.reverse();
            $scope.values = data.vals;
        })

        $scope.show = function(page) {

            $state.go('app.auto-page-show' , {id: page.id})

        }

    });
"use strict";

auto.controller('ServiceCtrl',
    function ($scope, $http, Service ,  $rootScope, $modal, $sce) {


        $scope.openModal = function(modal_id, modal_size, modal_backdrop)
        {
            $rootScope.currentModal = $modal.open({
                templateUrl: modal_id,
                size: modal_size,
                backdrop: typeof modal_backdrop == 'undefined' ? true : modal_backdrop
            });
        };

        Service.index().$promise.then(function(data) {
            $scope.items = data.services.reverse()
            $scope.values = data.vals
        })

        $scope._create = function() {
            Service.create({service :$scope.new_service}).$promise.then(function(data){

                $scope.services.unshift(data);
                $('#createModel').modal('hide');

            })
        }

        $scope.update_model = function(servise) {

            $scope.update_service = servise;
            $('#updateModel').modal('show');

        }

        $scope._edit = function() {

            Service.update({id: $scope.update_service.id, service :$scope.update_service}).$promise.then(function(data){

                $('#updateModel').modal('hide');

            })

        }

        $scope._remove = function(service) {

            Service.destroy({ id: service.id}).$promise.then(function(data){

                $scope.services.splice($scope.services.indexOf(service), 1);


            })
        }

    });
auto.controller('SettingsPaneCtrl', function($rootScope)
{
    // Define Settings Pane Public Variable
    public_vars.$settingsPane = public_vars.$body.find('.settings-pane');
    public_vars.$settingsPaneIn = public_vars.$settingsPane.find('.settings-pane-inner');
})
"use strict";

auto.controller('ShowFiltersCtrl',
    function ($scope, $http, FilterValue, $state, $stateParams ,  $rootScope, $modal, $sce) {

        $scope.props = true;
        $scope.langs = false;
        $scope.new_value = {};


        $scope.actions = {
            show: 'true',
            edit: 'true',
            remove: 'true'
        }

        $scope.openModal = function(modal_id, modal_size, modal_backdrop)
        {
            $rootScope.currentModal = $modal.open({
                templateUrl: modal_id,
                size: modal_size,
                scope: $scope,
                backdrop: typeof modal_backdrop == 'undefined' ? true : modal_backdrop
            });
        };

        $scope.openLang = function() {

            $state.go('app.auto-filter-show-lang' , {id: $stateParams.id})

        };

        FilterValue.index({p_id: $stateParams.id}).
            $promise.then(function(data) {

                $scope.items = data.prop_vals.reverse()
                $scope.values = data.vals

            });

        $scope._create = function() {

            var fd = new FormData();
            var image =  document.getElementById('file_val').files[0];
            if (image != null ) {
                fd.append('prop_val[image]', image);
            }
            fd.append('prop_val[title]',  $scope.new_value.title );
            fd.append('prop_val[color]',  $scope.new_value.color);
            fd.append('p_id',  $stateParams.id );

            var xhr = new XMLHttpRequest();
            xhr.open("POST", "prop_vals"); // Boooom!
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))

            xhr.onload = function(event) {

                var jsonObj = JSON.parse(this.responseText);
                $scope.items.unshift(jsonObj);
                $rootScope.currentModal.close();
                $scope.$apply();

            }

            xhr.send(fd);

        }

        $scope._remove = function(value) {

            FilterValue.destroy({id: value.id})
            $scope.items.splice($scope.items.indexOf(value), 1);

        }

        $scope.showEdit = function(value) {

            $scope.update_value = value;
            $('#updateModel').modal('show');

        }

        $scope._edit = function() {

            var fd = new FormData();
            var image =  document.getElementById('file_val_update').files[0];
            if (image != null ) {
                fd.append('prop_val[image]', image);
            }
            fd.append('prop_val[title]',  $scope.update_value.title );
            fd.append('prop_val[color]',  $scope.update_value.color);
            fd.append('p_id',  $stateParams.id );

            var xhr = new XMLHttpRequest();
            xhr.open("PUT", "prop_vals/" + $scope.update_value.id);
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))

            xhr.onload = function(event) {

                $scope.update_value.image = JSON.parse(this.responseText).image
                $('#updateModel').modal('hide')
                $scope.$apply()

            }

            xhr.send(fd);



        }

        $scope.setTab = function (page) {

            switch (page) {
                case 0:
                    $scope.props = true;
                    $scope.langs = false;
                    break
                case 1:
                    $scope.props =false;
                    $scope.langs = true;
                    break
                default:
                    break
            }

        }

        $scope.show = function(prop_val) {

            $state.go('app.auto-filter-subVel' , {id: prop_val.id})

        }


    });

"use strict";

auto.controller('UsersCtrl', function ($scope, Users ) {

    Users.index().
        $promise.then(function(data) {

            $scope.items = data.users.reverse();
            $scope.values = data.vals;

        });

});
"use strict";

auto.controller('VelRelCtrl',
    function ($scope, $http, FilterValue, $state, $stateParams ,  $rootScope, $modal, $sce ) {

        $scope.props = true;
        $scope.langs = false;
        $scope.new_value = {};


        $scope.actions = {
            show: 'true',
            edit: 'true',
            remove: 'true'
        }

        $scope.openModal = function(modal_id, modal_size, modal_backdrop)
        {
            $rootScope.currentModal = $modal.open({
                templateUrl: modal_id,
                size: modal_size,
                scope: $scope,
                backdrop: typeof modal_backdrop == 'undefined' ? true : modal_backdrop
            });
        };

        FilterValue.getSub({v_id: $stateParams.id}).
            $promise.then(function(data) {

                $scope.items = data.prop_vals.reverse()
                $scope.values = data.vals

            });


        $scope._create = function() {

            var fd = new FormData();
            var image =  document.getElementById('file_val').files[0];
            if (image != null ) {
                fd.append('prop_val[image]', image);
            }
            fd.append('prop_val[title]',  $scope.new_value.title );
            fd.append('prop_val[color]',  $scope.new_value.color);
            fd.append('v_id',  $stateParams.id );

            var xhr = new XMLHttpRequest();
            xhr.open("POST", "prop_vals/add_sub"); // Boooom!
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))

            xhr.onload = function(event) {

                var jsonObj = JSON.parse(this.responseText);
                $scope.items.unshift(jsonObj);
                $rootScope.currentModal.close()
                $scope.$apply()

            }

            xhr.send(fd);

        }

        $scope._remove = function(value) {

            FilterValue.destroy({id: value.id})
            $scope.items.splice($scope.items.indexOf(value), 1);

        }

        $scope.showEdit = function(value) {

            $scope.update_value = value;
            $('#updateModel').modal('show');

        }

        $scope._edit = function() {

            var fd = new FormData();
            var image =  document.getElementById('file_val_update').files[0];
            if (image != null ) {
                fd.append('prop_val[image]', image);
            }
            fd.append('prop_val[title]',  $scope.update_value.title );
            fd.append('prop_val[color]',  $scope.update_value.color);
            fd.append('p_id',  $stateParams.id );

            var xhr = new XMLHttpRequest();
            xhr.open("PUT", "prop_vals/" + $scope.update_value.id);
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))

            xhr.onload = function(event) {

                $scope.update_value.image = JSON.parse(this.responseText).image
                $('#updateModel').modal('hide')
                $scope.$apply()

            }

            xhr.send(fd);



        }

        $scope.setTab = function (page) {

            switch (page) {
                case 0:
                    $scope.props = true;
                    $scope.langs = false;
                    break
                case 1:
                    $scope.props =false;
                    $scope.langs = true;
                    break
                default:
                    break
            }

        }

        $scope.show = function(prop_val) {
            $state.go('app.auto-filter-subVel' , {id: prop_val.id})
        }


    });

angular.module('auto.directives', []).

	// Layout Related Directives
	directive('settingsPane', function(){
		return {
			restrict: 'E',
			templateUrl: appHelper.templatePath('layout/settings-pane'),
			controller: 'SettingsPaneCtrl'
		};
	}).
	directive('horizontalMenu', function(){
		return {
			restrict: 'E',
			replace: true,
			templateUrl: appHelper.templatePath('layout/horizontal-menu'),
			controller: 'HorizontalMenuCtrl'
		}
	}).
	directive('sidebarMenu', function(){
		return {
			restrict: 'E',
			templateUrl: appHelper.templatePath('layout/sidebar-menu'),
			controller: 'SidebarMenuCtrl'
		};
	}).
	directive('sidebarChat', function(){
		return {
			restrict: 'E',
			replace: true,
			templateUrl: appHelper.templatePath('layout/sidebar-chat')
		};
	}).
	directive('footerChat', function(){
		return {
			restrict: 'E',
			replace: true,
			controller: 'FooterChatCtrl',
			templateUrl: appHelper.templatePath('layout/footer-chat')
		};
	}).
	directive('sidebarLogo', function(){
		return {
			restrict: 'E',
			replace: true,
			templateUrl: appHelper.templatePath('layout/sidebar-logo')
		};
	}).
	directive('sidebarProfile', function(){
		return {
			restrict: 'E',
			replace: true,
			templateUrl: appHelper.templatePath('layout/sidebar-profile')
		};
	}).
	directive('userInfoNavbar', function(){
		return {
			restrict: 'E',
			replace: true,
			templateUrl: appHelper.templatePath('layout/user-info-navbar')
		};
	}).
	directive('pageTitle', function(){
		return {
			restrict: 'E',
			replace: true,
			templateUrl: appHelper.templatePath('layout/page-title'),
			link: function(scope, el, attr){
				scope.title = attr.title;
				scope.description = attr.description;
			}
		};
	}).
	directive('siteFooter', function(){
		return {
			restrict: 'E',
			templateUrl: appHelper.templatePath('layout/footer')
		};
	}).
	directive('xeBreadcrumb', function(){
		return {
			restrict: 'A',
			link: function(scope, el)
			{
				var $bc = angular.element(el);

				if($bc.hasClass('auto-hidden'))
				{
					var $as = $bc.find('li a'),
						collapsed_width = $as.width(),
						expanded_width = 0;

					$as.each(function(i, el)
					{
						var $a = $(el);

						expanded_width = $a.outerWidth(true);
						$a.addClass('collapsed').width(expanded_width);

						$a.hover(function()
						{
							$a.removeClass('collapsed');
						},
						function()
						{
							$a.addClass('collapsed');
						});
					});
				}
			}
		}
	}).

	// Widgets Directives
	directive('xeCounter', function(){

		return {
			restrict: 'EAC',
			link: function(scope, el, attrs)
			{
				var $el = angular.element(el),
					sm = scrollMonitor.create(el);

				sm.fullyEnterViewport(function()
				{
					var opts = {
						useEasing: 		attrDefault($el, 'easing', true),
						useGrouping:	attrDefault($el, 'grouping', true),
						separator: 		attrDefault($el, 'separator', ','),
						decimal: 		attrDefault($el, 'decimal', '.'),
						prefix: 		attrDefault($el, 'prefix', ''),
						suffix:			attrDefault($el, 'suffix', ''),
					},
					$count		= attrDefault($el, 'count', 'this') == 'this' ? $el : $el.find($el.data('count')),
					from        = attrDefault($el, 'from', 0),
					to          = attrDefault($el, 'to', 100),
					duration    = attrDefault($el, 'duration', 2.5),
					delay       = attrDefault($el, 'delay', 0),
					decimals	= new String(to).match(/\.([0-9]+)/) ? new String(to).match(/\.([0-9]+)$/)[1].length : 0,
					counter 	= new countUp($count.get(0), from, to, decimals, duration, opts);

					setTimeout(function(){ counter.start(); }, delay * 1000);

					sm.destroy();
				});
			}
		};
	}).
	directive('xeFillCounter', function(){

		return {
			restrict: 'EAC',
			link: function(scope, el, attrs)
			{
				var $el = angular.element(el),
					sm = scrollMonitor.create(el);

				sm.fullyEnterViewport(function()
				{
					var fill = {
						current: 	null,
						from: 		attrDefault($el, 'fill-from', 0),
						to: 		attrDefault($el, 'fill-to', 100),
						property: 	attrDefault($el, 'fill-property', 'width'),
						unit: 		attrDefault($el, 'fill-unit', '%'),
					},
					opts 		= {
						current: fill.to, onUpdate: function(){
							$el.css(fill.property, fill.current + fill.unit);
						},
						delay: attrDefault($el, 'delay', 0),
					},
					easing 		= attrDefault($el, 'fill-easing', true),
					duration 	= attrDefault($el, 'fill-duration', 2.5);

					if(easing)
					{
						opts.ease = Sine.easeOut;
					}

					// Set starting point
					fill.current = fill.from;

					TweenMax.to(fill, duration, opts);

					sm.destroy();
				});
			}
		};
	}).
	directive('xeStatusUpdate', function(){

		return {
			restrict: 'EAC',
			link: function(scope, el, attrs)
			{
				var $el          	= angular.element(el),
					$nav            = $el.find('.xe-nav a'),
					$status_list    = $el.find('.xe-body li'),
					index           = $status_list.filter('.active').index(),
					auto_switch     = attrDefault($el, 'auto-switch', 0),
					as_interval		= 0;

				if(auto_switch > 0)
				{
					as_interval = setInterval(function()
					{
						goTo(1);

					}, auto_switch * 1000);

					$el.hover(function()
					{
						window.clearInterval(as_interval);
					},
					function()
					{
						as_interval = setInterval(function()
						{
							goTo(1);

						}, auto_switch * 1000);;
					});
				}

				function goTo(plus_one)
				{
					index = (index + plus_one) % $status_list.length;

					if(index < 0)
						index = $status_list.length - 1;

					var $to_hide = $status_list.filter('.active'),
						$to_show = $status_list.eq(index);

					$to_hide.removeClass('active');
					$to_show.addClass('active').fadeTo(0,0).fadeTo(320,1);
				}

				$nav.on('click', function(ev)
				{
					ev.preventDefault();

					var plus_one = $(this).hasClass('xe-prev') ? -1 : 1;

					goTo(plus_one);
				});
			}
		};
	}).

	// Extra (Section) Directives
	directive('tocify', function(){
		return {
			restrict: 'AC',
			link: function(scope, el, attr)
			{
				if( ! jQuery.isFunction(jQuery.fn.tocify))
					return false;

				var $this = angular.element(el),
					watcher = scrollMonitor.create($this.get(0));

				$this.tocify({
					context: '.tocify-content',
					selectors: "h2,h3,h4,h5"
				});


				$this.width( $this.parent().width() );

				watcher.lock();

				watcher.stateChange(function()
				{
					$($this.get(0)).toggleClass('fixed', this.isAboveViewport)
				});
			}
		}
	}).
	directive('scrollable', function(){
		return {
			restrict: 'AC',
			link: function(scope, el, attr)
			{
				if( ! jQuery.isFunction(jQuery.fn.perfectScrollbar))
					return false;

				var $this = angular.element(el),
					max_height = parseInt(attrDefault($this, 'max-height', 200), 10);

				max_height = max_height < 0 ? 200 : max_height;

				$this.css({maxHeight: max_height}).perfectScrollbar({
					wheelPropagation: true
				});
			}
		}
	}).

	// Forms Directives
	directive('tagsinput', function(){
		return {
			restrict: 'AC',
			link: function(scope, el, attr)
			{
				var $el = angular.element(el);

				if( ! jQuery.isFunction(jQuery.fn.tagsinput))
					return false;

				$el.tagsinput();
			}
		}
	}).
	directive('dropzone', function(){
		return {
			restrict: 'AC',
			link: function(scope, el, attr)
			{
				var $el = angular.element(el);

				if( ! jQuery.isFunction(jQuery.fn.dropzone))
					return false;

				$el.dropzone();
			}
		}
	}).
	directive('wysihtml5', function(){

		return {
			restrict: 'AC',
			link: function(scope, el, attr)
			{
				if( ! jQuery.isFunction(jQuery.fn.wysihtml5))
					return false;

				var $this = angular.element(el),
					stylesheets = attrDefault($this, 'stylesheet-url', '')


				$(".wysihtml5").wysihtml5({
					size: 'white',
					stylesheets: stylesheets.split(','),
					"html": attrDefault($this, 'html', true),
					"color": attrDefault($this, 'colors', true),
				});
			}
		}
	}).
	directive('autogrow', function(){
		return {
			restrict: 'AC',
			link: function(scope, el, attr)
			{
				if( ! jQuery.isFunction(jQuery.fn.autosize))
					return false;

				var $el = angular.element(el);

				$el.autosize();
			}
		}
	}).
	directive('slider', function(){
		return {
			restrict: 'AC',
			link: function(scope, el, attr)
			{
				if( ! jQuery.isFunction(jQuery.fn.slider))
					return false;

				var $this = angular.element(el),
					$label_1 = $('<span class="ui-label"></span>'),
					$label_2 = $label_1.clone(),

					orientation = attrDefault($this, 'vertical', 0) != 0 ? 'vertical' : 'horizontal',

					prefix = attrDefault($this, 'prefix', ''),
					postfix = attrDefault($this, 'postfix', ''),

					fill = attrDefault($this, 'fill', ''),
					$fill = $(fill),

					step = attrDefault($this, 'step', 1),
					value = attrDefault($this, 'value', 5),
					min = attrDefault($this, 'min', 0),
					max = attrDefault($this, 'max', 100),
					min_val = attrDefault($this, 'min-val', 10),
					max_val = attrDefault($this, 'max-val', 90),

					is_range = $this.is('[data-min-val]') || $this.is('[data-max-val]'),

					reps = 0;


				// Range Slider Options
				if(is_range)
				{
					$this.slider({
						range: true,
						orientation: orientation,
						min: min,
						max: max,
						values: [min_val, max_val],
						step: step,
						slide: function(e, ui)
						{
							var min_val = (prefix ? prefix : '') + ui.values[0] + (postfix ? postfix : ''),
								max_val = (prefix ? prefix : '') + ui.values[1] + (postfix ? postfix : '');

							$label_1.html( min_val );
							$label_2.html( max_val );

							if(fill)
								$fill.val(min_val + ',' + max_val);

							reps++;
						},
						change: function(ev, ui)
						{
							if(reps == 1)
							{
								var min_val = (prefix ? prefix : '') + ui.values[0] + (postfix ? postfix : ''),
									max_val = (prefix ? prefix : '') + ui.values[1] + (postfix ? postfix : '');

								$label_1.html( min_val );
								$label_2.html( max_val );

								if(fill)
									$fill.val(min_val + ',' + max_val);
							}

							reps = 0;
						}
					});

					var $handles = $this.find('.ui-slider-handle');

					$label_1.html((prefix ? prefix : '') + min_val + (postfix ? postfix : ''));
					$handles.first().append( $label_1 );

					$label_2.html((prefix ? prefix : '') + max_val+ (postfix ? postfix : ''));
					$handles.last().append( $label_2 );
				}
				// Normal Slider
				else
				{

					$this.slider({
						range: attrDefault($this, 'basic', 0) ? false : "min",
						orientation: orientation,
						min: min,
						max: max,
						value: value,
						step: step,
						slide: function(ev, ui)
						{
							var val = (prefix ? prefix : '') + ui.value + (postfix ? postfix : '');

							$label_1.html( val );


							if(fill)
								$fill.val(val);

							reps++;
						},
						change: function(ev, ui)
						{
							if(reps == 1)
							{
								var val = (prefix ? prefix : '') + ui.value + (postfix ? postfix : '');

								$label_1.html( val );

								if(fill)
									$fill.val(val);
							}

							reps = 0;
						}
					});

					var $handles = $this.find('.ui-slider-handle');
						//$fill = $('<div class="ui-fill"></div>');

					$label_1.html((prefix ? prefix : '') + value + (postfix ? postfix : ''));
					$handles.html( $label_1 );

					//$handles.parent().prepend( $fill );

					//$fill.width($handles.get(0).style.left);
				};
			}
		}
	}).
	directive('formWizard', function(){
		return {
			restrict: 'AC',
			link: function(scope, el, attr)
			{
				if( ! jQuery.isFunction(jQuery.fn.bootstrapWizard))
					return;

				var $this = $(el),
					$tabs = $this.find('> .tabs > li'),
					$progress = $this.find(".progress-indicator"),
					_index = $this.find('> ul > li.active').index();

				// Validation
				var checkFormWizardValidaion = function(tab, navigation, index)
					{
			  			if($this.hasClass('validate'))
			  			{
							var $valid = $this.valid();

							if( ! $valid)
							{
								$this.data('validator').focusInvalid();
								return false;
							}
						}

				  		return true;
					};


				// Setup Progress
				if(_index > 0)
				{
					$progress.css({width: _index/$tabs.length * 100 + '%'});
					$tabs.removeClass('completed').slice(0, _index).addClass('completed');
				}

				$this.bootstrapWizard({
					tabClass: "",
			  		onTabShow: function($tab, $navigation, index)
			  		{
			  			var pct = $tabs.eq(index).position().left / $tabs.parent().width() * 100;

			  			$tabs.removeClass('completed').slice(0, index).addClass('completed');
			  			$progress.css({width: pct + '%'});
			  		},

			  		onNext: checkFormWizardValidaion,
			  		onTabClick: checkFormWizardValidaion
			  	});

			  	$this.data('bootstrapWizard').show( _index );

			  	$this.find('.pager a').on('click', function(ev)
			  	{
				  	ev.preventDefault();
			  	});
			}
		}
	}).
	directive('colorpicker', function(){
		return {
			restrict: 'AC',
			link: function(scope, el, attr)
			{
				if( ! jQuery.isFunction(jQuery.fn.colorpicker))
					return false;

				var $this = angular.element(el),
					opts = {
					},
					$n = $this.next(),
					$p = $this.prev(),

					$preview = $this.siblings('.input-group-addon').find('.color-preview');

				$this.colorpicker(opts);

				if($n.is('.input-group-addon') && $n.has('a'))
				{
					$n.on('click', function(ev)
					{
						ev.preventDefault();

						$this.colorpicker('show');
					});
				}

				if($p.is('.input-group-addon') && $p.has('a'))
				{
					$p.on('click', function(ev)
					{
						ev.preventDefault();

						$this.colorpicker('show');
					});
				}

				if($preview.length)
				{
					$this.on('changeColor', function(ev){

						$preview.css('background-color', ev.color.toHex());
					});

					if($this.val().length)
					{
						$preview.css('background-color', $this.val());
					}
				}
			}
		}
	}).
	directive('validate', function(){
		return {
			restrict: 'AC',
			link: function(scope, el, attr)
			{
				if( ! jQuery.isFunction(jQuery.fn.validate))
					return false;

				var $this = angular.element(el),
					opts = {
						rules: {},
						messages: {},
						errorElement: 'span',
						errorClass: 'validate-has-error',
						highlight: function (element) {
							$(element).closest('.form-group').addClass('validate-has-error');
						},
						unhighlight: function (element) {
							$(element).closest('.form-group').removeClass('validate-has-error');
						},
						errorPlacement: function (error, element)
						{
							if(element.closest('.has-switch').length)
							{
								error.insertAfter(element.closest('.has-switch'));
							}
							else
							if(element.parent('.checkbox, .radio').length || element.parent('.input-group').length)
							{
								error.insertAfter(element.parent());
							}
							else
							{
								error.insertAfter(element);
							}
						}
					},
					$fields = $this.find('[data-validate]');


				$fields.each(function(j, el2)
				{
					var $field = $(el2),
						name = $field.attr('name'),
						validate = attrDefault($field, 'validate', '').toString(),
						_validate = validate.split(',');

					for(var k in _validate)
					{
						var rule = _validate[k],
							params,
							message;

						if(typeof opts['rules'][name] == 'undefined')
						{
							opts['rules'][name] = {};
							opts['messages'][name] = {};
						}

						if($.inArray(rule, ['required', 'url', 'email', 'number', 'date', 'creditcard']) != -1)
						{
							opts['rules'][name][rule] = true;

							message = $field.data('message-' + rule);

							if(message)
							{
								opts['messages'][name][rule] = message;
							}
						}
						// Parameter Value (#1 parameter)
						else
						if(params = rule.match(/(\w+)\[(.*?)\]/i))
						{
							if($.inArray(params[1], ['min', 'max', 'minlength', 'maxlength', 'equalTo']) != -1)
							{
								opts['rules'][name][params[1]] = params[2];


								message = $field.data('message-' + params[1]);

								if(message)
								{
									opts['messages'][name][params[1]] = message;
								}
							}
						}
					}
				});

				$this.validate(opts);
			}
		}
	}).
	directive('inputmask', function(){
		return {
			restrict: 'AC',
			link: function(scope, el, attr)
			{
				if( ! jQuery.isFunction(jQuery.fn.inputmask))
					return false;

				var $this = angular.element(el),
					mask = $this.data('mask').toString(),
					opts = {
						numericInput: attrDefault($this, 'numeric', false),
						radixPoint: attrDefault($this, 'radixPoint', ''),
						rightAlign: attrDefault($this, 'numericAlign', 'left') == 'right'
					},
					placeholder = attrDefault($this, 'placeholder', ''),
					is_regex = attrDefault($this, 'isRegex', '');

				if(placeholder.length)
				{
					opts[placeholder] = placeholder;
				}

				switch(mask.toLowerCase())
				{
					case "phone":
						mask = "(999) 999-9999";
						break;

					case "currency":
					case "rcurrency":

						var sign = attrDefault($this, 'sign', '$');;

						mask = "999,999,999.99";

						if($this.data('mask').toLowerCase() == 'rcurrency')
						{
							mask += ' ' + sign;
						}
						else
						{
							mask = sign + ' ' + mask;
						}

						opts.numericInput = true;
						opts.rightAlignNumerics = false;
						opts.radixPoint = '.';
						break;

					case "email":
						mask = 'Regex';
						opts.regex = "[a-zA-Z0-9._%-]+@[a-zA-Z0-9-]+\\.[a-zA-Z]{2,4}";
						break;

					case "fdecimal":
						mask = 'decimal';
						$.extend(opts, {
							autoGroup		: true,
							groupSize		: 3,
							radixPoint		: attrDefault($this, 'rad', '.'),
							groupSeparator	: attrDefault($this, 'dec', ',')
						});
				}

				if(is_regex)
				{
					opts.regex = mask;
					mask = 'Regex';
				}

				$this.inputmask(mask, opts);
			}
		}
	}).
	directive('timepicker', function(){
		return {
			restrict: 'AC',
			link: function(scope, el, attr)
			{
				if( ! jQuery.isFunction(jQuery.fn.timepicker))
					return false;

				var $this = angular.element(el),
					opts = {
						template: attrDefault($this, 'template', false),
						showSeconds: attrDefault($this, 'showSeconds', false),
						defaultTime: attrDefault($this, 'defaultTime', 'current'),
						showMeridian: attrDefault($this, 'showMeridian', true),
						minuteStep: attrDefault($this, 'minuteStep', 15),
						secondStep: attrDefault($this, 'secondStep', 15)
					},
					$n = $this.next(),
					$p = $this.prev();

				$this.timepicker(opts);

				if($n.is('.input-group-addon') && $n.has('a'))
				{
					$n.on('click', function(ev)
					{
						ev.preventDefault();

						$this.timepicker('showWidget');
					});
				}

				if($p.is('.input-group-addon') && $p.has('a'))
				{
					$p.on('click', function(ev)
					{
						ev.preventDefault();

						$this.timepicker('showWidget');
					});
				}
			}
		}
	}).
	directive('datepicker', function(){
		return {
			restrict: 'AC',
			link: function(scope, el, attr)
			{
				if( ! jQuery.isFunction(jQuery.fn.datepicker))
					return false;

				var $this = angular.element(el),
					opts = {
						format: attrDefault($this, 'format', 'mm/dd/yyyy'),
						startDate: attrDefault($this, 'startDate', ''),
						endDate: attrDefault($this, 'endDate', ''),
						daysOfWeekDisabled: attrDefault($this, 'disabledDays', ''),
						startView: attrDefault($this, 'startView', 0),
						//rtl: rtl()
					},
					$n = $this.next(),
					$p = $this.prev();

				$this.datepicker(opts);

				if($n.is('.input-group-addon') && $n.has('a'))
				{
					$n.on('click', function(ev)
					{
						ev.preventDefault();

						$this.datepicker('show');
					});
				}

				if($p.is('.input-group-addon') && $p.has('a'))
				{
					$p.on('click', function(ev)
					{
						ev.preventDefault();

						$this.datepicker('show');
					});
				}
			}
		}
	}).
	directive('daterange', function(){
		return {
			restrict: 'AC',
			link: function(scope, el, attr)
			{
				if( ! jQuery.isFunction(jQuery.fn.daterangepicker))
					return false;

				var $this = angular.element(el);

					// Change the range as you desire
				var ranges = {
					'Today': [moment(), moment()],
					'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
					'Last 7 Days': [moment().subtract('days', 6), moment()],
					'Last 30 Days': [moment().subtract('days', 29), moment()],
					'This Month': [moment().startOf('month'), moment().endOf('month')],
					'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
				};

				var opts    = {
						format: attrDefault($this, 'format', 'MM/DD/YYYY'),
						timePicker: attrDefault($this, 'timePicker', false),
						timePickerIncrement: attrDefault($this, 'timePickerIncrement', false),
						separator: attrDefault($this, 'separator', ' - '),
					},
					min_date   = attrDefault($this, 'minDate', ''),
					max_date   = attrDefault($this, 'maxDate', ''),
					start_date = attrDefault($this, 'startDate', ''),
					end_date   = attrDefault($this, 'endDate', '');

				if($this.hasClass('add-ranges'))
				{
					opts['ranges'] = ranges;
				}

				if(min_date.length)
				{
					opts['minDate'] = min_date;
				}

				if(max_date.length)
				{
					opts['maxDate'] = max_date;
				}

				if(start_date.length)
				{
					opts['startDate'] = start_date;
				}

				if(end_date.length)
				{
					opts['endDate'] = end_date;
				}


				$this.daterangepicker(opts, function(start, end)
				{
					var drp = $this.data('daterangepicker');

					if($this.is('[data-callback]'))
					{
						//daterange_callback(start, end);
						callback_test(start, end);
					}

					if($this.hasClass('daterange-inline'))
					{
						$this.find('span').html(start.format(drp.format) + drp.separator + end.format(drp.format));
					}
				});

				if(typeof opts['ranges'] == 'object')
				{
					$this.data('daterangepicker').container.removeClass('show-calendar');
				}
			}
		}
	}).
	directive('spinner', function(){
		return {
			restrict: 'AC',
			link: function(scope, el, attr)
			{
				var $ig = angular.element(el),
					$dec = $ig.find('[data-type="decrement"]'),
					$inc = $ig.find('[data-type="increment"]'),
					$inp = $ig.find('.form-control'),

					step = attrDefault($ig, 'step', 1),
					min = attrDefault($ig, 'min', 0),
					max = attrDefault($ig, 'max', 0),
					umm = min < max;


				$dec.on('click', function(ev)
				{
					ev.preventDefault();

					var num = new Number($inp.val()) - step;

					if(umm && num <= min)
					{
						num = min;
					}

					$inp.val(num);
				});

				$inc.on('click', function(ev)
				{
					ev.preventDefault();

					var num = new Number($inp.val()) + step;

					if(umm && num >= max)
					{
						num = max;
					}

					$inp.val(num);
				});
			}
		}
	}).

	// Other Directives
	directive('loginForm', function(){
		return {
			restrict: 'AC',
			link: function(scope, el){

				jQuery(el).find(".form-group:has(label)").each(function(i, el)
				{
					var $this = angular.element(el),
						$label = $this.find('label'),
						$input = $this.find('.form-control');

						$input.on('focus', function()
						{
							$this.addClass('is-focused');
						});

						$input.on('keydown', function()
						{
							$this.addClass('is-focused');
						});

						$input.on('blur', function()
						{
							$this.removeClass('is-focused');

							if($input.val().trim().length > 0)
							{
								$this.addClass('is-focused');
							}
						});

						$label.on('click', function()
						{
							$input.focus();
						});

						if($input.val().trim().length > 0)
						{
							$this.addClass('is-focused');
						}
				});
			}
		};
	});
auto.filter('currencyService', function() {
    return function(input) {

        switch (input) {

            case '1':

                return 'RUB'

                break
            case '2':

                return 'EUR'

                break
            case '3':

                return 'USD'

                break
            default:

                return 'Undefined'

                break
        }



    };
});

auto.filter('durationFilter', function() {
    return function(input) {

        switch (input) {

            case '302400':

                return 'Две недели'

                break
            case '602800':

                return 'Месяц'

                break
            case '1209600':

                return 'Два месяца'

                break
            default:

                return 'Undefined'

                break
        }



    };
});

auto.filter('filterType', function() {
    return function(input) {

        switch (input) {

            case 1:

                return 'Text'

                break
            case 2:

                return 'Checkbox'

                break
            case 3:

                return 'Radio'

                break
            case 4:

                return 'Select'

                break
            case 5:

                return 'Color'

                break
            case 6:

                return 'Image'

                break
            default:

                return 'Undefined'

                break
        }



    };
});


'use strict'

auto.factory("Advert", function($resource) {
    return $resource("posters/:id", { id: "@id" },
        {
            'create':  { method: 'POST', isArray: false },
            'setTop':  { method: 'GET', url: 'poster/set_top/:id' , isArray: false},
            'index':   { method: 'GET', isArray: false },
            'show':    { method: 'GET', isArray: false },
            'update':  { method: 'PUT', isArray: false  },
            'destroy': { method: 'DELETE' }
        }
    );
});


angular.module('auto.factory', []).
    factory('$layoutToggles', function($rootScope, $layout){

        return {

            initToggles: function()
            {
                // Sidebar Toggle
                $rootScope.sidebarToggle = function()
                {
                    $layout.setOptions('sidebar.isCollapsed', ! $rootScope.layoutOptions.sidebar.isCollapsed);
                };


                // Settings Pane
                $rootScope.settingsPaneToggle = function()
                {
                    var use_animation = $rootScope.layoutOptions.settingsPane.useAnimation && ! isxs();

                    var scroll = {
                        top: jQuery(document).scrollTop(),
                        toTop: 0
                    };

                    if(public_vars.$body.hasClass('settings-pane-open'))
                    {
                        scroll.toTop = scroll.top;
                    }

                    TweenMax.to(scroll, (use_animation ? .1 : 0), {top: scroll.toTop, roundProps: ['top'], ease: scroll.toTop < 10 ? null : Sine.easeOut, onUpdate: function()
                    {
                        jQuery(window).scrollTop( scroll.top );
                    },
                        onComplete: function()
                        {
                            if(use_animation)
                            {
                                // With Animation
                                public_vars.$settingsPaneIn.addClass('with-animation');

                                // Opening
                                if( ! public_vars.$settingsPane.is(':visible'))
                                {
                                    public_vars.$body.addClass('settings-pane-open');

                                    var height = public_vars.$settingsPane.outerHeight(true);

                                    public_vars.$settingsPane.css({
                                        height: 0
                                    });

                                    TweenMax.to(public_vars.$settingsPane, .25, {css: {height: height}, ease: Circ.easeInOut, onComplete: function()
                                    {
                                        public_vars.$settingsPane.css({height: ''});
                                    }});

                                    public_vars.$settingsPaneIn.addClass('visible');
                                }
                                // Closing
                                else
                                {
                                    public_vars.$settingsPaneIn.addClass('closing');

                                    TweenMax.to(public_vars.$settingsPane, .25, {css: {height: 0}, delay: .15, ease: Power1.easeInOut, onComplete: function()
                                    {
                                        public_vars.$body.removeClass('settings-pane-open');
                                        public_vars.$settingsPane.css({height: ''});
                                        public_vars.$settingsPaneIn.removeClass('closing visible');
                                    }});
                                }
                            }
                            else
                            {
                                // Without Animation
                                public_vars.$body.toggleClass('settings-pane-open');
                                public_vars.$settingsPaneIn.removeClass('visible');
                                public_vars.$settingsPaneIn.removeClass('with-animation');

                                $layout.setOptions('settingsPane.isOpen', ! $rootScope.layoutOptions.settingsPane.isOpen);
                            }
                        }
                    });
                };


                // Chat Toggle
                $rootScope.chatToggle = function()
                {
                    $layout.setOptions('chat.isOpen', ! $rootScope.layoutOptions.chat.isOpen);
                };


                // Mobile Menu Toggle
                $rootScope.mobileMenuToggle = function()
                {
                    $layout.setOptions('sidebar.isMenuOpenMobile', ! $rootScope.layoutOptions.sidebar.isMenuOpenMobile);
                    $layout.setOptions('horizontalMenu.isMenuOpenMobile', ! $rootScope.layoutOptions.horizontalMenu.isMenuOpenMobile);
                };


                // Mobile User Info Navbar Toggle
                $rootScope.mobileUserInfoToggle = function()
                {
                    $layout.setOptions('userInfoNavVisible', ! $rootScope.layoutOptions.userInfoNavVisible);
                }
            }
        };
    }).
    factory('$pageLoadingBar', function($rootScope, $window){

        return {

            init: function()
            {
                var pl = this;

                $window.showLoadingBar = this.showLoadingBar;
                $window.hideLoadingBar = this.hideLoadingBar;

                $rootScope.$on('$stateChangeStart', function()
                {
                    pl.showLoadingBar({
                        pct: 95,
                        delay: 1.1,
                        resetOnEnd: false
                    });

                    jQuery('body .page-container .main-content').addClass('is-loading');
                });

                $rootScope.$on('$stateChangeSuccess', function()
                {
                    pl.showLoadingBar({
                        pct: 100,
                        delay: .65,
                        resetOnEnd: true
                    });

                    jQuery('body .page-container .main-content').removeClass('is-loading');
                });
            },

            showLoadingBar: function(options)
            {
                var defaults = {
                        pct: 0,
                        delay: 1.3,
                        wait: 0,
                        before: function(){},
                        finish: function(){},
                        resetOnEnd: true
                    },
                    pl = this;

                if(typeof options == 'object')
                    defaults = jQuery.extend(defaults, options);
                else
                if(typeof options == 'number')
                    defaults.pct = options;


                if(defaults.pct > 100)
                    defaults.pct = 100;
                else
                if(defaults.pct < 0)
                    defaults.pct = 0;

                var $ = jQuery,
                    $loading_bar = $(".xenon-loading-bar");

                if($loading_bar.length == 0)
                {
                    $loading_bar = $('<div class="xenon-loading-bar progress-is-hidden"><span data-pct="0"></span></div>');
                    public_vars.$body.append( $loading_bar );
                }

                var $pct = $loading_bar.find('span'),
                    current_pct = $pct.data('pct'),
                    is_regress = current_pct > defaults.pct;


                defaults.before(current_pct);

                TweenMax.to($pct, defaults.delay, {css: {width: defaults.pct + '%'}, delay: defaults.wait, ease: is_regress ? Expo.easeOut : Expo.easeIn,
                    onStart: function()
                    {
                        $loading_bar.removeClass('progress-is-hidden');
                    },
                    onComplete: function()
                    {
                        var pct = $pct.data('pct');

                        if(pct == 100 && defaults.resetOnEnd)
                        {
                            hideLoadingBar();
                        }

                        defaults.finish(pct);
                    },
                    onUpdate: function()
                    {
                        $pct.data('pct', parseInt($pct.get(0).style.width, 10));
                    }});
            },

            hideLoadingBar: function()
            {
                var $ = jQuery,
                    $loading_bar = $(".xenon-loading-bar"),
                    $pct = $loading_bar.find('span');

                $loading_bar.addClass('progress-is-hidden');
                $pct.width(0).data('pct', 0);
            }
        };
    }).
    factory('$layout', function($rootScope, $cookies, $cookieStore){

        return {
            propsToCache: [
                'horizontalMenu.isVisible',
                'horizontalMenu.isFixed',
                'horizontalMenu.minimal',
                'horizontalMenu.clickToExpand',

                'sidebar.isVisible',
                'sidebar.isCollapsed',
                'sidebar.toggleOthers',
                'sidebar.isFixed',
                'sidebar.isRight',
                'sidebar.userProfile',

                'chat.isOpen',

                'container.isBoxed',

                'skins.sidebarMenu',
                'skins.horizontalMenu',
                'skins.userInfoNavbar'
            ],

            setOptions: function(options, the_value)
            {
                if(typeof options == 'string' && typeof the_value != 'undefined')
                {
                    options = this.pathToObject(options, the_value);
                }

                jQuery.extend(true, $rootScope.layoutOptions, options);

                this.saveCookies();
            },

            saveCookies: function()
            {
                var cookie_entries = this.iterateObject($rootScope.layoutOptions, '', {});

                angular.forEach(cookie_entries, function(value, prop)
                {
                    $cookies[prop] = value;
                });
            },

            resetCookies: function()
            {
                var cookie_entries = this.iterateObject($rootScope.layoutOptions, '', {});

                angular.forEach(cookie_entries, function(value, prop)
                {
                    $cookieStore.remove(prop);
                });
            },

            loadOptionsFromCookies: function()
            {
                var dis = this,
                    cookie_entries = dis.iterateObject($rootScope.layoutOptions, '', {}),
                    loaded_props = {};

                angular.forEach(cookie_entries, function(value, prop)
                {
                    var cookie_val = $cookies[prop];

                    if(typeof cookie_val != 'undefined')
                    {
                        jQuery.extend(true, loaded_props, dis.pathToObject(prop, cookie_val));
                    }
                });

                jQuery.extend($rootScope.layoutOptions, loaded_props);
            },

            is: function(prop, value)
            {
                var cookieval = this.get(prop);

                return cookieval == value;
            },

            get: function(prop)
            {
                var cookieval = $cookies[prop];

                if(cookieval && cookieval.match(/^true|false|[0-9.]+$/))
                {
                    cookieval = eval(cookieval);
                }

                if( ! cookieval)
                {
                    cookieval = this.getFromPath(prop, $rootScope.layoutOptions);
                }

                return cookieval;
            },

            getFromPath: function(path, lo)
            {
                var val = '',
                    current_path,
                    paths = path.split('.');

                angular.forEach(paths, function(path_id, i)
                {
                    var is_last = paths.length - 1 == i;

                    if( ! current_path)
                        current_path = lo[path_id];
                    else
                        current_path = current_path[path_id];

                    if(is_last)
                    {
                        val = current_path;
                    }
                });

                return val;
            },

            pathToObject: function(obj_path, the_value)
            {
                var new_obj = {},
                    curr_obj = null,
                    last_key;

                if(obj_path)
                {
                    var paths = obj_path.split('.'),
                        depth = paths.length - 1,
                        array_scls = '';

                    angular.forEach(paths, function(path_id, i)
                    {
                        var is_last = paths.length - 1 == i;

                        array_scls += '[\'' + path_id + '\']';

                        if(is_last)
                        {
                            if(typeof the_value == 'string' && ! the_value.toString().match(/^true|false|[0-9.]+$/))
                            {
                                the_value = '"' + the_value + '"';
                            }

                            eval('new_obj' + array_scls + ' = ' + the_value + ';');
                        }
                        else
                            eval('new_obj' + array_scls + ' = {};');
                    });
                }

                return new_obj;
            },

            iterateObject: function(objects, append, arr)
            {
                var dis = this;

                angular.forEach(objects, function(obj, key)
                {
                    if(typeof obj == 'object')
                    {
                        return dis.iterateObject(obj, append + key + '.', arr);
                    }
                    else
                    if(typeof obj != 'undefined')
                    {
                        arr[append + key] = obj;
                    }
                });

                // Filter Caching Objects
                angular.forEach(arr, function(value, prop)
                {
                    if( ! inArray(prop, dis.propsToCache))
                        delete arr[prop];
                });

                function inArray(needle, haystack) {
                    var length = haystack.length;
                    for(var i = 0; i < length; i++) {
                        if(haystack[i] == needle) return true;
                    }
                    return false;
                }

                return arr;
            }
        };
    });
auto.factory("Filter", function($resource) {
    return $resource("properties/:id", { id: "@id" },
        {
            'create':  { method: 'POST' },
            'index':   { method: 'GET', isArray: false },
            'show':    { method: 'GET', isArray: false },
            'update':  { method: 'PUT', isArray: false  },
            'destroy': { method: 'DELETE' }
        }
    );
});
auto.factory("FilterValue", function($resource) {
    return $resource("prop_vals/:id", { id: "@id" },
        {
            'create':  { method: 'POST' },
            'add_sub':  { method: 'POST' , url: 'prop_vals/add_sub'},
            'index':   { method: 'GET', isArray: false },
            'getSub':   { method: 'GET', isArray: false, url: 'prop_vals/get_sub'},
            'show':    { method: 'GET', isArray: false },
            'update':  { method: 'PUT', isArray: false  },
            'destroy': { method: 'DELETE' }
        }
    );
});
auto.factory("Filters", function($resource) {
    return $resource("api/:action", { action: "@action" },
        {
            'get_filters': { method: 'GET' , params: { 'action' : 'get_filters'}, isArray: true },
            'get_filter_for_new': { method: 'GET' , params: { 'action' : 'get_filters_for_new'}, isArray: false },
            'index':   { method: 'GET', isArray: true },
            'show':    { method: 'GET', isArray: false },
            'update':  { method: 'PUT', isArray: false  },
            'destroy': { method: 'DELETE' }
        }
    );
});


auto.factory("Group", function($resource) {
    return $resource("groups/:id", { id: "@id" },
        {
            'add_filter':  { method: 'GET' , url: '/add_filter' , isArray: false  },
            'remove_filter':  { method: 'GET' , url: '/remove_filter', isArray: false   },
            'create':  { method: 'POST' },
            'index':   { method: 'GET', isArray: false },
            'show':    { method: 'GET', isArray: false },
            'update':  { method: 'PUT', isArray: false  },
            'destroy': { method: 'DELETE' }
        }
    );
});
auto.factory("Lang", function($resource) {
    return $resource("langs/:id", { id: "@id" },
        {
            'create':  { method: 'POST' },
            'index':   { method: 'GET', isArray: false },
            'show':    { method: 'GET', isArray: false },
            'update':  { method: 'PUT', isArray: false  },
            'destroy': { method: 'DELETE' }
        }
    );
});
auto.factory("LangVal", function($resource) {
    return $resource("langs/:id", { id: "@id" },
        {
            'add_filter':  { method: 'POST', url: 'lang/add_filter' },
            'get_filters':   { method: 'GET', url: 'lang/get_filters/:id', isArray: false },
            'show':    { method: 'GET', isArray: false },
            'update':  { method: 'PUT', isArray: false  },
            'destroy': { method: 'DELETE' }
        }
    );
});
auto.factory("Pages", function($resource) {
    return $resource("pages/:id", { id: "@id" },
        {
            'update_lang':  { method: 'POST', url: 'pages/update_lang/:id' },
            'create':  { method: 'POST' },
            'index':   { method: 'GET', isArray: false },
            'show':    { method: 'GET', isArray: true },
            'update':  { method: 'PUT', isArray: false  },
            'destroy': { method: 'DELETE' }
        }
    );
});
auto.factory("Service", function($resource) {
    return $resource("services/:id", { id: "@id" },
        {
            'create':  { method: 'POST' },
            'add':  { method: 'POST', url:'service/add/:id'},
            'index':   { method: 'GET', isArray: false },
            'show':    { method: 'GET', isArray: false },
            'update':  { method: 'PUT', isArray: false  },
            'destroy': { method: 'DELETE' }
        }
    );
});
auto.factory("Users", function($resource) {
    return $resource("users/:id", { id: "@id" },
        {
            'create':  { method: 'POST' },
            'index':   { method: 'GET', isArray: false },
            'show':    { method: 'GET', isArray: false },
            'update':  { method: 'PUT', isArray: false  },
            'destroy': { method: 'DELETE' }
        }
    );
});

'use strict';
auto.service('AuthService', function($cookies, $http, Restangular) {
    'use strict';

    var self = this;
    this.status = {
        authorized: false,
    };

    this.loginByCredentials = function(username, password) {
        return Restangular.all('sessions').post({ email: username, password: password })
            .then(function(response) {
                return self.loginByToken(response.contents);
            });
    };

    this.loginByToken = function(token) {
        $http.defaults.headers.common['X-Token'] = token;

        return Restangular.all('sessions').get(token)
            .then(function(response) {
                $cookies.accessToken = token;
                self.status.authorized = true;
                return response;
            });
    };

    this.logout = function() {
        self.status.authorized = false;
        $cookies.accessToken = '';

        Restangular.all('sessions').remove();
    };
});



angular.module('auto.services', []).
    service('$menuItems', function()
    {
        this.menuItems = [];

        var $menuItemsRef = this;

        var menuItemObj = {
            parent: null,

            title: '',
            link: '', // starting with "./" will refer to parent link concatenation
            state: '', // will be generated from link automatically where "/" (forward slashes) are replaced with "."
            icon: '',

            isActive: false,
            label: null,

            menuItems: [],

            setLabel: function(label, color, hideWhenCollapsed)
            {
                if(typeof hideWhenCollapsed == 'undefined')
                    hideWhenCollapsed = true;

                this.label = {
                    text: label,
                    classname: color,
                    collapsedHide: hideWhenCollapsed
                };

                return this;
            },

            addItem: function(title, link, icon)
            {
                var parent = this,
                    item = angular.extend(angular.copy(menuItemObj), {
                        parent: parent,

                        title: title,
                        link: link,
                        icon: icon
                    });

                if(item.link)
                {
                    if(item.link.match(/^\./))
                        item.link = parent.link + item.link.substring(1, link.length);

                    if(item.link.match(/^-/))
                        item.link = parent.link + '-' + item.link.substring(2, link.length);

                    item.state = $menuItemsRef.toStatePath(item.link);
                }

                this.menuItems.push(item);

                return item;
            }
        };

        this.addItem = function(title, link, icon)
        {
            var item = angular.extend(angular.copy(menuItemObj), {
                title: title,
                link: link,
                state: this.toStatePath(link),
                icon: icon
            });

            this.menuItems.push(item);

            return item;
        };

        this.getAll = function()
        {
            return this.menuItems;
        };

        this.prepareSidebarMenu = function()
        {
            var dashboard    = this.addItem('Dashboard', 		'/app/dashboard', 			'linecons-cog');
            var layouts      = this.addItem('Layout & Skins',	'/app/layout-and-skins',	'linecons-desktop');
            var ui_elements  = this.addItem('UI Elements', 		'/app/ui', 					'linecons-note');
            var widgets  	 = this.addItem('Widgets', 			'/app/widgets', 			'linecons-star');
            var mailbox  	 = this.addItem('Mailbox', 			'/app/mailbox', 			'linecons-mail').setLabel('5', 'secondary', false);
            var tables  	 = this.addItem('Tables', 			'/app/tables', 				'linecons-database');
            var forms  	 	 = this.addItem('Forms', 			'/app/forms', 				'linecons-params');
            var extra  	 	 = this.addItem('Extra', 			'/app/extra', 				'linecons-beaker').setLabel('New Items', 'purple');
            var charts  	 = this.addItem('Charts', 			'/app/charts', 				'linecons-globe');
            var menu_lvls  	 = this.addItem('Menu Levels', 		'', 						'linecons-cloud');


            // Subitems of Dashboard
            dashboard.addItem('Dashboard 1', 	'-/variant-1'); // "-/" will append parents link
            dashboard.addItem('Dashboard 2', 	'-/variant-2');
            dashboard.addItem('Dashboard 3', 	'-/variant-3');
            dashboard.addItem('Dashboard 4', 	'-/variant-4');
            dashboard.addItem('Update Hightlights', '/app/update-highlights').setLabel('v1.3', 'pink');


            // Subitems of UI Elements
            ui_elements.addItem('Panels', 				'-/panels');
            ui_elements.addItem('Buttons', 				'-/buttons');
            ui_elements.addItem('Tabs & Accordions', 	'-/tabs-accordions');
            ui_elements.addItem('Modals', 				'-/modals');
            ui_elements.addItem('Breadcrumbs', 			'-/breadcrumbs');
            ui_elements.addItem('Blockquotes', 			'-/blockquotes');
            ui_elements.addItem('Progress Bars', 		'-/progress-bars');
            ui_elements.addItem('Navbars', 				'-/navbars');
            ui_elements.addItem('Alerts', 				'-/alerts');
            ui_elements.addItem('Pagination', 			'-/pagination');
            ui_elements.addItem('Typography', 			'-/typography');
            ui_elements.addItem('Other Elements', 		'-/other-elements');


            // Subitems of Mailbox
            mailbox.addItem('Inbox', 			'-/inbox');
            mailbox.addItem('Compose Message', 	'-/compose');
            mailbox.addItem('View Message', 	'-/message');


            // Subitems of Tables
            tables.addItem('Basic Tables',		'-/basic');
            tables.addItem('Responsive Tables',	'-/responsive');
            tables.addItem('Data Tables',		'-/datatables');


            // Subitems of Forms
            forms.addItem('Native Elements',		'-/native');
            forms.addItem('Advanced Plugins',		'-/advanced');
            forms.addItem('Form Wizard',			'-/wizard');
            forms.addItem('Form Validation',		'-/validation');
            forms.addItem('Input Masks',			'-/input-masks');
            forms.addItem('File Upload',			'-/file-upload');
            forms.addItem('Editors',				'-/wysiwyg');
            forms.addItem('Sliders',				'-/sliders');


            // Subitems of Extra
            var extra_icons = extra.addItem('Icons', 	'-/icons');
            var extra_maps  = extra.addItem('Maps', 	'-/maps');
            var members 	= extra.addItem('Members', 	'-/members').setLabel('New', 'warning');
            extra.addItem('Gallery', 					'-/gallery');
            extra.addItem('Calendar', 					'-/calendar');
            extra.addItem('Profile', 					'-/profile');
            extra.addItem('Login', 						'/login');
            extra.addItem('Lockscreen', 				'/lockscreen');
            extra.addItem('Login Light', 				'/login-light');
            extra.addItem('Timeline', 					'-/timeline');
            extra.addItem('Timeline Centered', 			'-/timeline-centered');
            extra.addItem('Notes', 						'-/notes');
            extra.addItem('Image Crop', 				'-/image-crop');
            extra.addItem('Portlets', 					'-/portlets');
            extra.addItem('Blank Page', 				'-/blank-page');
            extra.addItem('Search', 					'-/search');
            extra.addItem('Invoice', 					'-/invoice');
            extra.addItem('404 Page', 					'-/page-404');
            extra.addItem('Tocify', 					'-/tocify');
            extra.addItem('Loading Progress', 			'-/loading-progress');
            //extra.addItem('Page Loading Overlay', 		'-/page-loading-overlay'); NOT SUPPORTED IN ANGULAR
            extra.addItem('Notifications', 				'-/notifications');
            extra.addItem('Nestable Lists', 			'-/nestable-lists');
            extra.addItem('Scrollable', 				'-/scrollable');

            // Submenu of Extra/Icons
            extra_icons.addItem('Font Awesome', 	'-/font-awesome');
            extra_icons.addItem('Linecons', 		'-/linecons');
            extra_icons.addItem('Elusive', 			'-/elusive');
            extra_icons.addItem('Meteocons', 		'-/meteocons');

            // Submenu of Extra/Maps
            extra_maps.addItem('Google Maps', 		'-/google');
            extra_maps.addItem('Advanced Map', 		'-/advanced');
            extra_maps.addItem('Vector Map', 		'-/vector');

            // Submenu of Members
            members.addItem('Members List', '-/list');
            members.addItem('Add Member', '-/add');


            // Subitems of Charts
            charts.addItem('Chart Variants', 		'-/variants');
            charts.addItem('Range Selector', 		'-/range-selector');
            charts.addItem('Sparklines', 			'-/sparklines');
            charts.addItem('Map Charts', 			'-/map-charts');
            charts.addItem('Circular Gauges', 		'-/gauges');
            charts.addItem('Bar Gauges', 			'-/bar-gauges');



            // Subitems of Menu Levels
            var menu_lvl1 = menu_lvls.addItem('Menu Item 1.1');  // has to be referenced to add sub menu elements
            menu_lvls.addItem('Menu Item 1.2');
            menu_lvls.addItem('Menu Item 1.3');

            // Sub Level 2
            menu_lvl1.addItem('Menu Item 2.1');
            var menu_lvl2 = menu_lvl1.addItem('Menu Item 2.2'); // has to be referenced to add sub menu elements
            menu_lvl1.addItem('Menu Item 2.3');

            // Sub Level 3
            menu_lvl2.addItem('Menu Item 3.1');
            menu_lvl2.addItem('Menu Item 3.2');


            return this;
        };

        this.prepareHorizontalMenu = function()
        {
            var dashboard    = this.addItem('Dashboard', 		'/app/dashboard', 			'linecons-cog');
            var layouts      = this.addItem('Layout',			'/app/layout-and-skins',	'linecons-desktop');
            var ui_elements  = this.addItem('UI Elements', 		'/app/ui', 					'linecons-note');
            var forms  	 	 = this.addItem('Forms', 			'/app/forms', 				'linecons-params');
            var other  	 	 = this.addItem('Other', 			'/app/extra', 				'linecons-beaker');


            // Subitems of Dashboard
            dashboard.addItem('Dashboard 1', 	'-/variant-1'); // "-/" will append parents link
            dashboard.addItem('Dashboard 2', 	'-/variant-2');
            dashboard.addItem('Dashboard 3', 	'-/variant-3');
            dashboard.addItem('Dashboard 4', 	'-/variant-4');


            // Subitems of UI Elements
            ui_elements.addItem('Panels', 				'-/panels');
            ui_elements.addItem('Buttons', 				'-/buttons');
            ui_elements.addItem('Tabs & Accordions', 	'-/tabs-accordions');
            ui_elements.addItem('Modals', 				'-/modals');
            ui_elements.addItem('Breadcrumbs', 			'-/breadcrumbs');
            ui_elements.addItem('Blockquotes', 			'-/blockquotes');
            ui_elements.addItem('Progress Bars', 		'-/progress-bars');
            ui_elements.addItem('Navbars', 				'-/navbars');
            ui_elements.addItem('Alerts', 				'-/alerts');
            ui_elements.addItem('Pagination', 			'-/pagination');
            ui_elements.addItem('Typography', 			'-/typography');
            ui_elements.addItem('Other Elements', 		'-/other-elements');


            // Subitems of Forms
            forms.addItem('Native Elements',		'-/native');
            forms.addItem('Advanced Plugins',		'-/advanced');
            forms.addItem('Form Wizard',			'-/wizard');
            forms.addItem('Form Validation',		'-/validation');
            forms.addItem('Input Masks',			'-/input-masks');
            forms.addItem('File Upload',			'-/file-upload');
            forms.addItem('Editors',				'-/wysiwyg');
            forms.addItem('Sliders',				'-/sliders');


            // Subitems of Others
            var widgets     = other.addItem('Widgets', 			'/app/widgets', 			'linecons-star');
            var mailbox     = other.addItem('Mailbox', 			'/app/mailbox', 			'linecons-mail').setLabel('5', 'secondary', false);
            var tables      = other.addItem('Tables', 			'/app/tables', 				'linecons-database');
            var extra       = other.addItem('Extra', 			'/app/extra', 				'linecons-beaker').setLabel('New Items', 'purple');
            var charts      = other.addItem('Charts', 			'/app/charts', 				'linecons-globe');
            var menu_lvls   = other.addItem('Menu Levels', 		'', 						'linecons-cloud');


            // Subitems of Mailbox
            mailbox.addItem('Inbox', 			'-/inbox');
            mailbox.addItem('Compose Message', 	'-/compose');
            mailbox.addItem('View Message', 	'-/message');


            // Subitems of Tables
            tables.addItem('Basic Tables',		'-/basic');
            tables.addItem('Responsive Tables',	'-/responsive');
            tables.addItem('Data Tables',		'-/datatables');


            // Subitems of Extra
            var extra_icons = extra.addItem('Icons', 	'-/icons').setLabel(4, 'warning');
            var extra_maps  = extra.addItem('Maps', 	'-/maps');
            extra.addItem('Gallery', 					'-/gallery');
            extra.addItem('Calendar', 					'-/calendar');
            extra.addItem('Profile', 					'-/profile');
            extra.addItem('Login', 						'/login');
            extra.addItem('Lockscreen', 				'/lockscreen');
            extra.addItem('Login Light', 				'/login-light');
            extra.addItem('Timeline', 					'-/timeline');
            extra.addItem('Timeline Centered', 			'-/timeline-centered');
            extra.addItem('Notes', 						'-/notes');
            extra.addItem('Image Crop', 				'-/image-crop');
            extra.addItem('Portlets', 					'-/portlets');
            extra.addItem('Blank Page', 				'-/blank-page');
            extra.addItem('Search', 					'-/search');
            extra.addItem('Invoice', 					'-/invoice');
            extra.addItem('404 Page', 					'-/page-404');
            extra.addItem('Tocify', 					'-/tocify');
            extra.addItem('Loading Progress', 			'-/loading-progress');
            //extra.addItem('Page Loading Overlay', 		'-/page-loading-overlay'); NOT SUPPORTED IN ANGULAR
            extra.addItem('Notifications', 				'-/notifications');
            extra.addItem('Nestable Lists', 			'-/nestable-lists');
            extra.addItem('Scrollable', 				'-/scrollable');

            // Submenu of Extra/Icons
            extra_icons.addItem('Font Awesome', 	'-/font-awesome');
            extra_icons.addItem('Linecons', 		'-/linecons');
            extra_icons.addItem('Elusive', 			'-/elusive');
            extra_icons.addItem('Meteocons', 		'-/meteocons');

            // Submenu of Extra/Maps
            extra_maps.addItem('Google Maps', 		'-/google');
            extra_maps.addItem('Advanced Map', 		'-/advanced');
            extra_maps.addItem('Vector Map', 		'-/vector');


            // Subitems of Charts
            charts.addItem('Chart Variants', 		'-/variants');
            charts.addItem('Range Selector', 		'-/range-selector');
            charts.addItem('Sparklines', 			'-/sparklines');
            charts.addItem('Map Charts', 			'-/map-charts');
            charts.addItem('Circular Gauges', 		'-/gauges');
            charts.addItem('Bar Gauges', 			'-/bar-gauges');



            // Subitems of Menu Levels
            var menu_lvl1 = menu_lvls.addItem('Menu Item 1.1');  // has to be referenced to add sub menu elements
            menu_lvls.addItem('Menu Item 1.2');
            menu_lvls.addItem('Menu Item 1.3');

            // Sub Level 2
            menu_lvl1.addItem('Menu Item 2.1');
            var menu_lvl2 = menu_lvl1.addItem('Menu Item 2.2'); // has to be referenced to add sub menu elements
            menu_lvl1.addItem('Menu Item 2.3');

            // Sub Level 3
            menu_lvl2.addItem('Menu Item 3.1');
            menu_lvl2.addItem('Menu Item 3.2');

            return this;
        }

        this.instantiate = function()
        {
            return angular.copy( this );
        }

        this.toStatePath = function(path)
        {
            return path.replace(/\//g, '.').replace(/^\./, '');
        };

        this.setActive = function(path)
        {
            this.iterateCheck(this.menuItems, this.toStatePath(path));
        };

        this.setActiveParent = function(item)
        {
            item.isActive = true;
            item.isOpen = true;

            if(item.parent)
                this.setActiveParent(item.parent);
        };

        this.iterateCheck = function(menuItems, currentState)
        {
            angular.forEach(menuItems, function(item)
            {
                if(item.state == currentState)
                {
                    item.isActive = true;

                    if(item.parent != null)
                        $menuItemsRef.setActiveParent(item.parent);
                }
                else
                {
                    item.isActive = false;
                    item.isOpen = false;

                    if(item.menuItems.length)
                    {
                        $menuItemsRef.iterateCheck(item.menuItems, currentState);
                    }
                }
            });
        }
    });