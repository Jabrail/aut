'use strict';

var auto = angular.module('auto_client', [
    'ngRoute',
    'uiGmapgoogle-maps',
    'xenon.directives',
    'ngResource',
    'slickCarousel'
])

var view_dir = "/assets/client_app/view/"

auto.config(function($httpProvider) {
    $httpProvider.defaults.headers.common['X-CSRF-Token'] =
        $('meta[name=csrf-token]').attr('content');
});

auto.config(['$routeProvider', '$locationProvider',
    function($routeProvider ,$locationProvider ) {
        $routeProvider.
            when('/', {
                templateUrl: "/assets/client_app/view/home/index.html",
                controller: 'HomeCtrl'
            }).
            when('/search', {
                templateUrl: "/assets/client_app/view/search/index.html",
                controller: 'SearchCtrl'
            }).
            when('/search/mark/:id', {
                templateUrl: "/assets/client_app/view/search/index.html",
                controller: 'SearchCtrl'
            }).
            when('/search/:id', {
                templateUrl: "/assets/client_app/view/search/detail.html",
                controller: 'DetailCtrl'
            }).
            when('/newPoster', {
                templateUrl: "/assets/client_app/view/authpages/newPoster/newPoster.html",
                controller: 'NewAdvertCtrl'
            }).
            when('/profileMain', {
                templateUrl: "/assets/client_app/view/authpages/profileMain/index.html",
                controller: 'ProfileMain'
            }).
            when('/profileService', {
                templateUrl: "/assets/client_app/view/authpages/profileMain/service.html",
                controller: 'ProfileService'
            }).
            when('/profileSettings', {
                templateUrl: "/assets/client_app/view/authpages/profileMain/settings.html",
                controller: 'ProfileSettings'
            }).
            when('/page/:name', {
                templateUrl: "/assets/client_app/view/staticPages/index.html",
                controller: 'StaticPagesCtrl'
            }).
            otherwise({
                redirectTo: '/'
            });
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });


    }
]);

auto.controller('MenuCtrl',['$scope', '$location'
    , function ($scope, $location ) {

        $scope.getClass = function (path) {
            if ($location.path().substr(0, path.length) === path) {
                return 'active';
            } else {
                return '';
            }
        }

    }
    ]);

auto.config(function(uiGmapGoogleMapApiProvider) {
    uiGmapGoogleMapApiProvider.configure({
        //    key: 'your api key',
        v: '3.20', //defaults to latest 3.X anyhow
        libraries: 'places' // Required for SearchBox.
    });
})
auto.run(['$templateCache', function ($templateCache) {
    $templateCache.put('searchbox.tpl.html', '<input id="pac-input" class="pac-controls" type="text" placeholder="Search">');
    $templateCache.put('window.tpl.html', '<div ng-controller="WindowCtrl" ng-init="showPlaceDetails(parameter)">{{place.name}}</div>');
}])

function getUser(user, scope, callback) {

}
"use strict";

auto.controller('DetailCtrl',
    function ($scope, $http, $routeParams, Advert , $interval) {

        var firstLoad = true;
        $scope.showGallery = false;

        $scope.bigImage = false;

        $scope.showBigImage = function(i) {
            $scope.bigImage  = true;

            if (firstLoad) {
                firstLoad = false;
                $scope.numberLoadedNav = true;
                $scope.numberLoadedBig = true;

                $interval(function() {

                    $scope.slickConfigBig = {
                        initialSlide: i,
                        draggable: false,
                        slidesToShow: 1,
                        infinite: true,
                        dots: false,
                        centerMode: true,
                        variableWidth:true,
                        adaptiveHeight:true,
                        asNavFor: '.slider-nav',
                        nextArrow: '<button type="button" data-role="none" class="slick-next navigator" aria-label="next"><span class="icon-arrow_right"></span></button>   ',
                        prevArrow: '<button type="button" data-role="none" class="slick-prev navigator" style="z-index: 1000;" aria-label="previous"><span class="icon-arrow_left"></span></button>'
                    };

                    $scope.slickConfigNav = {
                        slidesToShow: 9,
                        slidesToScroll: 4,
                        infinite: false,
                        asNavFor: '.slider-for',
                        dots: false,
                        arrows: false,
                        centerMode: true,
                        centerPadding: '20px',
                        focusOnSelect: true,
                        variableWidth:true
                    };
                }, 400);
            }

        }
        $scope.hideBigImage = function() {
            $scope.bigImage  = false;
            $scope.numberLoadedNav = false;
            $scope.numberLoadedBig = false;
        }

        Advert.get_poster({id: $routeParams.id}).$promise.then(function(data){

            var stop;
            $scope.advert = data;

            $scope.numberLoaded = true;

            stop = $interval(function() {



                $scope.slickConfig = {
                    draggable: false,
                    slidesToShow: 1,
                    infinite: true,
                    dots: true,
                    centerMode: true,
                    variableWidth:true,
                    adaptiveHeight:true,
                    nextArrow: '<button type="button" data-role="none" class="slick-next navigator" aria-label="next"><span class="icon-arrow_right"></span></button>   ',
                    prevArrow: '<button type="button" data-role="none" class="slick-prev navigator" style="z-index: 1000;" aria-label="previous"><span class="icon-arrow_left"></span></button>',
                    responsive: [
                        {
                            breakpoint:1200,
                            settings: {
                                arrows:false,
                                centerMode:false,
                                variableWidth:false,
                                slidesToShow:1,
                                dots:true,
                                adaptiveHeight:true,
                                draggable:true
                            }
                        }
                    ],

                    method: {}
                };


            }, 400);
        });
    });


"use strict";

auto.controller('HomeCtrl',
    function ($scope, $http, Advert , Filter, $rootScope, $location, User) {

        var filtersArray = [];


        Filter.get_filters().$promise.then(function(data) {


            $scope.filter = data.filters;
            $scope.filter.mark.default = 'Выберите марку';
            $scope.filter.country.default = 'Выберите страну';
            $scope.filter.mark.title_sub = 'Выберите модель';
            $scope.filter.country.title_sub = 'Выберите город';
            $scope.filter.year.selected = '0';
            $scope.filter.mileage.selected = '0';
            $scope.filter.state.selected = '0';
            $scope.filter.additional.selected = [];

            sendFilters();

        });


        $scope._change = function(filter) {

            filter.selected = filter.__selected.id;

            sendFilters();

        }

        $scope.change = function(filter) {

            filter.selected_sub = filter._selected;
            filter.selected = filter.selected_sub? filter.selected_sub.id : '0';
            filter.__selected = '0';

            sendFilters();

        };

        $scope.setSelect = function(filter) {
            filter.selected = filter._selected.id;
            sendFilters();

        }

        $scope.setState = function(filter, val) {

            var elem;

            if (filter.selected && filter.selected != '0') {
                elem = filter.selected.filter(function (item) {
                    return item == val.id
                })[0]

                if (elem) {
                    filter.selected.splice(filter.selected.indexOf(val.id), 1)
                } else {
                    filter.selected.push(val.id);
                }
            } else {
                filter.selected = [];
                filter.selected.push(val.id);

            }

            sendFilters();

        };

        $scope.openSearch = function () {

            $rootScope.filters = $scope.filter;
            console.log($scope.filter)
            console.log($rootScope.filters)
            $location.path('/search')

        }


        function sendFilters() {

            var filtersArray = [];


            for (var fl in $scope.filter) {

                if ($scope.filter[fl].selected) {
                    if ($scope.filter[fl].selected != "0") {
                        if ($scope.filter[fl].type_value == 10) {
                            if ($scope.filter[fl].__selected != '0') {
                                filtersArray.push( { id: $scope.filter[fl].id , value: $scope.filter[fl].selected_sub.id,  value_sub: Number($scope.filter[fl].selected)});
                            } else {
                                filtersArray.push( { id: $scope.filter[fl].id , value: $scope.filter[fl].selected});
                            }
                        } else {
                            filtersArray.push( { id: $scope.filter[fl].id , value: $scope.filter[fl].selected});
                        }
                    } else if ($scope.filter[fl].selected_sub){
                        filtersArray.push( { id: $scope.filter[fl].id , value: Number($scope.filter[fl].selected_sub.id)});
                    }
                }

            }

            Advert.get_by_prop({pops: filtersArray}).$promise.then(function(data){
                $scope.adverts_by_search = data
            })
        }

        Advert.get_last({id: 10}).$promise.then(function (data) {

            $scope.adverts = data;


            $scope.slickConfig = {


                infinite: false,

                speed: 300,

                slidesToShow: 1
            };
            $scope.responsiveConfig = {
                dots: true,

                infinite: false,

                speed: 300,

                slidesToShow: 4,

                slidesToScroll: 4,

                responsive: [

                    {

                        breakpoint: 1024,

                        settings: {

                            slidesToShow: 3,

                            slidesToScroll: 3,

                            infinite: true,

                            dots: true

                        }

                    },

                    {

                        breakpoint: 640,

                        settings: {

                            slidesToShow: 2,

                            slidesToScroll: 2

                        }

                    },

                    {

                        breakpoint: 480,

                        settings: {

                            slidesToShow: 1,

                            slidesToScroll: 1

                        }

                    }

                ]

            }


            $scope.numberLoaded = true;


        });

        function findById(arr, id) {

            var res_item = false;
            arr.forEach(function(item) {

                if ( item.id == id ) {
                    res_item = item
                }

            });

            return res_item
        }

        function removeById(arr, id) {

            arr.forEach(function(item) {

                if ( item.id == id ) {
                    arr.splice( arr.indexOf(item) , 1)
                }

            });

        }

        function sendRequest() {

            var vals = [];
            filtersArray.forEach(function(item) {
                vals.push(item.val)
            })
            Advert.get_by_prop({pops_id: vals}).$promise.then(function(data){
                $scope.adverts_by_search = data
            })
        }


    });
"use strict";

auto.controller('SearchCtrl',['$scope', '$http', 'uiGmapLogger',  'uiGmapGoogleMapApi', 'Filter', 'Advert', '$rootScope', '$routeParams'
    , function ($scope, $http , $log,  GoogleMapApi, Filter, Advert, $rootScope, $routeParams) {




        $scope.orders = [
            { value: '0' , title: 'Упорядочить по:'},
            { value: '1' , title: 'Цене'},
            { value: '2' , title: 'Дате добавления'}
        ];
        $scope.order_desc = $scope.orders[0];

        $scope._change = function(filter) {

            filter.selected = filter.__selected.id;

            sendFilters();

        }

        Advert.count().$promise.then(function(data) {
            $scope.advert_count = data.count
        })

        $scope.change = function(filter) {

            filter.selected_sub = filter._selected;
            filter.selected = filter.selected_sub? filter.selected_sub.id : '0';
            filter.__selected = '0';

            sendFilters();

        };

        Filter.get_filters().$promise.then(function (data) {
            var jsonYear = ''
            var jsonMileage = ''

            $scope.filter = data.filters;
            $scope.filter.year.selected = '0'
            $scope.filter.state.selected = '0'
            $scope.filter.additional.selected = [];
            $scope.filter.mark.default = 'Выберите марку';
            $scope.filter.country.default = 'Выберите страну';
            $scope.filter.mark.title_sub = 'Выберите модель';
            $scope.filter.country.title_sub = 'Выберите город';

            jsonYear = JSON.stringify(data.filters.year);
            jsonMileage = JSON.stringify(data.filters.mileage);
            $scope.year = {
                before: JSON.parse(jsonYear),
                after: JSON.parse(jsonYear)
            };
            $scope.mileage = {
                before: JSON.parse(jsonMileage),
                after: JSON.parse(jsonMileage)
            };

            $scope.year.before.title = "Год от:";
            $scope.year.after.title = "Год до:";
            $scope.mileage.before.title = "Пробег от:";
            $scope.mileage.after.title = "Пробег до:";

            if ($rootScope.filters) {
                $scope.filter = $rootScope.filters;
            }


            if ($routeParams.id) {

                $scope.filter.mark._selected = $scope.filter.mark.prop_vals.filter(function (item) {

                    return item.id == $routeParams.id
                })[0];
                $scope.change($scope.filter.mark)
            }

            $scope.filter_loaded = true;
            sendFilters()
        });



        $scope.setSelect = function(filter) {
            filter.selected = filter._selected.id.toString();
            sendFilters();

        }

        $scope.setState = function(filter, val) {

            var elem;

            if (filter.selected && filter.selected != '0') {
                elem = filter.selected.filter(function (item) {
                    return item == val.id
                })[0]

                if (elem) {
                    filter.selected.splice(filter.selected.indexOf(val.id), 1)
                } else {
                    filter.selected.push(val.id);
                }
            } else {
                filter.selected = [];
                filter.selected.push(val.id);

            }

            sendFilters();

        }

        $scope.changrDesc = function(desc) {
            console.log($scope.order_desc)
            sendFilters()
        }



        function sendFilters() {

            var filtersArray = [];

            for (var fl in $scope.filter) {

                if ($scope.filter[fl].selected) {
                    if ($scope.filter[fl].selected != "0") {
                        if ($scope.filter[fl].type_value == 10) {
                            if ($scope.filter[fl].__selected != '0') {
                                filtersArray.push( { id: $scope.filter[fl].id , value: $scope.filter[fl].selected_sub.id,  value_sub: Number($scope.filter[fl].selected)});
                            } else {
                                filtersArray.push( { id: $scope.filter[fl].id , value: $scope.filter[fl].selected});
                            }
                        } else {
                            filtersArray.push( { id: $scope.filter[fl].id , value: $scope.filter[fl].selected});
                        }
                    } else if ($scope.filter[fl].selected_sub){
                        filtersArray.push( { id: $scope.filter[fl].id , value: Number($scope.filter[fl].selected_sub.id)});
                    }
                }

            }


            if ($scope.year.before.selected != '0' || $scope.year.after.selected != '0') {
                var before = ($scope.year.before.selected  != '0') ? $scope.year.before.selected : '10000000000000000'
                var after = ($scope.year.after.selected  != '0') ? $scope.year.after.selected : '0'
                filtersArray.push( { id: $scope.filter.year.id , value_befor: before, value_after: after});

            }

            if ($scope.mileage.before.selected != '0' || $scope.mileage.after.selected !='0') {
                var before = ($scope.mileage.before.selected != '0') ? $scope.mileage.before.selected : $scope.mileage.before.prop_vals[0].id;
                var after = ($scope.mileage.after.selected != '0') ? $scope.mileage.after.selected : $scope.mileage.before.prop_vals[$scope.mileage.before.prop_vals.length-1].id;

                filtersArray.push( { id: $scope.filter.mileage.id , value_befor: before, value_after: after});
            }


            Advert.get_by_prop({pops: filtersArray, desc: $scope.order_desc.value}).$promise.then(function(data){
                $scope.adverts = data
            })
        }

    }]);
auto.factory("Advert", function($resource) {
    return $resource("/posters/:id", { id: "@id" },
        {
            'create':  { method: 'POST' },
            'get_user_poster':{ method: 'GET', isArray: true, url: "/api/get_user_poster" },
            'get_by_prop':{ method: 'POST', isArray: true, url: "/api/get_by_prop" },
            'get_poster':{ method: 'GET', isArray: false, url: "/api/get_poster/:id" },
            'index':   { method: 'GET', isArray: true },
            'show':    { method: 'GET', isArray: false },
            'update':  { method: 'PUT', isArray: false  },
            'destroy': { method: 'DELETE' , isArray: false },
            'get_last' : {method: 'GET', isArray: true, url: "/api/last_poster/:id"},
            'get_marks' : {method: 'GET', isArray: true, url: "/api/get_marks/"},
            'count' : {method: 'GET', isArray: false, url: "/api/count_poster/"}

        }
    );
});
auto.factory("Filter", function($resource) {
    return $resource("/api/:action", { action: "@action" },
        {
            'get_filters': { method: 'GET' , params: { 'action' : 'get_filters'}, isArray: false },
            'get_filter_for_new': { method: 'GET' , params: { 'action' : 'get_filters_for_new'}, isArray: false },

            'index':   { method: 'GET', isArray: true },
            'show':    { method: 'GET', isArray: false },
            'update':  { method: 'PUT', isArray: false  },
            'destroy': { method: 'DELETE' }
        }
    );
});


auto.factory("Pages", function($resource) {
	return $resource("/api/get_pages/:name", { name: "@name" },
		{
			'get_pages':  { method: 'GET' , isArray: false },
			'get_page':  { method: 'GET' , isArray: false }
		}
	);
});
auto.factory("Services", function($resource) {
	return $resource("/services/:id", { id: "@id" },
		{
			'create':  { method: 'POST' },
			'add':  { method: 'POST', url:'/service/add/:id'},
			'index':   { method: 'GET', isArray: false },
			'show':    { method: 'GET', isArray: false },
			'update':  { method: 'PUT', isArray: false  },
			'destroy': { method: 'DELETE' }
		}
	);
});
auto.factory("User", function($resource) {
	return $resource("/api/get_user", { name: "@name" },
		{
			'get_user':   { method: 'GET', isArray: false },
			'login':   { method: 'POST', isArray: false, url: '/users/sign_in' },
			'logout':   { method: 'DELETE', isArray: false, url: '/users/sign_out' },
			'registration':   { method: 'POST', isArray: false, url: '/users' }
		}
	);
});
auto.directive('auth', [   'User' , '$http' , '$location' , function(User, $http, $location){
	return {
		restrict: 'E',
		templateUrl: appHelper.templatePath('admTpls/auth'),
		link: function(scope, el) {
			$(el).hide();

			scope.user = {};

			scope.hideLogin = function() {
				scope.showAuth = false
			}
			scope.$watch(
					"showAuth",
					function handleFooChange( newValue, oldValue ) {
						scope.pageLoaded = false;
						if (scope.showAuth) {
							$(el).show()

							$http({
								method: 'GET',
								url: '/users/sign_in'
							}).then(function(data) {

								$(el).find('.login_box').empty();
								$(el).find('.login_box').append(data.data);

								$http({
									method: 'GET',
									url: '/users/sign_up'
								}).then(function(data) {
									scope.pageLoaded = true;
									$(el).find('.registration_box').empty();
									$(el).find('.registration_box').append(data.data);
								}, function(response) {
									// called asynchronously if an error occurs
									// or server returns response with an error status.
								});

							}, function(response) {
								// called asynchronously if an error occurs
								// or server returns response with an error status.
							});


						} else {
							$(el).hide(500)
						}
					}
			);

			scope.auth = function(form) {

				User.login({'user[email]' : scope.user.email, 'user[password]' : scope.user.password}).

				$promise.then(function(data) {

					User.get_user().$promise.then(function(data) {
						scope.login = data.status
						if (scope.login) {
							$(el).hide();
							$location.path('/profileMain')
						}
					});

				})

			}
			scope.registration = function() {

				User.registration({'user[email]' : scope.user.email, 'user[password]' : scope.user.password,
					'user[password_confirmation]' : scope.user.password_confirmation}).
				$promise.then(function(data) {

					User.get_user().$promise.then(function(data) {
						scope.login = data.status
						if (scope.login) $(el).hide()
					});

				})

			}
		}

	};
}])
auto.directive('footer', [ 'Advert' , '$location' , function(Advert, $location){
	return {
		restrict: 'E',
		templateUrl: appHelper.templatePath('admTpls/footer'),
		link: function(scope, el) {

			scope.marks = Advert.get_marks();

			scope.opentSearch = function(mark) {

				$location.path('/search/mark/'+mark.id)

			}
		}

	};
}])

auto.directive('header', [ 'Pages' , '$location' , 'User',  function(Pages, $location, User){
	return {
		restrict: 'E',
		templateUrl: appHelper.templatePath('admTpls/header'),
		link: function(scope, el) {

			scope.login = false;
			scope.showAuth = false;

			Pages.get_pages().$promise.then(function (data) {
				scope.pages = data.pages;
			});

			User.get_user().$promise.then(function(data) {
				scope.login = data.status
			});

			scope.goToPage = function(href) {
				$location.path('/page/'+href);
			};
			scope.goToPath = function(href) {
				$location.path(href);
			};
			scope.showRegistration = function() {
				scope.showAuth = true;
				scope.showReg = true;
			};
			scope.showLogin = function() {
				scope.showAuth = true
				scope.showReg = false
			}
			scope.logout = function() {
				User.logout().$promise.then(function(){
						scope.login = false
						$location.path('/')
				})
			}

			scope.currentPath = function(path) {

				if ($location.path() == path) {
					return true
				} else {
					return false
				}
			}

		}

	};
}])
auto.directive('userMenu', [ '$location' , function( $location){
	return {
		restrict: 'E',
		templateUrl: appHelper.templatePath('admTpls/user_menu'),
		link: function(scope, el) {

			scope.paths = [
				{href: '/profileMain', title: "Мои объявления" },
				{href: '/profileService', title: "Услуги" },
				{href: '/profileSettings', title: "Настройки" }
			]

			scope.currentActive = function(path) {
				if (path === $location.path()) return true
			}
			scope.opentPath = function(path) {

				$location.path(path)

			}
		}

	};
}])

'use strict';
auto.service('AuthService', function($cookies, $http, Restangular) {
    'use strict';

    var self = this;
    this.status = {
        authorized: false,
    };

    this.loginByCredentials = function(username, password) {
        return Restangular.all('sessions').post({ email: username, password: password })
            .then(function(response) {
                return self.loginByToken(response.contents);
            });
    };

    this.loginByToken = function(token) {
        $http.defaults.headers.common['X-Token'] = token;

        return Restangular.all('sessions').get(token)
            .then(function(response) {
                $cookies.accessToken = token;
                self.status.authorized = true;
                return response;
            });
    };

    this.logout = function() {
        self.status.authorized = false;
        $cookies.accessToken = '';

        Restangular.all('sessions').remove();
    };
});



'use strict';
auto.service('CurrencyService', function() {
	'use strict';

	 return
});


"use strict";

auto.controller('StaticPagesCtrl',
	function ($scope, Pages, $routeParams, $sce) {

		Pages.get_page({name: $routeParams.name}).$promise.then(function(data) {

			$scope.page = data.page
			$scope.page.text = $sce.trustAsHtml($scope.page.text);
		})

	});
"use strict";

auto.controller('NewAdvertCtrl',
    function ($scope, $http , Filter, Advert , $location) {
        var images = [];
        var prop_vals = [];

        $scope.user = {
            email: '',
            pass: '',
            passConf: ''
        };


        var i = 1,

            $example_dropzone_filetable = $("#example-dropzone-filetable"),
            example_dropzone = $(".advancedDropzone").dropzone({
                url: '/images',
                method: 'POST',

                // Events
                addedfile: function(file)
                {
                    if(i == 1)
                    {
                        $example_dropzone_filetable.find('tbody').html('');
                    }

                    var size = parseInt(file.size/1024, 10);
                    size = size < 1024 ? (size + " KB") : (parseInt(size/1024, 10) + " MB");

                    var $entry = $('<tr>\
										<td class="text-center">'+(i++)+'</td>\
										<td>'+file.name+'</td>\
										<td style="width: 100px"><div class="progress progress-striped"><div class="progress-bar progress-bar-warning"></div></div></td>\
										<td  style="width: 100px">'+size+'</td>\
										<td class="actions"> </td>\
										<td style="width: 100px">Uploading...</td>\
									</tr>');


                    $example_dropzone_filetable.find('tbody').append($entry);
                    file.fileEntryTd = $entry;
                    file.progressBar = $entry.find('.progress-bar');
                    file.actions = $entry.find('.actions');
                },

                uploadprogress: function(file, progress, bytesSent)
                {
                    file.progressBar.width(progress + '%');
                },

                success: function(file, response)
                {
                    images.push(response.id);
                    var items = $('.droppable-area_poster_item');
                    for (var k = 0; k < items.length ; k++) {

                        if ($(items[k]).find('img').length == 0) {
                            $(items[k]).append('<img src="'+response.src+'"/>');
                            return true
                        }

                    }

                    /*            file.fileEntryTd.find('td:last').html('<span class="text-success">Uploaded</span>');
                     file.actions.html('<button type="button" class="btn btn-danger btn-sm"   ng-click="remove_image(response.id)">Remove</button>');
                     file.progressBar.removeClass('progress-bar-warning').addClass('progress-bar-success');
                     $scope.$apply();*/
                },

                error: function(file)
                {
                    file.fileEntryTd.find('td:last').html('<span class="text-danger">Failed</span>');
                    file.progressBar.removeClass('progress-bar-warning').addClass('progress-bar-red');
                }
            });



        Filter.get_filter_for_new().$promise.then(function(data) {

            $scope.filter = data.filters;
            $scope.filter.year.selected = '0'
            $scope.filter.state.selected = '0'
            $scope.filter.additional.selected = [];
            $scope.filter.mark.default = 'Выберите марку';
            $scope.filter.country.default = 'Выберите страну';
            $scope.filter.mark.title_sub = 'Выберите модель';
            $scope.filter.country.title_sub = 'Выберите город';

        })


        $scope._change = function(filter) {
            filter.valid = false
            findById(prop_vals, filter.id).value.sub = filter.__selected.id;
        }

        $scope.setInput = function(filter) {

            filter.valid =false;

            var val = findById(prop_vals, filter.id)

            if (val) {
                val.value = filter.selected;
            } else {
                val = { id: filter.id,
                    type: filter.type_value,
                    value:  filter.selected
                }
                prop_vals.push(val)
            }
        }

        $scope.change = function(filter) {


            filter.__selected = '0';

            filter.valid = true
            filter.selected_sub = false ;
            filter.selected_sub = filter._selected;
            filter.selected = filter.selected_sub.id;
            filter.__selected = '0';

            removeById(prop_vals, filter.id);
            prop_vals.push({ id: filter.id,
                type: filter.type_value,
                value: {
                    base: filter.selected,
                    sub: 0
                }
            });
        }

        $scope.setValue = function(value) {


        }

        $scope.set_model = function () {
        }

        $scope.setRadio = function(filter, val) {
            filter.selected = val.id
        };

        $scope.setSelect = function(filter) {

            filter.valid = false

            var val = findById(prop_vals, filter.id);

            if (val) {
                val.value =  filter._selected.id
            } else {
                val = { id: filter.id,
                    type: filter.type_value,
                    value:   filter._selected.id
                };
                prop_vals.push(val);
            }
        };

        $scope.setCheckboxSingl = function(filter) {

            var val = findById(prop_vals, filter.id);
            if (val) {
                prop_vals.splice( prop_vals.indexOf(val) , 1)
            } else {
                val = { id: filter.id,
                    type: filter.type_value,
                    value: filter.prop_vals[0].id

                };
                prop_vals.push(val);
            }

        };

        $scope.setCheckbox = function(filter, val) {

            if (filter.selected.indexOf(val.id) > -1) {

                filter.selected.splice(filter.selected.indexOf(val.id), 1);

            }
            else {
                filter.selected.push(val.id)
            }

            var val = findById(prop_vals, filter.id);

            if (val) {
                val.value = filter.selected
            } else {
                val = { id: filter.id,
                    type: filter.type_value,
                    value: filter.selected
                };
                prop_vals.push(val);
            }

        };

        $scope.create = function(type) {

            var valid = true;
            for (var fl in $scope.filter) {

                if ($scope.filter[fl].valid_data == 'true') {
                    if (!findById(prop_vals, $scope.filter[fl].id)) {

                        $scope.filter[fl].valid = true;
                        valid = false
                    }
                }
            }

            if ($scope.advert.desc == '' || !$scope.advert.desc) {
                $scope.descValid = true;
                valid = false
            }
            if ($scope.advert.price == '' || !$scope.advert.price) {
                $scope.priceValid = true;
                valid = false
            }

            console.log(valid)
            if (valid) {
                if (type == 'top') {console.log(prop_vals)
                    Advert.create({
                        poster: {description: $scope.advert.desc, price: $scope.advert.price},
                        set_top: 1,
                        currency: $scope.advert.currency,
                        filters: prop_vals
                    }).$promise.then(function () {
                        $location.path('/adverts/')
                    })
                } else {
                    console.log(prop_vals)
                    Advert.create({
                        poster: {description: $scope.advert.desc, price: $scope.advert.price},
                        set_top: 0,
                        images: images,
                        currency: $scope.advert.currency,
                        filters: prop_vals
                    }).$promise.then(function () {
                        $location.path('/adverts/')
                    })

                }
            }

        }

        $scope.remove_image = function (id) {

        }


        function findById(arr, id) {

            var res_item = false;
            arr.forEach(function(item) {

                if ( item.id == id ) {
                    res_item = item
                }

            });

            return res_item
        }

        function removeById(arr, id) {

            arr.forEach(function(item) {

                if ( item.id == id ) {
                    arr.splice( arr.indexOf(item) , 1)
                }

            });

        }

    }
);


"use strict";

auto.controller('ProfileMain',
		function ($scope, User, Advert, $location) {

			$scope.showContent = false;

			$scope.showContent = false;

			User.get_user().$promise.then(function(data){

				if (data.status)  {

					Advert.get_user_poster().$promise.then(function(data){

						$scope.showContent = true;

						$scope.adverts = data;

					})

				} else {
					window.location = '/users/sign_in'
				}

			}, function(err) {

				window.location = '/users/sign_in'

			})


			$scope.remove_advert = function(advert) {

				Advert.destroy({id: advert.id}).$promise.then(function() {


					Advert.get_user_poster().$promise.then(function(data){

						$scope.adverts = data;

					})

				})

			}

			$scope.getAdvert = function(advert) {

				$location.path('/search/'+advert.id)

			}


		}
);


"use strict";

auto.controller('ProfileService',
	function ($scope, User, Advert, $location, Services) {

		$scope.showContent = false;




		User.get_user().$promise.then(function(data){

			if (data.status)  {

				Services.index().$promise.then(function(data) {

					$scope.my_services = data.services;
					$scope.showContent = true;


				})

			} else {
				window.location = '/users/sign_in'
			}

		}, function(err) {

			window.location = '/users/sign_in'

		})


		$scope.remove_advert = function(advert) {

			Advert.destroy({id: advert.id}).$promise.then(function() {


				Advert.get_user_poster().$promise.then(function(data){

					$scope.adverts = data;

				})

			})

		}

		$scope.getAdvert = function(advert) {

			$location.path('/search/'+advert.id)

		}


	}
);


"use strict";

auto.controller('ProfileSettings',
	function ($scope, User, Advert, $location) {

		$scope.showContent = false;

		$scope.showContent = false;

		User.get_user().$promise.then(function(data){

			if (data.status)  {

				Advert.get_user_poster().$promise.then(function(data){

					$scope.showContent = true;

					$scope.adverts = data;

				})

			} else {
				window.location = '/users/sign_in'
			}

		}, function(err) {

			window.location = '/users/sign_in'

		})


		$scope.remove_advert = function(advert) {

			Advert.destroy({id: advert.id}).$promise.then(function() {


				Advert.get_user_poster().$promise.then(function(data){

					$scope.adverts = data;

				})

			})

		}

		$scope.getAdvert = function(advert) {

			$location.path('/search/'+advert.id)

		}


	}
);

