class CreatePageLangRels < ActiveRecord::Migration
  def change
    create_table :page_lang_rels do |t|
      t.string :title
      t.text :text
      t.integer :lang_id
      t.integer :page_id

      t.timestamps null: false
    end
  end
end
