class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :name
      t.integer :price
      t.integer :duration
      t.integer :s_type

      t.timestamps null: false
    end
  end
end
