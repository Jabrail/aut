class AddImageColumnToPropVals < ActiveRecord::Migration
  def up
    add_attachment :prop_vals, :image
  end

  def down
    remove_attachment :prop_vals, :image
  end
end
