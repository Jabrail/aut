class CreateLangValues < ActiveRecord::Migration
  def change
    create_table :lang_values do |t|
      t.string :value
      t.belongs_to :lang, index: true
      t.belongs_to :property, index: true
      t.timestamps null: false
    end
  end
end
