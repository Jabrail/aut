class AddColumnPropValToPropVals < ActiveRecord::Migration
  def change
    change_table :prop_vals do |t|

      t.belongs_to :prop_val, index: true

    end

  end
end
