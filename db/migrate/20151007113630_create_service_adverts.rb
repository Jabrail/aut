class CreateServiceAdverts < ActiveRecord::Migration
  def change
    create_table :service_adverts do |t|
      t.belongs_to :service, index: true
      t.belongs_to :advert, index: true
      t.timestamps null: false
    end
  end
end
