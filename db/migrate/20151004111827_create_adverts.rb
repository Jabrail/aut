class CreateAdverts < ActiveRecord::Migration
  def change
    create_table :adverts do |t|
      t.float :price
      t.string :description

      t.timestamps null: false
    end
  end
end
