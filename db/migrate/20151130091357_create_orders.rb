class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :status

      t.belongs_to :service, index: true
      t.belongs_to :user, index: true
      t.belongs_to :payment, index: true

      t.timestamps null: false
    end
  end
end
