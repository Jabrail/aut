class AddColumnToAdverts < ActiveRecord::Migration
  def change

    change_table :adverts do |t|
      t.belongs_to :user, index: true
    end
  end
end
