class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.integer :status
      t.integer :invid
      t.float :price
      t.text :desc

      t.timestamps null: false
    end
  end
end
