class CreateAdvProRels < ActiveRecord::Migration
  def change
    create_table :adv_pro_rels do |t|
      t.integer :advert_id
      t.integer :prop_val_id

      t.timestamps null: false
    end
  end
end
