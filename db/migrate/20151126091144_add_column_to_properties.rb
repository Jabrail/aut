class AddColumnToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :valid_data, :string, null: 'false'
  end
end
