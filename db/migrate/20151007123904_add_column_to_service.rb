class AddColumnToService < ActiveRecord::Migration
  def change
    add_column :services, :currency, :string
    add_column :services, :desc, :text
  end
end
