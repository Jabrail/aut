class CreateProperties < ActiveRecord::Migration
  def change
    create_table :properties do |t|
      t.string :name
      t.integer :type_value
      t.belongs_to :group, index: true

      t.timestamps null: false
    end
  end
end
