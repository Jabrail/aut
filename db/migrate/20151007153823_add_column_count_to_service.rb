class AddColumnCountToService < ActiveRecord::Migration
  def change
    add_column :services, :count, :integer
  end
end
