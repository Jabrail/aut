class CreatePropVals < ActiveRecord::Migration
  def change
    create_table :prop_vals do |t|
      t.string :title
      t.string :color
      t.string :img
      t.belongs_to :property, index: true

      t.timestamps null: false
    end
  end
end
