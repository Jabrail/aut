class AddColumnValueToAdvProRels < ActiveRecord::Migration
  def change
    add_column :adv_pro_rels, :values, :string
  end
end
