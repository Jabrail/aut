class AddColumnsToAdverts < ActiveRecord::Migration
  def change
    add_column :adverts, :state, :string
    add_column :adverts, :year, :string
    add_column :adverts, :body, :string
    add_column :adverts, :color, :string
    add_column :adverts, :mark, :string
    add_column :adverts, :model, :string
    add_column :adverts, :mileage, :string
    add_column :adverts, :transmission, :string
    add_column :adverts, :fuel_type, :string
    add_column :adverts, :power, :string
    add_column :adverts, :drive, :string
    add_column :adverts, :number_of_doors, :string
    add_column :adverts, :accident, :string
    add_column :adverts, :requires_repair, :string
    add_column :adverts, :additional, :string
    add_column :adverts, :volume, :string
    add_column :adverts, :country, :string
    add_column :adverts, :city, :string
    add_column :adverts, :partic_filter, :string
    add_column :adverts, :number_of_seats, :string
    add_column :adverts, :metalik, :string
    add_column :adverts, :pearl, :string
    add_column :adverts, :unpolished, :string
    add_column :adverts, :akryl, :string
    add_column :adverts, :wheel, :string
    add_column :adverts, :seller, :string
  end
end
