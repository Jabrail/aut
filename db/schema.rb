# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151130091357) do

  create_table "adv_pro_rels", force: :cascade do |t|
    t.integer  "advert_id",   limit: 4
    t.integer  "prop_val_id", limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "values",      limit: 255
  end

  create_table "adverts", force: :cascade do |t|
    t.float    "price",           limit: 24
    t.text     "description",     limit: 65535
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "user_id",         limit: 4
    t.string   "state",           limit: 255
    t.string   "year",            limit: 255
    t.string   "body",            limit: 255
    t.string   "color",           limit: 255
    t.string   "mark",            limit: 255
    t.string   "model",           limit: 255
    t.string   "mileage",         limit: 255
    t.string   "transmission",    limit: 255
    t.string   "fuel_type",       limit: 255
    t.string   "power",           limit: 255
    t.string   "drive",           limit: 255
    t.string   "number_of_doors", limit: 255
    t.string   "accident",        limit: 255
    t.string   "requires_repair", limit: 255
    t.string   "additional",      limit: 255
    t.string   "volume",          limit: 255
    t.string   "country",         limit: 255
    t.string   "city",            limit: 255
    t.string   "partic_filter",   limit: 255
    t.string   "number_of_seats", limit: 255
    t.string   "metalik",         limit: 255
    t.string   "pearl",           limit: 255
    t.string   "unpolished",      limit: 255
    t.string   "akryl",           limit: 255
    t.string   "wheel",           limit: 255
    t.string   "seller",          limit: 255
  end

  add_index "adverts", ["user_id"], name: "index_adverts_on_user_id", using: :btree

  create_table "groups", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "images", force: :cascade do |t|
    t.string   "title",              limit: 255
    t.integer  "advert_id",          limit: 4
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
  end

  add_index "images", ["advert_id"], name: "index_images_on_advert_id", using: :btree

  create_table "lang_values", force: :cascade do |t|
    t.string   "value",       limit: 255
    t.integer  "lang_id",     limit: 4
    t.integer  "property_id", limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "lang_values", ["lang_id"], name: "index_lang_values_on_lang_id", using: :btree
  add_index "lang_values", ["property_id"], name: "index_lang_values_on_property_id", using: :btree

  create_table "langs", force: :cascade do |t|
    t.string   "short_name", limit: 255
    t.string   "long_name",  limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "models", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "models", ["email"], name: "index_models_on_email", unique: true, using: :btree
  add_index "models", ["reset_password_token"], name: "index_models_on_reset_password_token", unique: true, using: :btree

  create_table "orders", force: :cascade do |t|
    t.integer  "status",     limit: 4
    t.integer  "service_id", limit: 4
    t.integer  "user_id",    limit: 4
    t.integer  "payment_id", limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "orders", ["payment_id"], name: "index_orders_on_payment_id", using: :btree
  add_index "orders", ["service_id"], name: "index_orders_on_service_id", using: :btree
  add_index "orders", ["user_id"], name: "index_orders_on_user_id", using: :btree

  create_table "page_lang_rels", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.text     "text",       limit: 65535
    t.integer  "lang_id",    limit: 4
    t.integer  "page_id",    limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "pages", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "href",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "payments", force: :cascade do |t|
    t.integer  "status",     limit: 4
    t.integer  "invid",      limit: 4
    t.float    "price",      limit: 24
    t.text     "desc",       limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "prop_vals", force: :cascade do |t|
    t.string   "title",              limit: 255
    t.string   "color",              limit: 255
    t.string   "img",                limit: 255
    t.integer  "property_id",        limit: 4
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
    t.integer  "prop_val_id",        limit: 4
  end

  add_index "prop_vals", ["prop_val_id"], name: "index_prop_vals_on_prop_val_id", using: :btree
  add_index "prop_vals", ["property_id"], name: "index_prop_vals_on_property_id", using: :btree

  create_table "properties", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "type_value", limit: 4
    t.integer  "group_id",   limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "valid_data", limit: 255
  end

  add_index "properties", ["group_id"], name: "index_properties_on_group_id", using: :btree

  create_table "service_adverts", force: :cascade do |t|
    t.integer  "service_id", limit: 4
    t.integer  "advert_id",  limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "service_adverts", ["advert_id"], name: "index_service_adverts_on_advert_id", using: :btree
  add_index "service_adverts", ["service_id"], name: "index_service_adverts_on_service_id", using: :btree

  create_table "service_users", force: :cascade do |t|
    t.integer  "service_id", limit: 4
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "service_users", ["service_id"], name: "index_service_users_on_service_id", using: :btree
  add_index "service_users", ["user_id"], name: "index_service_users_on_user_id", using: :btree

  create_table "services", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "price",      limit: 4
    t.integer  "duration",   limit: 4
    t.integer  "s_type",     limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "currency",   limit: 255
    t.text     "desc",       limit: 65535
    t.integer  "count",      limit: 4
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.integer  "user_type",              limit: 4
    t.string   "token",                  limit: 255
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
