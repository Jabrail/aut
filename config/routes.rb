Rails.application.routes.draw do
  resources :orders
  get 'payments/result'

  get 'payments/success'

  get 'payments/fail'

  get 'payments/pay'

  resources :pages , :defaults => { :format => 'json' }
  resources :images, :defaults => { :format => 'json' }
  devise_for :users, :controllers => { :registrations => 'users'}
  devise_scope :user do
    get 'users/' => 'users#index' , :defaults => { :format => 'json' }
  end

  
  get 'home/index'

  get 'prop_vals/get_sub' => 'prop_vals#get_sub',  :defaults => { :format => 'json' }
  post 'prop_vals/add_sub' => 'prop_vals#add_sub',  :defaults => { :format => 'json' }

  post 'pages/update_lang/:id' => 'pages#update_lang',  :defaults => { :format => 'json' }

  get 'api/get_filters' => 'client_api#get_filters' , :defaults => { :format => 'json' }
  get 'api/get_filters_for_new' => 'client_api#get_filters_for_new' , :defaults => { :format => 'json' }
  get 'api/get_poster/:id' => 'client_api#get_poster' , :defaults => { :format => 'json' }
  post 'api/get_by_prop' => 'adverts#get_by_prop' , :defaults => { :format => 'json' }
  get 'api/pages/:id' => 'pages#add_content', :defaults => { :format => 'json' }
  get 'api/last_poster/:count' => "client_api#last_poster" , :defaults => { :format => 'json' }
  get 'api/get_marks/' => "client_api#get_marks" , :defaults => { :format => 'json' }
  get 'api/get_pages/' => "client_api#get_pages" , :defaults => { :format => 'json' }
  get 'api/get_pages/:id' => "client_api#get_page" , :defaults => { :format => 'json' }
  get 'api/get_user/' => "client_api#get_user" , :defaults => { :format => 'json' }
  get 'api/get_user_poster/' => "client_api#get_user_poster" , :defaults => { :format => 'json' }
  get 'api/count_poster/' => "client_api#count_poster" , :defaults => { :format => 'json' }

  post 'lang/add_filter' => 'lang_values#add_filter' , :defaults => { :format => 'json' }
  get 'lang/get_filters/:id' => 'lang_values#get_filters' , :defaults => { :format => 'json' }


  get 'users/' => 'users#index' , :defaults => { :format => 'json' }


  get 'control' => 'control#index'
  get 'control/private_person'
  get 'control/dealer'
  get 'control/admin'

  get 'poster/set_top/:id' => 'adverts#set_top',  :defaults => { :format => 'json' }

  post 'service/add/:id' => 'services#add',  :defaults => { :format => 'json' }

  resources :adverts, path: "/posters",  :defaults => { :format => 'json' }
  resources :prop_vals,  :defaults => { :format => 'json' }
  resources :properties, :defaults => { :format => 'json' }
  resources :groups,  :defaults => { :format => 'json' }
  resources :lang_values,  :defaults => { :format => 'json' }
  resources :langs,  :defaults => { :format => 'json' }
  resources :services,  :defaults => { :format => 'json' }

  get 'add_filter' => 'groups#add_filter' , :defaults => { :format => 'json' }
  get 'remove_filter' => 'groups#remove_filter' , :defaults => { :format => 'json' }
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".
  # You can have the root of your site routed with "root"
  root 'home#index'

  get '*path' => 'home#index'

    # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
