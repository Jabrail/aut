var lr = require('tiny-lr'), // Минивебсервер для livereload
    gulp = require('gulp'), // Сообственно Gulp JS
    livereload = require('gulp-livereload'), // Livereload для Gulp
    uglify = require('gulp-uglify'), // Минификация JS
    concat = require('gulp-concat'), // Склейка файлов
    connect = require('connect'), // Webserver
    server = lr();


gulp.task('js', function() {
    gulp.src(['./app/assets/javascripts/client_app/**/*.js', '!./app/assets/javascripts/client_app/vendor/**/*.js'])
        .pipe(concat('index.js')) // Собираем все JS, кроме тех которые находятся в ./assets/js/vendor/**
        .pipe(gulp.dest('./app/assets/javascripts/client_public/'))
        .pipe(livereload(server)); // даем команду на перезагрузку страницы
});
gulp.task('js_libs', function() {
    gulp.src(['./app/assets/javascripts/libs/**/*.js', '!./app/assets/javascripts/client_app/vendor/**/*.js'])
        .pipe(concat('libs.js')) // Собираем все JS, кроме тех которые находятся в ./assets/js/vendor/**
        .pipe(gulp.dest('./app/assets/javascripts/client_public/'))
        .pipe(livereload(server)); // даем команду на перезагрузку страницы
});
gulp.task('js_control', function() {
    gulp.src(['./app/assets/javascripts/app/**/*.js', '!./app/assets/javascripts/app/apps/**/*.js'])
        .pipe(concat('control.js')) // Собираем все JS, кроме тех которые находятся в ./assets/js/vendor/**
        .pipe(gulp.dest('./app/assets/javascripts/control/'))
        .pipe(livereload(server)); // даем команду на перезагрузку страницы
});

gulp.task('http-server', function() {
    connect()
        .use(require('connect-livereload')())
        .use(connect.static('./'))
        .listen('9000');

    console.log('Server listening on http://localhost:9000');
});


gulp.task('watch', function() {
    // Предварительная сборка проекта

    gulp.run('js');
    gulp.run('js_libs');
    gulp.run('js_control');

    //  gulp.run('js_libs');

    // Подключаем Livereload


    gulp.watch('./app/assets/javascripts/client_app/**/*.js', function() {
            gulp.run('js');
    });

    gulp.watch('./app/assets/javascripts/libs/**/*.js', function() {
            gulp.run('js_libs');
    });

    gulp.watch('./app/assets/javascripts/app/**/*.js', function() {
            gulp.run('js_control');
    });
 });
